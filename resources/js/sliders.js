window.arrowLeft = function() {
    let hash = Math.random().toString(36).substring(7);
    return `<button type="button" class="slick-prev slick-arrow">
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <mask id="path-1-inside-${hash}" fill="white">
                        <path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path>
                    </mask>
                    <path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-${hash})"></path>
                </svg>
            </button>`;
};
window.arrowRight = function() {
    let hash = Math.random().toString(36).substring(7);
    return `<button type="button" class="slick-next slick-arrow">
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <mask id="path-1-inside-${hash}" fill="white">
                        <path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path>
                    </mask>
                    <path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-${hash})"></path>
                </svg>
            </button>`;
}


let filteredProducts = false;
if (document.querySelector('.product-slider')) {
    let $productSlider = $('.product-slider');
    let productSlides = document.querySelectorAll('.product-slider .item');
    /* $('.product-slider').on('init', function(event, slick) {
        setTimeout(() => {
            $('.product-slider .slick-slide').each(function(index, element) {
                $(this).addClass('cid-' + $(this).find('.item').data('cid'));
            });
            filterProductSlider();
            $('.product-slider').addClass('-is-show');
        }, 300);

    }); */
    initProductSlider();
    setTimeout(() => {
        filterProductSlider();
    }, 300);


    function initProductSlider() {
        $productSlider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            responsive: [{
                breakpoint: 851,
                settings: {
                    dots: true,
                }
            }]
        });
        $productSlider.addClass('-is-show');
    }

    function filterProductSlider() {
        let cid = $('.js-filter-color:checked').data('cid');
        $productSlider.slick('unslick');
        $productSlider.html('');
        productSlides.forEach(function(item, index, obj) {
            if (!cid || item.dataset.cid == cid) {
                $productSlider.append(item);
            }
        }, cid);
        initProductSlider();
    }
    $(document).on('change', '.js-filter-color', function() {
        filterProductSlider();
        filterProductSliderNav();
    });
}



function filterProductSliderNav() {
    let cid = +$('.js-filter-color:checked').data('cid');
    if (cid) {
        $('.product-slider-nav .item').addClass('d-none').removeClass('-is-current');
        $(`.product-slider-nav .item[data-cid="${cid}"]`).removeClass('d-none');
        $(`.product-slider-nav .item[data-cid="${cid}"]:first`).addClass('-is-current');
    }

}

filterProductSliderNav();



function navProductsSlider() {
    let num = 0;
    $('.product-slider-nav .item:not(.d-none)').each(function(index, element) {
        if ($(element).hasClass('-is-current')) {
            num = index;
        }
    });
    $('.product-slider').slick('slickGoTo', num);
}
$(document).on('click', '.product-slider-nav .item', function() {
    $('.product-slider-nav .item').removeClass('-is-current');
    $(this).addClass('-is-current');
    navProductsSlider();
});



/* $('.product-slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    infinite: false,
    //asNavFor: '.product-slider',
    focusOnSelect: true
}); */

function filterFilteredProductsSlider() {
    let tid = $('.js-slider-filtered-link.active').data('tid');
    $('.js-slider-filtered').slick('slickUnfilter');
    $('.js-slider-filtered .slick-slide .catalog_item').each(function(index, element) {
        let id = $(element).data('tid');
        if (id) {
            $(element).parents('.slick-slide').addClass(`tid-${id}`);
        }
    });
    if (tid) {
        setTimeout(() => {
            $('.js-slider-filtered').slick('slickFilter', `.tid-${tid}`);
        }, 1000);

    }
}


/* $('.js-slider-filtered').on('init', function(event, slick) {
    setTimeout(() => {
        $('.js-slider-filtered .slick-slide').each(function(index, element) {
            $(this).addClass('tid-' + $(this).find('.catalog_item').data('tid'));
        });
        let currentIndex = $('.js-slider-filtered .catalog_item.checked').data('num');
        if (currentIndex) {
            $('.js-slider-filtered').slick('slickGoTo', currentIndex);
        }
        filterFilteredProductsSlider();
        $('.js-slider-filtered').addClass('-is-show');
    }, 300);

}); */

/* Related products slider */

if (document.querySelector('.js-slider-filtered-products')) {
    let slides = document.querySelectorAll('.js-slider-filtered-products .catalog_item');

    function initProductRelatedSlider() {
        $('.js-slider-filtered-products').html('');
        let tid = $('.js-slider-filtered-products-link.active').data('tid');
        let currentIndex = 0;
        let addIndex = 0;
        slides.forEach(function(item, index, obj) {
            if (!tid || item.dataset.tid == tid) {
                if ($(item).hasClass('checked')) {
                    currentIndex = addIndex;
                }
                addIndex++;
                $('.js-slider-filtered-products').append(item); // console.log(currentValue + ', ' + currentIndex + ', ' + this);
            }
        }, tid);
        /* если элементов меньше 5 то не прокручиваем слайдер*/
        console.log('length ' + slides.length);
        if (addIndex < 5) {
            currentIndex = 0;
        }
        setTimeout(() => {
            $('.js-slider-filtered-products').css({ height: 'auto', opacity: 1, overflow: 'auto' });

            /* if (currentIndex > 0) {
                setTimeout(() => {
                    $('.js-slider-filtered-products').slick('slickGoTo', currentIndex);
                }, 300);
            } */
            console.log(currentIndex);
            $('.js-slider-filtered-products').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                prevArrow: arrowLeft(),
                nextArrow: arrowRight(),
                infinite: false,
                initialSlide: currentIndex,
                focusOnSelect: false,
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 821,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2.5,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 520,
                        settings: {
                            slidesToShow: 1.5,
                            slidesToScroll: 1,
                            arrows: false
                        }
                    }
                ]
            });
        }, 200);

    }
    initProductRelatedSlider();
    $(document).on('click', '.js-slider-filtered-products-link', function() {
        $('.js-slider-filtered-products-link').removeClass('active');
        $(this).addClass('active');
        $('.js-slider-filtered-products').slick('unslick');
        $('.js-slider-filtered-products').css({ height: 222, opacity: 0, overflow: 'hidden', transition: 'opacity 0s' });
        initProductRelatedSlider();
        return false;
    });
}

/* Things slider in the front page */

if (document.querySelector('.js-slider-filtered-things-front')) {
    let slides = document.querySelectorAll('.js-slider-filtered-things-front .catalog_item');

    function initProductRelatedSlider() {
        $('.js-slider-filtered-things-front').html('');
        let tid = $('.js-slider-filtered-things-front-link.active').data('tid');
        let currentIndex = 0;
        let addIndex = 0;
        slides.forEach(function(item, index, obj) {
            if (!tid || item.dataset.tid == tid) {
                if ($(item).hasClass('checked')) {
                    currentIndex = addIndex;
                }
                addIndex++;
                $('.js-slider-filtered-things-front').append(item); // console.log(currentValue + ', ' + currentIndex + ', ' + this);
            }
        }, tid);
        setTimeout(() => {
            $('.js-slider-filtered-things-front').css({ height: 'auto', opacity: 1, overflow: 'unset' });
            if (currentIndex > 0) {
                setTimeout(() => {
                    $('.js-slider-filtered-things-front').slick('slickGoTo', currentIndex);
                }, 300);
            }
            $('.js-slider-filtered-things-front').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                infinite: false,
                prevArrow: arrowLeft(),
                nextArrow: arrowRight(),
                focusOnSelect: false,
                touchMove: false,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 821,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2.5,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 520,
                        settings: {
                            slidesToShow: 1.5,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    }
                ]
            });
        }, 100);

    }
    initProductRelatedSlider();
    $(document).on('click', '.js-slider-filtered-things-front-link', function() {
        $('.js-slider-filtered-things-front-link').removeClass('active');
        $(this).addClass('active');
        $('.js-slider-filtered-things-front').slick('unslick');
        $('.js-slider-filtered-things-front').css({ height: 222, opacity: 0, overflow: 'hidden', transition: 'opacity 0s' });
        initProductRelatedSlider();
        return false;
    });
}

/* $(document).on('click', '.js-slider-filtered__link', function(e) {
    e.stopPropagation();
    $('body').addClass('show-init');
}); */
$('.js-slider-filtered-things').each(function(index, element) {
    $(element).slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: false,
        prevArrow: arrowLeft(),
        nextArrow: arrowRight(),
        focusOnSelect: false,
        touchMove: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 821,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2.5,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1.5,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
});
$('.carousel_product').each(function(index, element) {
    $(element).slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: false,
        prevArrow: arrowLeft(),
        nextArrow: arrowRight(),
        focusOnSelect: false,
        touchMove: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 821,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2.5,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1.5,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    });
});

/* $('.js-filter').on('click', function() {
    if (filtered === false) {
        $('.filtering').slick('slickFilter', ':even');
        $(this).text('Unfilter Slides');
        filtered = true;
    } else {
        $('.filtering').slick('slickUnfilter');
        $(this).text('Filter Slides');
        filtered = false;

    }
}); */
