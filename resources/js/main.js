const { default: Axios } = require("axios");

jQuery(function($) {

    $(function() {
        let vacation = $('#vacation');
        let isShowed = sessionStorage.getItem('showedVacation') || 0;
        if (vacation.length && !isShowed) {
            sessionStorage.setItem('showedVacation', 1);
            vacation.modal();
        }
        $(".scroll_left").mousedown(function() {
            var rightPos = $(this).parent().next().scrollLeft();
            $(this).parent().next().scrollLeft(rightPos - 10);
        });

        $(".scroll_right").mousedown(function() {
            var leftPos = $(this).parent().next().scrollLeft();
            $(this).parent().next().scrollLeft(leftPos + 10);
        });

        $("#carouselExampleIndicators").carousel({
            interval: false
        });

        let text = '';

        function getItems() {
            Axios.post("/catalog/search", { text })
                .then(function(result) {
                    data = result.data;
                    if (data.status == 'success') {
                        $('.js-search-result').html(data.list);
                        if ($(window).width() <= 560) {
                            $('.header_search').addClass('visible');
                        }
                        $('.header_search .button_close').addClass('active');
                        $('.popup_search_result').addClass('active');
                        $('.overlay_wrap').fadeIn(400);
                    } else {
                        $('.popup_search_result').removeClass('active');
                        $('.overlay_wrap').fadeOut(400);
                    }
                });
        }
        let debounced = _.debounce(getItems, 250, { 'maxWait': 1000 });
        $('.header_search .input_search').on("change paste keyup keydown", function(e) {
            if (e.keyCode == 27) {
                $('.overlay_wrap').trigger('click')
                return false;
            }
            if ($(this).val().length >= 3) {
                text = $(this).val();
                debounced();
            } else {
                $('.header_search .button_close').removeClass('active');
            }
        });

        $('.overlay_wrap').on("click", function() {
            $('.popup_search_result').removeClass('active');
            $(this).fadeOut(400);
        });
        $(document).on('click', '.result_close_popup', function(e) {
            e.stopPropagation();
            $('.input_search').val('');
            $(this).removeClass('active');
            $('.header_search').removeClass('visible');

            $('.popup_search_result').removeClass('active');
            $('.overlay_wrap').fadeOut(400);
            return false;
        });
        $('.header_search .button_close').on("click", function() {
            if ($(window).width() <= 560) {
                //$('.header_search').removeClass('visible');
            }
            $('.header_search .input_search').val('');
            $(this).removeClass('active');
            //$('.popup_search_result').removeClass('active');
            //$('.overlay_wrap').fadeOut(400);
        });



        $('[data-toggle="tooltip"]').tooltip();
        var btn = $('.arrow-top');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('active');
            } else {
                btn.removeClass('active');
            }
        });
        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, '300');
        });
        /* if ($(window).width() < 600) {
            $('.category_list_wrap .row:not(.opt_items)').addClass('owl-carousel')
            $('.category_list_wrap .row:not(.opt_items)').owlCarousel({
                loop: false, //Зацикливаем слайдер
                margin: 15, //Отступ от элемента справа в 50px
                nav: true, //Отключение навигации
                dots: false,
                autoplay: false, //Автозапуск слайдера
                smartSpeed: 1000, //Время движения слайда
                autoplayTimeout: 2000, //Время смены слайда
                navText: ['<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-12" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-12)"></path></svg>',
                    '<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-22" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-22)"></path></svg>'
                ],
                responsive: { //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                    0: {
                        items: 2,
                        smartSpeed: 500,
                    },
                    550: {
                        items: 3
                    },
                    700: {
                        items: 3
                    },
                    1100: {
                        items: 4
                    },
                    1367: {
                        items: 5
                    }
                }
            });
        } */

        /* $('.catalog_items.line .owl-carousel').owlCarousel({
            loop: false, //Зацикливаем слайдер
            margin: 15, //Отступ от элемента справа в 50px
            nav: true, //Отключение навигации
            dots: false,
            autoplay: false, //Автозапуск слайдера
            smartSpeed: 1000, //Время движения слайда
            autoplayTimeout: 2000, //Время смены слайда
            navText: ['<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-6" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-6)"></path></svg>',
                '<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-2" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-2)"></path></svg>'
            ],
            responsive: { //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0: {
                    items: 2,
                    smartSpeed: 500,
                },
                550: {
                    items: 3
                },
                700: {
                    items: 3
                },
                1100: {
                    items: 4
                },
                1367: {
                    items: 5
                }
            }
        }); */

        /* $('.carousel_product').owlCarousel({
            loop: true, //Зацикливаем слайдер
            margin: 10, //Отступ от элемента справа в 50px
            nav: true, //Отключение навигации
            dots: false,
            navText: ['<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-23" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-23)"></path></svg>',
                '<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-24" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-24)"></path></svg>'
            ],
            autoplay: false, //Автозапуск слайдера
            smartSpeed: 1000, //Время движения слайда
            autoplayTimeout: 2000, //Время смены слайда
            responsive: { //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0: {
                    dots: false,
                    items: 2,
                    smartSpeed: 500,
                },
                550: {
                    dots: false,
                    items: 2
                },

                700: {
                    items: 3
                },
                1100: {
                    items: 4,
                    dots: false,
                },
                1367: {
                    dots: false,
                    items: 5
                }
            }
        }); */

        $('.catalog_items .owl-carousel.carousel__page_product').owlCarousel({
            loop: false, //Зацикливаем слайдер
            margin: 10, //Отступ от элемента справа в 50px
            nav: true, //Отключение навигации
            dots: false,
            navText: ['<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-25" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-25)"></path></svg>',
                '<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-26" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-26)"></path></svg>'
            ],
            autoplay: false, //Автозапуск слайдера
            smartSpeed: 1000, //Время движения слайда
            autoplayTimeout: 2000, //Время смены слайда
            responsive: { //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0: {
                    items: 4,
                    smartSpeed: 500,
                },
                550: {
                    items: 5
                },
                700: {
                    items: 4
                },
                900: {
                    items: 3,
                    dots: false,
                },
                1030: {
                    items: 4,
                    dots: false,
                },
                1200: {
                    dots: false,
                    items: 4
                }
            }
        });
        $('.instagram_carousel').owlCarousel({
            loop: true, //Зацикливаем слайдер
            margin: 10, //Отступ от элемента справа в 50px
            nav: true, //Отключение навигации
            navText: ['<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-27" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-27)"></path></svg>',
                '<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-28" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-28)"></path></svg>'
            ],
            autoplay: false, //Автозапуск слайдера
            smartSpeed: 1000, //Время движения слайда
            autoplayTimeout: 2000, //Время смены слайда
            responsive: { //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0: {
                    items: 3,
                    smartSpeed: 500,
                },
                680: {
                    items: 3
                },
                970: {
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
        $('.comment_items').owlCarousel({
            loop: true, //Зацикливаем слайдер
            margin: 10, //Отступ от элемента справа в 50px
            nav: true, //Отключение навигации
            autoplay: false, //Автозапуск слайдера
            navText: ['<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-29" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-29)"></path></svg>',
                '<svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><mask id="path-1-inside-30" fill="white"><path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"></path></mask><path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z" fill="#BD2937" mask="url(#path-1-inside-30)"></path></svg>'
            ],
            smartSpeed: 1000, //Время движения слайда
            autoplayTimeout: 2000, //Время смены слайда
            responsive: { //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0: {
                    items: 1,
                    nav: false,
                    smartSpeed: 500,
                },
                640: {
                    items: 1,
                    //nav: false,
                },
                980: {
                    items: 2
                },
                1200: {
                    items: 2
                }
            }
        });

        $('.faq_item_title').on('click', function() {
            if ($(this).hasClass('active')) {
                $('.faq_item_title').removeClass('active');
                $(this).next().slideUp(500);
            } else {
                $('.faq_item_title').removeClass('active');
                $('.faq_item_text').slideUp(500);
                $(this).next().slideDown(500);
                $(this).addClass('active');
            }
        });

        $('.header_search_open').on('click', function() {
            $('.header_mobile .header_search').toggleClass('visible');
        });

        $('.open_collection a').on('click', function(e) {
            e.preventDefault();
        });

        $('.menu_ico').on('click', function() {
            $('.mobile_menu').addClass('open');
            $('.mobile_menu').removeClass('animate');
        });
        $('.mobile_menu_close').on('click', function() {
            //$('.mobile_menu').removeClass('open');
            $('.mobile_menu').addClass('animate');
        });

        function openCatalogMenu() {
            $('body').addClass('aside-open')
            $('.col-aside').toggleClass('open');
            $('.col-aside').slideToggle(500);
        }
        $('.open_collection').on('click', function() {
            openCatalogMenu();
        });
        if (window.location.search == '?menu' && window.innerWidth < 768) {
            openCatalogMenu();
        }
        $(document).on('click', '.minus', function() {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $(document).on('click', '.plus', function() {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();

            return false;
        });

        /* Меняем текст ссылок "смотреть все" */
        if ($(window).width() <= 480) {
            $('.section_title_link').text('все >>');
        }
    });
    $(document).on('change', '.js-qty', function() {
        calcProductPrice();
    });

    function calcProductPrice() {
        let prices = $('.js-price').data('prices');
        let qty = +$('.js-qty').val();
        price = _.find(prices, (p) => { return p.min <= qty; });
        let text = 'Договорная';
        if (price && price.value) {
            text = (price.value * qty) + ' руб.'
        }
        $('.js-price').html(text);
    }
    /* Галерея картинок товара
    ------------------------------------------*/

    dotcount = 1;


    mask();

    // function mask input
    function mask() {

        $.mask.definitions['~'] = '[+-]';

        $('#date').mask('99/99/9999');

        $('.input-phone').mask('+375 (99) 999-99-99');

        $('#phoneext').mask("(999) 999-9999? x99999");

        $("#tin").mask("99-9999999");

        $("#ssn").mask("999-99-9999");

        $("#product").mask("a*-999-a999");

        $("#eyescript").mask("~9.99 ~9.99 999");

    }

});

jQuery(function($) {

    $('.product_size_items [data-toggle="tooltip"]').tooltip({
        container: '.product_size_items'
    });

    $(window).resize(function() {
        if ($(window).width() <= 480) {
            $('.section_title_link').text('все >>');
        } else {
            $('.section_title_link').text('Смотреть все >>');
        }
    });

    $('.nav-link').each(function() {
        var parents = $(this).closest('.nav-tabs');
        var line = $(this).closest('.nav-tabs').find('.tab_line');
        if ($(this).hasClass('active')) {
            var active_width = $(this).outerWidth();
            var active_left = $(this).position().left;
            line.css({ 'width': active_width + 'px', 'left': active_left + 'px' })
        }
        $(this).click(function() {
            var active_width = $(this).outerWidth();
            var active_left = $(this).closest('.nav-item').position().left;
            line.css({ 'width': active_width + 'px', 'left': active_left + 'px' })
            if ($(window).width() < 767) {
                parents.stop().animate({
                    scrollLeft: '+=' + (active_left)
                });
            }
        })
    })
    if ($(window).width() < 767) {
        $('.aside_menu_item').each(function() {
            $(this).removeClass('open');
            /* if ($(this).find('.aside_menu_arrow')) {
                $(this).find('a').click(function(e) {
                    e.preventDefault();
                    $(this).closest('.aside_menu_item').addClass('open')
                })
            } */
        })
        $('.aside-back').click(function() {
            $('.col-aside').removeClass('open')
            $('body').removeClass('aside-open')
        })
        $('.aside_sub_menu_back').click(function() {
            $(this).closest('.aside_menu_item').removeClass('open')
        })
    }
    $('.js-cat-parent').on('click', function() {
        if ($(window).width() < 767) {
            $(this).parent().addClass('open')
        } else {
            if ($(this).parent().hasClass('open')) {
                $('.aside_menu_item').removeClass('open');
                $(this).parent().find('ul').slideUp(500);
            } else {
                $('.aside_menu_item').removeClass('open');
                $('.aside_sub_menu').slideUp(500);
                $(this).parent().children('.aside_sub_menu').slideDown(500);
                $(this).parent().addClass('open');
            }
        }
        return false;
    });
    if ($(window).scrollTop() > 80) {
        $('.col-aside').addClass('active')
    } else {
        $('.col-aside').removeClass('active')
    }
    $(window).scroll(function() {
        if ($(window).scrollTop() > 80) {
            $('.col-aside').addClass('active')
        } else {
            $('.col-aside').removeClass('active')
        }
    })
});
