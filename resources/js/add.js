$(document).on('click', '.js-show-more', function() {
    let wrapper = $(this).parents('.js-list-wrapper');
    $.getJSON($(this).attr('href'),
        function(data, textStatus, jqXHR) {
            $(wrapper).find('.js-list').append(data.list);
            $(wrapper).find('.js-list-paginate').html(data.paginate);
        }
    );
    return false;
});
$('#typesTab a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    let $link = $(e.target);
    let type = $link.data('type');
    let path = window.location.origin + window.location.pathname;
    let wrapper = $($link.attr('href'));
    if (!wrapper.find('.js-list').html()) {
        $.getJSON(path, { type },
            function(data, textStatus, jqXHR) {
                $(wrapper).find('.js-list').append(data.list);
                $(wrapper).find('.js-list-paginate').html(data.paginate);
            }
        );
    }
})
$(document).on('click', '.js-list-wrapper .page-link', function() {
    let wrapper = $(this).parents('.js-list-wrapper');
    $(".js-list").css({ opacity: 0 });
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".js-list").offset().top - 85
    }, 300);
    $.getJSON($(this).attr('href'),
        function(data, textStatus, jqXHR) {
            $(wrapper).find('.js-list').html(data.list);
            $(wrapper).find('.js-list-paginate').html(data.paginate);
            $(".js-list").css({ opacity: 1 });
        }
    );
    return false;
});

window.setImages = function() {

    $('[data-src]').each(function(index, element) {
        $(element).attr('src', $(element).data('src')).removeAttr('data-src');
    });
}
setImages();
$(document).on('click', '.js-product_size_item', function() {
    $('.js-product_size_item').removeClass('active');
    $(this).addClass('active');
    $('.js-size').val($(this).data('size'));
    setProductBtn();
});

$(document).on('change', '.js-filter-color', function() {
    setProductBtn();
});
if ($('div').hasClass('js-product-btn-add')) {
    setProductBtn();
}

function setProductBtn() {
    let keys = $('.js-product-btn-add').data('keys');
    let key = $('[name="key"]').val();
    if ($('input').hasClass('js-filter-color')) {
        key = $('.js-filter-color:checked').data('key');
    }
    if ($('div').hasClass('js-product_size_item')) {
        key = $('.js-product_size_item.active').data('key');
    }
    let is = keys.find(k => k == key);
    if (is) {
        $('.js-is-not-added').addClass('d-none');
        $('.js-is-added').removeClass('d-none');
    } else {
        $('.js-is-added').addClass('d-none');
        $('.js-is-not-added').removeClass('d-none');
    }
}

$(document).on('submit', '.js-to-cart', function() {
    $.post($(this).attr('action'), $(this).serialize(),
        function(data, textStatus, jqXHR) {
            $.each(data.fields, function(indexInArray, valueOfElement) {
                $(indexInArray).html(valueOfElement);
            });
            let keys = $('.js-product-btn-add').data('keys');
            keys.push(data.key);
            $('.js-product-btn-add').data('keys', keys);
            setProductBtn();
        },
        "json"
    );
    return false;
});
$(document).on('click', '.js-change-size-link', function() {

});
$(document).on('click', '.js-item_add_cart', function() {
    /* $('#sizesModal').modal();
    return false; */
    if ($(this).hasClass('purchase')) {
        location.href = '/cart';
        return;
    }
    $.getJSON($(this).attr('href'), function(data, textStatus, jqXHR) {
        $.each(data.fields, function(indexInArray, valueOfElement) {
            $(indexInArray).html(valueOfElement);
        });
        $(`[data-key="${data.key}"]`).addClass('purchase');
    });
    return false;
});

$(document).on('click', '.js-checkout-link, .js-back-cart', function() {
    $.getJSON($(this).attr('href'), function(data, textStatus, jqXHR) {
        $('.js-cart-content').html(data.content);
        initCheckoutForm();
    });
    return false;
});
initCheckoutForm();

function initCheckoutForm() {
    $('.input-phone').mask('+375 (99) 999-99-99');
    $(".js-checkout-form").each(function(index, element) {
        $(this).validate({
            rules: {
                phone: {
                    required: true,
                },
                name: {
                    required: true,
                },
            },
            messages: {
                phone: {
                    required: 'Введите телефон',
                },
                name: {
                    required: 'Введите имя',
                },
            },
            /*  submitHandler: function submitHandler(form) {
                 $.post($(form).attr('action'), $(form).serialize(),
                     function(data, textStatus, jqXHR) {
                         $('.js-cart-content').html(data.content);
                         setTimeout(() => {
                             initProductSlider();
                         }, 500);
                     },
                     "json"
                 );
                 return false;
             }*/
        });
    });
}