$(document).ready(function() {
    $(document).on('click', '.js-quick-modal', function() {
        if ($(this).hasClass('js-quick-cart')) {
            $('#quickModal [name="eid"]').val('cart');
        } else {
            $('#quickModal [name="eid"]').val($(this).data('eid'));
            $('#quickModal [name="qty"]').val($('.js-qty').val());
            if ($('.js-size').length) {
                $('#quickModal [name="size"]').val($('.js-size').val());
            }
            if ($('.js-filter-color').length) {
                $('#quickModal [name="color"]').val($('.js-filter-color:checked').val());
            }
        }
        $('#quickModal').modal();
        return false;
    });

    $(".js-form").each(function(index, element) {
        $(this).validate({
            rules: {
                phone: {
                    required: true,
                },
                name: {
                    required: true,
                },
            },
            messages: {
                phone: {
                    required: 'Введите телефон',
                },
                name: {
                    required: 'Введите имя',
                },
            },
            invalidHandler: function(event, validator) {
                $('.popup-error-message').removeClass('d-none');
            },
            submitHandler: function submitHandler(form) {
                $.post($(form).attr('action'), $(form).serialize(),
                    function(data, textStatus, jqXHR) {
                        $.each(data.fields, function(indexInArray, valueOfElement) {
                            $(indexInArray).html(valueOfElement);
                        });
                        if (data.popup) {
                            $('.modal').modal('hide');
                            $(data.popup).modal('show');
                        }
                        if (data.location) {
                            location.href = data.location;
                        }
                        $('form').trigger("reset");
                    },
                    "json"
                );
                return false;
            }
        });
    });

    $(document).on('change', '.js-cart-qty', function() {
        $(this).parents('form').submit();
    });
    $(document).on('click', '.js-cart-remove', function() {
        $(this).parents('form').find('.js-cart-qty').val(0);
        $(this).parents('form').submit();
    });
    if ($('span').hasClass('js-checkout-total')) {
        calcDelivery();
        setAddress();
    }
    $(document).on('change', '[name="delivery"]', function() {

        calcDelivery();
        setAddress();
    });
    $.jMaskGlobals = {
        maskElements: 'input,td,span,div',
        dataMaskAttr: '*[data-mask]',
        dataMask: true,
        watchInterval: 300,
        watchInputs: true,
        watchDataMask: false,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            '0': { pattern: /\d/ },
            '9': { pattern: /\d/, optional: true },
            '#': { pattern: /\d/, recursive: true },
            'A': { pattern: /[a-zA-Z0-9]/ },
            'S': { pattern: /[a-zA-Z]/ }
        }
    };
    $.mask.definitions['r'] = "[A-Za-z0-9А-я \.\,]"

    function setAddress() {
        let did = +$('[name="delivery"]:checked').val();
        console.log(did);
        switch (did) {
            case 1:
                $('.js-field-name-group label').text('Имя');
                $('.js-field-name-group input').attr('placeholder', 'Марина');
                $('.js-d-hide-1').addClass('d-none');
                break;
            case 2:
                $('.js-field-name-group label').text('Имя');
                $('.js-field-name-group input').attr('placeholder', 'Марина');
                $('.js-d-hide-1').removeClass('d-none');
                $('.js-d-hide-2').addClass('d-none');
                $('.js-delivery-address').find('label').text('Адрес доставки');
                $('.js-delivery-address').find('input').addClass('-is_city').attr('placeholder', 'г.Минск');
                $('.-is_city').mask('г.Минск, r?rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', { placeholder: "" });
                break;
            case 3:
                $('.js-field-name-group label').text('ФИО');
                $('.js-field-name-group input').attr('placeholder', 'Кузнецова Мария Сергеевна');
                $('.js-d-hide-1').removeClass('d-none');
                $('.js-d-hide-2').removeClass('d-none');
                $('.js-delivery-address').find('label').text('Город, улица и номер дома');
                $('.js-delivery-address').find('input').addClass('-is_city').attr('placeholder', 'г.Борисов, ул. Притыцкого, д.23, кв.72');
                //$('.js-delivery-address').find('input').removeClass('-is_city');

                $('.-is_city').mask('r?rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', { placeholder: "" });
                $('.js-d-hide-1').removeClass('d-none');
                break;
            default:
                $('.js-d-hide-1').removeClass('d-none');
                $('.js-d-hide-2').removeClass('d-none');
                $('.js-delivery-address').find('label').text('Город, улица и номер дома');
                //$('.js-delivery-address').find('input').removeClass('-is_city');

                $('.-is_city').mask('r?rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', { placeholder: "" });
                $('.js-d-hide-1').removeClass('d-none');
                break;
        }
    }


    function calcDelivery() {
        let price = parseFloat($('[name="delivery"]:checked').data('price'));
        let total = parseFloat($('.js-checkout-total').data('total'));
        if (!total) {
            return;
        }
        if (price) {
            total += price;
            label = $('[name="delivery"]:checked').parent().find('.js-delivery-name').html();
            $('.js-delivery-item-name').html(label);
            $('.js-delivery-item-price').text(price + ' руб.');
            $('.js-delivery-item').removeClass('d-none');
        } else {
            $('.js-delivery-item').addClass('d-none');
        }
        $('.js-checkout-total').text(total + ' руб.')
    }
});