require('./bootstrap');
require('./main');
require('./forms');
require('./sliders');
require('./add');

$(document).ready(function() {
    $('body').removeClass('loading');
});