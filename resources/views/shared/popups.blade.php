<div class="modal fade" id="quickModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="{{ route('cart.quick') }}" class="js-quick-form js-form">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="checkout_form_info">
                        <div class="checkout_form_title head_3">Купить в 1 клик</div>
                        <div class="form-group">
                            <label>Имя</label>
                            <input type="text" name="name" class="form-control" placeholder="Марина">
                        </div>
                        <div class="form-group">
                            <label>Телефон</label>
                            <input type="tel" name="phone" class="form-control input-phone" placeholder="+375 (29) 623-29-17">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" name="email" class="form-control" placeholder="example@gmail.com">
                        </div>
                        <div class="form-group">
                            <label>Примечание к заказу</label>
                            <textarea class="form-control" name="comment" rows="8" placeholder="Ваш комментарий"></textarea>
                        </div>
                        @csrf
                        <input type="hidden" name="eid" value="">
                        <input type="hidden" name="qty" value="1">
                        <input type="hidden" name="size" value="">
                        <input type="hidden" name="color" value="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn modal_close" data-dismiss="modal">
                        <span class="ico">
                            @svg('images/svg/close.svg')
                        </span>
                    </button>
                    <button type="submit" class="btn_red w-100">Заказать</button>
                    <div class="popup-error-message error-message d-none">Заполните обязательные поля</div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="quick-success" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog quick-success__wrapper" role="document">
        <div class="modal-content">
            <div class="modal-body quick-success text-center">
                @svg('images/svg/success.svg')
                <div class="quick-success__title">Ваш заказ принят</div>
                <div class="quick-success__text">В ближайшее время с вами свяжется наш менеджер для подтверждения заказа</div>
                <button type="button" class="btn modal_close" data-dismiss="modal">
                    <span class="ico">
                        @svg('images/svg/close.svg')
                    </span>
                </button>
                <a href="{{ route('catalog.index') }}" class="btn_red w-100">В каталог</a>
            </div>

        </div>
    </div>
</div>
@if (\ConfigHelper::isHoliday())
<div class="modal fade" id="vacation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog vacation__wrapper modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body vacation text-center">
                <h2 class="quick-success__title mt-0">{!! \ConfigHelper::get('holiday_title') !!}</h2>
                <div class="quick-success__text">{!! \ConfigHelper::get('holiday_text') !!}</div>
                <button type="button" class="btn modal_close" data-dismiss="modal">
                    <span class="ico">
                        @svg('images/svg/close.svg')
                    </span>
                </button>
                <button type="button" class="btn_red m-0" data-dismiss="modal">
                    OK
                </button>
            </div>

        </div>
    </div>
</div>
@endif
{{-- <div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
                  <div class="table_size_name">Футболка мужская белая </div>
                  <div class="hidden_title dn">Сдвиньте чтобы увидеть больше &nbsp;
                      <span class="scroll_left"><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M0.292892 8.70711C-0.0976315 8.31658 -0.0976315 7.68342 0.292892 7.29289L6.65685 0.928932C7.04738 0.538408 7.68054 0.538408 8.07107 0.928932C8.46159 1.31946 8.46159 1.95262 8.07107 2.34315L2.41421 8L8.07107 13.6569C8.46159 14.0474 8.46159 14.6805 8.07107 15.0711C7.68054 15.4616 7.04738 15.4616 6.65685 15.0711L0.292892 8.70711ZM17 9H1V7H17V9Z" fill="#BDBDBD"></path>
  </svg>
  </span>
                      <span class="scroll_right"><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M16.7071 8.70711C17.0976 8.31658 17.0976 7.68342 16.7071 7.29289L10.3431 0.928932C9.95262 0.538408 9.31946 0.538408 8.92893 0.928932C8.53841 1.31946 8.53841 1.95262 8.92893 2.34315L14.5858 8L8.92893 13.6569C8.53841 14.0474 8.53841 14.6805 8.92893 15.0711C9.31946 15.4616 9.95262 15.4616 10.3431 15.0711L16.7071 8.70711ZM0 9H16V7H0V9Z" fill="#BDBDBD"></path>
  </svg>
  </span></div>
                  <div class="table_wrap">
                                      <table class="table table-bordered">
                                          <tbody><tr>
                                              <th>RU</th>
                                              <th>40</th>
                                              <th>42</th>
                                              <th>44</th>
                                              <th>46</th>
                                              <th>48</th>
                                              <th>50</th>
                                              <th>52</th>
                                              <th>54</th>
                                              <th>56</th>
                                              <th>58</th>
                                              <th>60</th>
                                              <th>62</th>
                                          </tr>
                                          <tr>
                                              <th>EU</th>
                                              <th>3XS</th>
                                              <th>2XS</th>
                                              <th>XS</th>
                                              <th>S</th>
                                              <th>M</th>
                                              <th>L</th>
                                              <th>XL</th>
                                              <th>2XL</th>
                                              <th>3XL</th>
                                              <th>4XL</th>
                                              <th>5XL</th>
                                              <th>6XL</th>
                                          </tr>
                                          <tr class="color-red">
                                              <td><div class="td-desktop">Ширина изделия, см </div><div class="td-mobile">Ширина, см </div></td>
                                              <td>44</td>
                                              <td>45</td>
                                              <td>48</td>
                                              <td>49</td>
                                              <td>50,5</td>
                                              <td>53</td>
                                              <td>55</td>
                                              <td>57,3</td>
                                              <td>58,5</td>
                                              <td>61,5</td>
                                              <td>62</td>
                                              <td>64,5</td>
                                          </tr>
                                          <tr class="color-blue2">
                                              <td><div class="td-desktop">Длина изделия, см</div><div class="td-mobile">Длина, см</div></td>
                                              <td>44</td>
                                              <td>45</td>
                                              <td>48</td>
                                              <td>49</td>
                                              <td>50,5</td>
                                              <td>53</td>
                                              <td>55</td>
                                              <td>57,3</td>
                                              <td>58,5</td>
                                              <td>61,5</td>
                                              <td>62</td>
                                              <td>64,5</td>
                                          </tr>
                                          <tr>
                                              <td><div class="td-desktop">Ростовка, см</div><div class="td-mobile">Рост, см</div></td>
                                              <td>44</td>
                                              <td>45</td>
                                              <td>48</td>
                                              <td>49</td>
                                              <td>50,5</td>
                                              <td>53</td>
                                              <td>55</td>
                                              <td>57,3</td>
                                              <td>58,5</td>
                                              <td>61,5</td>
                                              <td>62</td>
                                              <td>64,5</td>
                                          </tr>
                                      </tbody></table></div>
                  <br>
            <div class="table_size_name">Как определить размер футболки ?</div>
                  <div class="size_table_desc dflex">
                      <div class="size_table_img image"><img src="assets/images/table/img-2.png" alt=""></div>
                      <div class="size_table_content">
                          <p>Советуем выбрать для измерения свою любимую и проверенную кофту, футболку или лонгслив, в котором вам удобно и комфортно. Берем в руки измерительный прибор (линейку, метр или рулетку) и делаем простые измерения.</p>
                          <div class="size_info_items">
                              <div class="size_info_item">
                                  <div class="size_info_title color-blue2"><div class="before bg-blue2">1</div><strong>Измеряем ширину</strong></div>
                                  <p>Раскадываем свою одежду на ровную поверхность и измеряем ее ширину - от края до края в районе груди. </p>
                              </div>
                              <div class="size_info_item">
                                  <div class="size_info_title color-red"><div class="before bg-red">2</div><strong>Измеряем длину</strong></div>
                                  <p>Точно так же, измеряем от горловины до самого низа.</p>
                              </div>
                          </div>
                          <p>Теперь у вас есть 2 цифры: ШИРИНА и ДЛИНА кофты. Соотносим эти показатели с табличкой (накинув или отняв пару-тройку см.) и выбираем свой размер. Погрешность данных в табдлице может составлять до 2 см.</p>
                      </div>
                  </div>
        </div>
          <button type="button" class="btn modal_close" data-dismiss="modal"><span class="ico"><svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M21 21L1 1M1 21L21 1L1 21Z" stroke="#BDBDBD" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>
  </span></button>
      </div>
    </div>
  </div>
 --}}
