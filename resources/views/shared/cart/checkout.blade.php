<div class="col-12 col_cart_wrap">
    <div class="section page_section">
        <div class="section_wrap">
            <div class="cart_wrap">
                <div class="row cart_title_row align-items-center">
                    <div class="col-lg-6">
                        <div class="section_cart_title title dflex align-items-center">
                            <h2 class="head_1">Оформление заказ</h2>
                        </div>
                    </div>
                    <div class="col-lg-6 col-steps">
                        <div class="cart_steps dflex">
                            <div class="cart_step active true">
                                <div class="cart_step_ico">
                                    <svg width="16" height="12" viewBox="0 0 16 12" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M5.42188 11.7644C5.73438 12.0785 6.26562 12.0785 6.57812 11.7644L15.7656 2.5288C16.0781 2.21466 16.0781 1.68063 15.7656 1.36649L14.6406 0.235602C14.3281 -0.078534 13.8281 -0.078534 13.5156 0.235602L6.01562 7.77487L2.48438 4.25654C2.17188 3.94241 1.67188 3.94241 1.35938 4.25654L0.234375 5.38743C-0.078125 5.70157 -0.078125 6.2356 0.234375 6.54974L5.42188 11.7644Z"
                                            fill="#66BD3A" />
                                    </svg>
                                </div>
                                <div class="cart_step_text">Корзина</div>
                            </div>
                            <div class="cart_step active">
                                <div class="cart_step_ico">2</div>
                                <div class="cart_step_text">Оформление заказа</div>
                            </div>
                            <div class="cart_step">
                                <div class="cart_step_ico">3</div>
                                <div class="cart_step_text">Заказ принят</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row_checkout">
                    <div class="col-md-6">
                        <div class="checkout_form">
                            <form action="{{ route('order.store') }}"enctype="multipart/form-data" class="js-checkout-form" method="post">
                                @csrf
                                <div class="checkout_select">
                                    <div class="checkout_form_title head_2">Варианты доставки</div>
                                    @foreach (Delivery::all() as $delivery)
                                    <div class="select">
                                        <input id="delivery-{{ $delivery->id }}" {!! $loop->first ? 'checked="ckecked"' : '' !!} name="delivery" value="{{ $delivery->id }}" type="radio" data-price="{{ $delivery->price }}" class="radio js-delivery" />
                                        <label for="delivery-{{ $delivery->id }}">
                                            <span class="js-delivery-name">{!! $delivery->name !!}</span>
                                            @if ($delivery->hint ?? null)
                                            <span class="ico question" data-toggle="tooltip"
                                                data-placement="top" title=""
                                                data-original-title="{{ $delivery->hint }}">
                                                @svg('images/svg/question.svg')
                                            </span>
                                            @endif
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="checkout_form_info">
                                    <div class="checkout_form_title head_2">Введите ваши данные</div>
                                    <div class="form-group js-field-name-group">
                                        <label>Имя</label>
                                        <input type="text" name="name" class="form-control" required placeholder="Марина">
                                    </div>
                                    <div class="form-group">
                                        <label>Телефон</label>
                                        <input type="tel" name="phone" class="form-control input-phone" required  placeholder="+375 (29) 623-29-17">
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="email" name="email" class="form-control"
                                            placeholder="example@gmail.com">
                                    </div>
                                    <div class="form-group__flex js-d-hide-1 js-d-hide-2 d-none">
                                        <div class="form-group">
                                            <label>Индекс</label>
                                            <input type="text" name="address[index]" class="form-control" placeholder="220124">
                                        </div>
                                        <div class="form-group">
                                            <label>Область</label>
                                            <input type="text" name="address[region]" class="form-control" placeholder="Бресткая">
                                        </div>
                                        <div class="form-group">
                                            <label>Район</label>
                                            <input type="text" name="address[area]" class="form-control" placeholder="Пружанский">
                                        </div>
                                    </div>
                                    <div class="form-group  js-d-hide-1 js-delivery-address d-none">
                                        <label>Адрес доставки</label>
                                        <input type="text" name="address[address]" class="form-control" placeholder="г.Борисов, ул. Притыцкого, д.23, кв.72">
                                    </div>
                                    <div class="form-group">
                                        <label>Примечание к заказу</label>
                                        <textarea class="form-control" name="comment" rows="8"
                                            placeholder="Ваш комментарий"></textarea>
                                    </div>
                                    <div class="button_wrap dflex fs-16">
                                        <a href="{{ route('cart.show') }}" class="btn_silver js-back-cart"><span>назад в корзину</span></a>
                                        <button type="submit" class="btn_red"><span>Заказать</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="your_order">
                            <div class="your_order_title head_2">Ваш заказ</div>
                            <a class="order_edit_link js-back-cart" href="{{ route('cart.show') }}">Изменить</a>
                            <div class="your_order_items">
                                @foreach (\Cart::get()->items as $item)
                                @if (true || ($item->entity ?? null))
                                <div class="your_order_item fs-16">
                                    <div class="your_order_img image">
                                        <a href="{{ $item->url }}">
                                            <img src="{{ $item->image }}" alt="{{ $item->name ?? '' }}">
                                        </a>
                                    </div>
                                    <div class="your_order_content">
                                        <div class="your_order_name">
                                            <a href="{{ $item->url }}">{{ $item->thing_name ?? '' }}</a>
                                        </div>
                                        <div class="your_order_desc fs-18 fw-600">{{ $item->paint_name ?? '' }}</div>
                                        <div class="your_order_footer dflex jbetween">
                                            <span class="cnt color-gray3">{{ $item->qty }} шт.</span>
                                            <span class="item_price fw-600 fs-18 color-red">
                                                {{ $item->priceText ?? '' }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @elseif ($item->product ?? null)
                                <div class="your_order_item fs-16">
                                    <div class="your_order_img image">
                                        <div ">
                                            <img src="{{ \Images::productSmall($item->product->image) }}" alt="{{ $item->product->name }}">
                                        </div>
                                    </div>
                                    <div class="your_order_content">
                                        <div class="your_order_name">
                                            {{-- <div>{{ $item->product->name }}</div> --}}
                                        </div>
                                        <div class="your_order_desc fs-18 fw-600">{{ $item->product->name }}</div>
                                        <div class="your_order_footer dflex jbetween">
                                            <span class="cnt color-gray3">{{ $item->qty }} шт.</span>
                                            <span class="item_price fw-600 fs-18 color-red">
                                                @if ($item->total)
                                                {{ $item->total }} руб.
                                                @else
                                                Договорная
                                                @endif</span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                <div class="your_order_item fs-16 js-delivery-item d-none">
                                    <div class="your_order_img image" style="width: 100px"></div>
                                    <div class="your_order_content d-flex justify-content-between">
                                        <div>
                                            <div class="your_order_name">Доставка</div>
                                            <div class="your_order_desc fs-18 fw-600 js-delivery-item-name"></div>
                                        </div>
                                        <span class="item_price fw-600 fs-18 color-red js-delivery-item-price align-self-end"></span>
                                    </div>

                                </div>
                            </div>
                            <div class="your_order_bottom tright fs-18 fw-600">
                                <span>Сумма заказа:</span>
                                <span class="color-red fs-24 js-checkout-total" data-total="{{ Cart::get()->totalPrice }}">
                                    @if (Cart::get()->items->contains(function ($value, $key) {
                                        return $value->price < 1;
                                    }))
                                    Договорная
                                    @else
                                    {{ Cart::get()->totalPrice }} руб.
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
