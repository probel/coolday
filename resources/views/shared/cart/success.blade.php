<div class="col-12 col_cart_wrap">
    <div class="section page_section">
        <div class="section_wrap">
            <div class="cart_wrap complete_order">
                <div class="row cart_title_row align-items-center">
                    <div class="col-lg-6">
                        <div class="section_cart_title title dflex align-items-center">
                            <h1 class="head_1">Ваш заказ принят</h1>
                        </div>
                    </div>
                    <div class="col-lg-6 col-steps">
                        <div class="cart_steps dflex">
                            <div class="cart_step active true">
                                <div class="cart_step_ico">
                                    <svg width="16" height="12" viewBox="0 0 16 12" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M5.42188 11.7644C5.73438 12.0785 6.26562 12.0785 6.57812 11.7644L15.7656 2.5288C16.0781 2.21466 16.0781 1.68063 15.7656 1.36649L14.6406 0.235602C14.3281 -0.078534 13.8281 -0.078534 13.5156 0.235602L6.01562 7.77487L2.48438 4.25654C2.17188 3.94241 1.67188 3.94241 1.35938 4.25654L0.234375 5.38743C-0.078125 5.70157 -0.078125 6.2356 0.234375 6.54974L5.42188 11.7644Z"
                                            fill="#66BD3A" />
                                    </svg>
                                </div>
                                <div class="cart_step_text">Корзина</div>
                            </div>
                            <div class="cart_step active true">
                                <div class="cart_step_ico">
                                    <svg width="16" height="12" viewBox="0 0 16 12" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M5.42188 11.7644C5.73438 12.0785 6.26562 12.0785 6.57812 11.7644L15.7656 2.5288C16.0781 2.21466 16.0781 1.68063 15.7656 1.36649L14.6406 0.235602C14.3281 -0.078534 13.8281 -0.078534 13.5156 0.235602L6.01562 7.77487L2.48438 4.25654C2.17188 3.94241 1.67188 3.94241 1.35938 4.25654L0.234375 5.38743C-0.078125 5.70157 -0.078125 6.2356 0.234375 6.54974L5.42188 11.7644Z"
                                            fill="#66BD3A" />
                                    </svg>
                                </div>
                                <div class="cart_step_text">Оформление заказа</div>
                            </div>
                            <div class="cart_step active true">
                                <div class="cart_step_ico">
                                    <svg width="16" height="12" viewBox="0 0 16 12" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M5.42188 11.7644C5.73438 12.0785 6.26562 12.0785 6.57812 11.7644L15.7656 2.5288C16.0781 2.21466 16.0781 1.68063 15.7656 1.36649L14.6406 0.235602C14.3281 -0.078534 13.8281 -0.078534 13.5156 0.235602L6.01562 7.77487L2.48438 4.25654C2.17188 3.94241 1.67188 3.94241 1.35938 4.25654L0.234375 5.38743C-0.078125 5.70157 -0.078125 6.2356 0.234375 6.54974L5.42188 11.7644Z"
                                            fill="#66BD3A" />
                                    </svg>
                                </div>
                                <div class="cart_step_text">Заказ принят</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row_checkout fs-16">
                    <div class="col-md-6">
                        <div class="complete_order_wrap">
                            <div class="section_cart_desc dflex align-items-center fs-16">
                                <p>В ближайшее время с вами свяжется наш <br>менеджер для подтверждения
                                    заказа.</p>
                            </div>
                            <div class="operating_time">
                                <div class="operating_time_title fw-600 fs-18">Время работы</div>
                                <p>
                                    @if (Contacts::get('schedule'))
                                    {{ Contacts::get('schedule') }} <br>
                                    @endif
                                    @if (Contacts::get('weekends'))
                                        <span class="color-red">{{ Contacts::get('weekends') }}</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        &nbsp;
                    </div>
                </div>
                <br>
                <br>
                <div class="pt-0">
                    <x-products :items="$entities" :title="'Вам так же могут понравиться'"></x-products>
                </div>
            </div>
        </div>
    </div>
</div>
