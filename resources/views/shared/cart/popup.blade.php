<div class="message">
    <h2 class="main-title">
        <span class="icon-center text-center">Товар добавлен в корзину!</span>
    </h2>
    <div class="d-flex justify-content-between flex-wrap mt-5">
        <button type="button" onclick="$.fancybox.close();" class="btn btn-dark mx-2 flex-grow-1">Продолжить покупки</button>
        <a href="{{ route('cart.show') }}" class="btn mx-2 flex-grow-1">Перейти в корзину</a>
    </div>
</div>
