
<form  action="{{ route('order.store') }}" method="post" class="cart-form js-form__cart-submit">
    <div class="row">
        <div class="col-lg-4 col-md-6">
            @if (site()->deliveries->count())
            <div class="form-group mb-5">
                <label class="form-label mb-3">Выберите способ доставки<sup>*</sup></label>
                @foreach (site()->deliveries as $delivery)
                <label class="checkbox">
                    <input type="radio" name="delivery" data-price="{{ $delivery->price }}" class="" value="{{ $delivery->id }}"  @if ($loop->first) checked="checked" @endif>
                    <span class="checkbox__text">{{ $delivery->name }}</span>
                </label>
                @endforeach
            </div>
            @endif
            <div class="form-group">
                <label class="form-label mb-3">Выберите способ оплаты<sup>*</sup></label>
                <label class="checkbox">
                    <input type="radio" name="pay" class="" value="Оплата наличными курьеру" checked="checked">
                    <span class="checkbox__text">Оплата наличными курьеру</span>
                </label>
                <label class="checkbox">
                    <input type="radio" name="pay" class="" value="Оплата переводом Visa/Mastercard курьеру">
                    <span class="checkbox__text">Оплата переводом Visa/Mastercard курьеру</span>
                </label>
            </div>
        </div>
        <div class="col-lg-8 col-md-6">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label" for="cart-recal-form-name">Ваше имя<sup>*</sup></label>
                        <input class="form-control" id="cart-recal-form-name" type="text" name="name" placeholder="Введите имя" required="required">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label" for="cart-recal-form-tel">Ваше телефон<sup>*</sup></label>
                        <input class="form-control" id="cart-recal-form-tel" type="text" name="tel" placeholder="+7" required="required">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="form-label" for="cart-recal-form-address">Адрес доставки</label>
                        <input class="form-control" id="cart-recal-form-address" type="text" name="address" placeholder="г. Борисов, ул. Пятницкая, д.4, стр.1, этаж 8, кв. 34" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="cart-recal-form-comment">Комментарий к заказу</label>
                        <textarea class="form-control textarea" id="cart-recal-form-comment" type="text" name="comment"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @csrf
    <div class="d-flex buttons__wrapper flex-md-row flex-column justify-content-lg-end justify-content-center align-items-center">
        <a class="btn btn-dark btn-line order-2 order-md-1" href="/"><span>Вернуться на сайт</span></a>
        <button type="submit" class="btn order-1 order-md-2">Оформить заказ</button>
    </div>
</form>
