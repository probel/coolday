<span class="mobile-menu__item_pic cart">
    @svg('images/svg/basket-mobile.svg')
    <span class="mobile-menu__item_pic_count">{{ Cart::get()->totalQty }}</span>
</span>
<span class="mobile-menu__item_text">Корзина</span>
