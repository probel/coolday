<a href="{{ route('cart.show') }}" class="cart_mini_link">
    <span class="ico cart_mini_ico">
        @svg('images/svg/basket-header.svg')
    </span>
    <span class="cart_mini_q">{{ Cart::get()->totalQty }}</span>
</a>

