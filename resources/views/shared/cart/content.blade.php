<div class="col-12 col_cart_wrap">
    <div class="section page_section">
        <div class="section_wrap">
            <div class="cart_wrap">
                @if ($cart->totalQty)
                <div class="row cart_title_row align-items-center">
                    <div class="col-lg-6">
                        <div class="section_cart_title title dflex align-items-center">
                            <h1 class="head_1">Корзина</h1>
                        </div>
                    </div>
                    <div class="col-lg-6 col-steps">
                        <div class="cart_steps dflex">
                            <div class="cart_step active">
                                <div class="cart_step_ico">1</div>
                                <div class="cart_step_text">Корзина</div>
                            </div>
                            <div class="cart_step">
                                <div class="cart_step_ico">2</div>
                                <div class="cart_step_text">Оформление заказа</div>
                            </div>
                            <div class="cart_step">
                                <div class="cart_step_ico">3</div>
                                <div class="cart_step_text">Заказ принят</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cart_items">
                    <div class="cart-list-item">
                        <div class="row-cart-item dflex thead">
                            <div class="col">
                                <div class="cart-name">Товар</div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-product">
                                    <div class="cart-name">Наименования</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-price">
                                    <div class="cart-name">Цена</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-col">
                                    <div class="cart-name">Количество</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-size">
                                    <div class="cart-name">Стоимость</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        @foreach ($cart->items as $item)
                        @if (true || ($item->entity ?? null))
                        <form class="row-cart-item dflex js-form" action="{{ route('cart.change') }}">

                            <div class="col">
                                <div class="image cart-img">
                                    <a href="{{ $item->url }}">
                                        <img src="{{ $item->image }}" alt="{{ $item->thing_name ?? ''}}">
                                    </a>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-product">
                                    <a href="{{ $item->url }}" class="cart-link"><span>{{ $item->thing_name ?? '' }}</span></a>
                                    <div class="no_attr">{{ $item->paint_name ?? '' }}</div>
                                    @if ($item->color ?? null)
                                    <div class="cart-attr color-red">{{ $item->color-> name}}</div>
                                    @endif
                                    @if ($item->size ?? null)
                                    <div class="cart-attr color-red js-change-size-link">{{ $item->size->eu ?? '' }}({{ $item->size->rus ?? '' }})</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-size">
                                    <span class="cart-attr"><span>{{ $item->priceUnit ? $item->priceUnit.' руб.' : 'Договорная' }}</span></span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-col">
                                    <span class="cart-attr">
                                        <span class="product-quantity">
                                            <span class="minus">-</span>
                                            <input type="text" name="qty" class="js-cart-qty" value="{{ $item->qty }}" size="5">
                                            <span class="plus">+</span>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-price">
                                    <span class="cart-attr fw-600 color-red"><span>{{ $item->price ? $item->price.' руб.' : 'Договорная' }}</span></span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item">
                                    <div class="cart-close js-cart-remove">
                                        <div class="ico">
                                            @svg('images/svg/remove.svg')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @csrf
                            <input type="hidden" name="key" value="{{ $item->cartKey }}">
                        </form>
                        @elseif($item->product ?? null)
                        <form class="row-cart-item dflex js-form" action="{{ route('cart.change') }}">
                            <div class="col">
                                <div class="image cart-img">
                                    <img src="{{ Images::productSmall($item->product->image) }}" alt="{{ $item->product->name }}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-product">
                                    <div class="cart-link"><span>{{ $item->product->name }}</span></div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-size">
                                    <span class="cart-attr"><span>{{ $item->product->price }} руб.</span></span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-col">
                                    <span class="cart-attr">
                                        <span class="product-quantity">
                                            <input type="text" class="product-quantity-disabled" disabled value="1" size="5">

                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item name-price">
                                    <span class="cart-attr fw-600 color-red"><span>{{ $item->product->price }} руб.</span></span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="cart-item">
                                    <div class="cart-close js-cart-remove">
                                        <div class="ico">
                                            @svg('images/svg/remove.svg')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @csrf
                            <input type="hidden" name="key" value="{{ $item->key }}">
                        </form>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="cart_footer dflex">
                    <div class="cart_col-left">
                        &nbsp;
                    </div>
                    <div class="cart_col-right">
                        <div class="order_amount fw-600 fs-24">
                            Сумма заказа: <span class="color-red">
                                @if ($cart->items->contains(function ($value, $key) {
                                    return $value->price < 1;
                                }))
                                Договорная
                                @else
                                {{ $cart->totalPrice }} руб.
                                @endif

                            </span>
                        </div>
                    </div>
                </div>
                <div class="cart_footer dflex">
                    <div class="cart_col-left">
                        <div class="link_to_cat dflex jcenter al_item_center">
                            <a href="{{ route('catalog.index') }}" class="btn_all_product">назад в каталог</a>
                        </div>
                    </div>
                    <div class="cart_col-right">
                        <div class="link_to_cat dflex jcenter al_item_center bg-red">
                            <a href="{{ route('cart.checkout') }}" class="add_to_cart js-checkout-link">Оформить заказ</a>
                        </div>
                    </div>
                </div>
                <div class="cart_footer dflex justify-content-end">
                    <div class="bay_click p-0">
                        <a href="#" class="btn btn_blue bay_click_link color-blue2 js-quick-modal js-quick-cart">Купить в 1 клик</a>
                    </div>
                </div>
                @else
                <div class="section_cart_title title dflex align-items-center">
                    <h1 class="head_1">Корзина пуста</h1>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
