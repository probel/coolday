@if ($searchResults ?? null)
@foreach ($searchResults ?? [] as $item)
<div class="search_result_item dflex">
    <a href="{{ $item->url }}" class="result_item_text">{!! $item->name !!}</a>
</div>
@endforeach
<div class="search_result_item dflex mt-1">
    <a href="{{ route('catalog.search',['text'=>$text ?? '']) }}" class="result_more color-blue2">Смотреть все результаты поиска</a>
</div>
<div class="search_result_item dflex last">
    <a href="#" class="result_close_popup color-gray3">
        <span class="ico">
            <svg width="16" height="17" viewBox="0 0 16 17" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path d="M14.3996 14.9001L1.59961 2.1001M1.59961 14.9001L14.3996 2.1001"
                    stroke="#828282" stroke-width="2" stroke-linecap="round"
                    stroke-linejoin="round" />
            </svg>
        </span>
        Закрыть поиск
    </a>
</div>
@endif
