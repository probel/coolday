<footer class="footer">
    <div class="container">
        <div class="footer_colums dflex">
            <div class="footer_col_1 footer_col">
                <div class="footer_col_wrap">
                    <div class="footer_logo">
                        <a href="/" class="img">
                            <span class="ico">
                                @svg('images/svg/logo.svg')
                            </span>
                        </a>
                    </div>
                    <div class="footer_phones">
                        <div class="footer_phone">
                            <x-phone></x-phone>
                            <span class="social_buttons">
                                <x-messagers></x-messagers>
      		                </span>
                        </div>
                        <div class="footer_phone">
                            <x-phone2></x-phone2>
                        </div>
                        @if (Contacts::get('email'))
                        <div class="footer_email">
                            <a class="phone_link" href="mailto:{{ Contacts::get('email') }}" target="blank">{{ Contacts::get('email') }}</a></div>
                        @endif
                    </div>
                    <div class="footer_menu_wrap">
                        <div class="footer_menu_title head_3">Адрес</div>
                        <div class="footer_addr">
                            {!! Contacts::get('address') !!} <br>
                            <a class="addr_map_link" href="{{ Page::find(3)->url }}">Адрес на карте</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_col_2 footer_col">
                <div class="footer_menu_sect dflex">
                    <div class="footer_menu_wrap">
                        <div class="footer_menu_title head_3">Продукция</div>
                        <ul class="footer_menu">
                            @if (Page::find(8))
                            <li class="footer_menu_item"><a href="{{ Page::find(8)->url }}">Каталог принтов</a></li>
                            @endif
                            @if (Page::find(13))
                            <li class="footer_menu_item"><a href="{{ Page::find(13)->url }}">Конструтор</a></li>
                            @endif
                            @if (Page::find(7))
                            <li class="footer_menu_item"><a href="{{ Page::find(7)->url }}">Распродажа</a></li>
                            @endif
                            @if (Page::find(10))
                            <li class="footer_menu_item"><a href="{{ Page::find(10)->url }}">Таблица размеров</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="footer_menu_wrap">
                        <div class="footer_menu_title head_3">Информация</div>
                        <ul class="footer_menu">
                            @if (Page::find(2))
                            <li class="footer_menu_item"><a href="{{ Page::find(2)->url }}">Доставка и оплата</a></li>
                            @endif
                            @if (Page::find(9))
                            <li class="footer_menu_item"><a href="{{ Page::find(9)->url }}">Оптовые цены</a></li>
                            @endif
                            @if (Page::find(6))
                            <li class="footer_menu_item"><a href="{{ Page::find(6)->url }}">FAQ</a></li>
                            @endif
                            @if (\ConfigHelper::get('reviews_link'))
                            <li class="footer_menu_item"><a  target="_blank" href="{{ \ConfigHelper::get('reviews_link') }}">Отзывы</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="footer_menu_wrap">
                        <div class="footer_menu_title head_3">Адрес</div>
                        <div class="footer_addr">
                            {!! Contacts::get('address') !!} <br>
                            <a class="addr_map_link" href="{{ \Page::find(3)->url }}">Адрес на карте</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom dflex jbetween">
			<span class="social_buttons">
                @if (Contacts::get('vk'))
                <a href="{{ Contacts::get('vk') }}" target="_blank">
                    <span class="ico">@svg('images/svg/vk-footer.svg')</span>
                </a>
                @endif
                @if (Contacts::get('instagram'))
                <a href="{{ Contacts::get('instagram') }}" target="_blank">
					<span class="ico">
                        @svg('images/svg/instagram-footer.svg')
					</span>
                </a>
                @endif
      		</span>
            <span class="copyright">{!! \ConfigHelper::get('copyright') !!}</span>
        </div>
    </div>
</footer>
<div class="mobile-menu__container">
    @if (Page::find(8))
    <a href="{{ Page::find(8)->url }}?menu" class="mobile-menu__item {{-- -is-active --}}">
        <span class="mobile-menu__item_pic">
            @svg('images/svg/prints-mobile.svg')
        </span>
        <span class="mobile-menu__item_text">Принты</span>
    </a>
    @endif
    @if (Page::find(13))
    <a href="{{ Page::find(13)->url }}" class="mobile-menu__item">
        <span class="mobile-menu__item_pic">
            @svg('images/svg/edit-mobile.svg')
        </span>
        <span class="mobile-menu__item_text">Свой дизайн</span>
    </a>
    @endif
    {{-- <a href="#" class="mobile-menu__item menu_ico">
        <span class="mobile-menu__item_pic">
            <img src="{{ asset('images/svg/menu-mobile.svg') }}" alt="">
        </span>
        <span class="mobile-menu__item_text">Меню</span>
    </a> --}}
    @if (Page::find(4)->url)
    <a href="{{ Page::find(4)->url }}" class="mobile-menu__item js-mobile-cart-informer">
        @include('shared.cart.informer-mobile')
    </a>
    @endif
</div>
<div class="arrow-top">
    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.70711 0.292892C8.31658 -0.0976315 7.68342 -0.0976315 7.29289 0.292892L0.928932 6.65685C0.538408 7.04738 0.538408 7.68054 0.928932 8.07107C1.31946 8.46159 1.95262 8.46159 2.34315 8.07107L8 2.41421L13.6569 8.07107C14.0474 8.46159 14.6805 8.46159 15.0711 8.07107C15.4616 7.68054 15.4616 7.04738 15.0711 6.65685L8.70711 0.292892ZM9 17L9 1L7 1L7 17L9 17Z" fill="#4F4F4F"/>
    </svg>
</div>
