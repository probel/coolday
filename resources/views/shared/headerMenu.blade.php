<div class="header_menu">
    <div class="container">
        <div class="menu">
            <ul class="menu_wrap menu_inline">
                @if (Page::find(8))
                <li class="menu_item">
                    <a href="{{ Page::find(8)->url }}?menu" class="menu_link bg-red">
                        <span class="menu_link_ico ico">
                            @svg('images/svg/catalog.svg')
                        </span>
                        <span class="menu_link_text">Каталог принтов</span>
                    </a>
                </li>
                @endif
                @if (Page::find(13))
                <li class="menu_item parent">
                    {{-- <!-- добавляется класс bg-red --> --}}
                    <a href="{{ Page::find(13)->url }}" class="menu_link bg-red">
                        <span class="menu_link_ico ico">
                            @svg('images/svg/constructor.svg')
                        </span>
                        <span class="menu_link_text">Свой дизайн</span>
                    </a>
                    {{-- <div class="menu_child">
                        <ul class="menu_child_wrap">
                            <li class="menu_item"><a href="" class="menu_link"><span>link-1-1</span></a></li>
                            <li class="menu_item"><a href="" class="menu_link"><span>link-1-2</span></a></li>
                            <li class="menu_item"><a href="" class="menu_link"><span>link-1-3</span></a></li>
                        </ul>
                    </div> --}}
                </li>
                @endif
                @if (Page::find(9))
                <li class="menu_item"><a href="{{ Page::find(9)->url }}" class="menu_link"><span>Оптовые цены</span></a></li>
                @endif
                @if (Page::find(2))
                <li class="menu_item"><a href="{{ Page::find(2)->url }}" class="menu_link"><span>Доставка и оплата</span></a></li>
                @endif
                @if (Page::find(7))
                <li class="menu_item"><a href="{{ Page::find(7)->url }}" class="menu_link"><span>Распродажа</span></a></li>
                @endif
                @if (Page::find(6))
                <li class="menu_item parent"><a href="{{ Page::find(6)->url }}" class="menu_link"><span>FAQ</span></a></li>
                @endif
                @if (Page::find(3))
                <li class="menu_item parent"><a href="{{ Page::find(3)->url }}" class="menu_link"><span>Контакты</span></a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
