@php
    $breadcrumbs = [
        ['href'=>'/','name' => 'Главная'],
        ['href'=> '','name' => 404],
    ];
    $title = 'Страница не найдена';
@endphp
@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <br><br>
        <div class="row">
            <div class="col-12">
                <div class="section page_section">
                    <div class="section_wrap">
                        <div class="page_404">
                            <div class="page_404_img image tcenter"><img src="{{ asset('images/project/404.png') }}" alt="404"></div>
                            <div class="page_404_title">
                                <h3 class="head_2">Запрошенная вами страница не существует.</h3>
                            </div>
                            <div class="page_404_content">
                                <div class="head_3">Ошибка могла произойти по нескольким причинам:</div>
                                <ul>
                                    <li>Вы ввели неправильный адрес.</li>
                                    <li>Страница, на которую вы хотели зайти, устарела и была удалена.</li>
                                    <li>Акция, ранее действовавшая на сайте, закончилась.</li>
                                    <li>На сервере произошла ошибка. Если так, то мы уже знаем о ней и обязательно
                                        исправим.</li>
                                </ul>

                                <div class="page_prev_wrap">
                                    <a href="/" class="page_prev_link">
                                        <span class="ico page_prev_ico"><svg width="17" height="16" viewBox="0 0 17 16"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M0.292892 8.70711C-0.0976315 8.31658 -0.0976315 7.68342 0.292892 7.29289L6.65685 0.928932C7.04738 0.538408 7.68054 0.538408 8.07107 0.928932C8.46159 1.31946 8.46159 1.95262 8.07107 2.34315L2.41421 8L8.07107 13.6569C8.46159 14.0474 8.46159 14.6805 8.07107 15.0711C7.68054 15.4616 7.04738 15.4616 6.65685 15.0711L0.292892 8.70711ZM17 9H1V7H17V9Z"
                                                    fill="#2D9CDB" />
                                            </svg></span>
                                        &nbsp;<span>Вернуться назад</span></a>
                                </div>
                                <br>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
