<div class="nav_title color-gray3">Выберите предмет:</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link js-slider-filtered-products-link" href="#">Все</a>
    </li>
    @foreach ($types as $type)
    <li class="nav-item ">
        <a class="nav-link js-slider-filtered-products-link {{ $currentType == $type->id ? 'active' : '' }}"
            href="#type-{{ $type->id }}"
            data-tid="{{ $type->id }}">
            {{ $type->name }}
        </a>
    </li>
    @endforeach
</ul>
<div class="catalog_items mx-md-0">
    <div class="carousel__page_product show-init js-slider-filtered-products">
        @foreach ($items ?? [] as $item)
            <div class="catalog_item tcenter {{ $item->id == $current ? 'checked' : '' }}" data-num="{{ $loop->index }}" data-tid="{{ $item->type_id }}">
                <div class="catalog_item_img img">
                    <a href="{{ $item->url }}" class="js-slider-filtered__link">
                        <img src="{{ $item->image }}" alt="{{ $item->alt }}">
                    </a>
                </div>
                <div class="catalog_item_name">
                    <a href="{{ $item->url }}" class="color-gray3">{{ $item->thing_name }}</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
