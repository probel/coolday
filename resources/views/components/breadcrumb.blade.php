@props(['breadcrumbs' => []])
@if (count($breadcrumbs))
<nav aria-label="breadcrumb">
    <ol class="breadcrumb page_breadcrumb">
        @foreach ($breadcrumbs as $link)
            @if (!$loop->last)
            <li class="breadcrumb-item">
                <a href="{{ $link['href'] ?? '#'}}">{{ $link['name'] }}</a>
            </li>
            @else
            <li class="breadcrumb-item active" aria-current="page">
                {{ $link['name'] }}
            </li>
            @endif
        @endforeach
    </ol>
</nav>
<br><br>
@endif


