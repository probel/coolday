@if (Contacts::get('vk'))
<a href="{{ Contacts::get('vk') }}" target="_blank">
    <span class="ico">@svg('images/svg/vk.svg')</span>
</a>
@endif
@if (Contacts::get('instagram'))
<a href="{{ Contacts::get('instagram') }}" target="_blank">
    <span class="ico">@svg('images/svg/instagram.svg')</span>
</a>
@endif
