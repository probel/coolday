@if (Contacts::get('viber'))
<a href="{{ Contacts::get('viber') }}" target="_blank">
    <span class="ico">
        @svg('images/svg/viber.svg')
    </span>
</a>
@endif
@if (Contacts::get('telegram'))
<a href="{{ Contacts::get('telegram') }}" target="_blank">
    <span class="ico">
        @svg('images/svg/telegram.svg')
    </span>
</a>
@endif
@if (Contacts::get('whatsapp'))
<a href="{{ Contacts::get('whatsapp') }}" target="_blank">
    <span class="ico">
        @svg('images/svg/whatsapp.svg')
    </span>
</a>
@endif
