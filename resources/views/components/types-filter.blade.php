<ul class="nav nav-tabs" id="typesTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link js-slider-filtered-things-front-link active" href="#">
            <span class="nav-tabs_ico ico">
                {!! @file_get_contents(public_path('images/svg/all.svg')) !!}
            </span>
            Все предметы
        </a>
    </li>
    @foreach (App\Helpers\TypeHelper::all() as $type)
    <li class="nav-item">
        <a class="nav-link js-slider-filtered-things-front-link" data-tid="{{ $type->id }}" id="type-tab-{{ $type->id }}" href="#type-{{ $type->id }}">
            <span class="nav-tabs_ico ico">
                @if ($type->icon)
                    {!! @file_get_contents(public_path($type->icon)) !!}
                @endif
            </span>
            {{ $type->name }}
        </a>
    </li>
    @endforeach
</ul>
