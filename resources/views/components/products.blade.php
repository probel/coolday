@props(['items' => collect([]), 'title' => 'Вам могут понравится'])
@if ($items->count())
<div class="popular_product">
    <div class="head_wrap">
        <h3 class="head_2">{{ $title }}</h3>
    </div>
    <div class="catalog_items">
        <div class="js-slider-filtered-things">
            @foreach ($items as $item)
                @include('shared.catalog.item')
            @endforeach
        </div>
    </div>
</div>
@endif
