@props(['items' => [], 'all' => true])
@if (count($items))
<div class="faq_items">
    @foreach ($items as $item)
        @if ($all || ($item->show ?? false))
        <div class="faq_item">
            <div class="faq_item_title">
                {!! $item->title ?? '' !!}
                <div class="faq_item_arrow ico">
                    <svg width="12" height="6" viewBox="0 0 12 6"
                        fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 6L0.803849 -9.78799e-07L11.1962 -7.02746e-08L6 6Z"
                            fill="#C4C4C4" />
                    </svg>
                </div>
            </div>
            <div class="faq_item_text">
                {!! $item->description ?? '' !!}
            </div>
        </div>
        @endif
    @endforeach
</div>
@endif


