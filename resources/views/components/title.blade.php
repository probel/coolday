@props(['title' => ''])
@if ($title)
<div class="section_title title dflex align-items-center">
    <h1 class="head_1 m0">{!! $title !!}</h1>
</div>

@endif
