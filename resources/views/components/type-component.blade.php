<ul class="nav nav-tabs" id="typesTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
            role="tab" aria-controls="home" aria-selected="true">
        <span class="nav-tabs_ico ico">
            {!! @file_get_contents(public_path('images/svg/all.svg')) !!}
        </span>
        Все предметы</a>
    </li>
    @foreach ($types as $type)
    <li class="nav-item">
        <a class="nav-link" id="type-tab-{{ $type->id }}" data-type="{{ $type->id }}" data-toggle="tab" href="#type-{{ $type->id }}"  role="tab" aria-controls="type" aria-selected="false">
            <span class="nav-tabs_ico ico">
                @if ($type->icon)
                    {!! @file_get_contents(public_path($type->icon)) !!}
                @endif
            </span>
            {{ $type->name }}
        </a>
    </li>
    @endforeach
</ul>
