@props(['title' => '', 'things'])
<div class="category_list_wrap">
    <div class="container">
        <h3 class="category_list_title">{{ $title }}</h3>
        <div class="row">
            @foreach ($things as $thing)
            <div class="col-md-2 w2">
                <div class="catalog_item">
                    <div class="catalog_item_img img">
                        <a href="{{ $thing->url }}">
                            <img src="{{ $thing->image }}" alt="{{ $thing->name }}">
                        </a>
                    </div>
                    <div class="catalog_item_name"><a href="{{ $thing->url }}">{{ $thing->name }}</a></div>
                    <div class="catalog_item_params">
                        @foreach ($thing->params ?? [] as $param)
                        <div class="item_param">{{ $param->title ?? '' }}: <span>{{ $param->value ?? '' }}</span></div>
                        @endforeach
                        <div class="catalog_item_price head_2">{{ $thing->price }}</div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
