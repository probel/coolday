<div class="aside_wrap">
    <div class="aside_close dn"></div>
    <div class="aside-back">
        <img src="{{ asset('images/svg/aside-arrow.svg') }}" alt="">
        Назад
    </div>
    <aside class="aside">
        <ul class="aside_menu">
            @foreach ($categories->whereNull('parent_id') as $ct)
                @if ($categories->where('parent_id',$ct->id)->count())
                <li class="aside_menu_item {{ ($ct->id == $current || $categories->where('parent_id',$ct->id)->where('id',$current)->count()) ? 'open' : '' }}">
                    <a href="{{ $ct->url }}" class="aside_menu_link js-cat-parent">
                        <span class="aside_menu_text">{{ $ct->name }}</span>
                    </a>
                    <span class="aside_menu_arrow js-cat-parent">
                        @svg('images/svg/caret.svg')
                    </span>
                    <ul class="aside_sub_menu" {!! ($ct->id == $current || $categories->where('parent_id',$ct->id)->where('id',$current)->count()) ? 'style="display: block;"' : '' !!}>
                        <li class="aside_sub_menu_back">
                            <img src="{{ asset('images/svg/aside-arrow.svg') }}" alt="">
                            Каталог
                        </li>
                        @foreach ($categories->where('parent_id',$ct->id) as $child)
                        <li class="aside_sub_item {{ $current == $child->id ? 'current' : '' }}">
                            <a href="{{ $child->url }}" class="aside_menu_link">{{ $child->name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @else
                <li class="aside_menu_item {{ $ct->id == $current ? 'current' : '' }}">
                    <a href="{{ $ct->url }}" class="aside_menu_link">
                        <span class="aside_menu_text">{{ $ct->name }}</span>
                    </a>
                </li>
                @endif
            @endforeach
        </ul>
    </aside>
</div>
