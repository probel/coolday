@if (Contacts::get('phone'))
    <a class="phone_link" href="tel:{{ str_replace(['(',')',' ','-'],'',Contacts::get('phone')) }}" target="blank">{{ Contacts::get('phone') }}</a>
@endif
