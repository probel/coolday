
@if ($paginator->hasPages())
@if ($paginator->hasMorePages())
<div class="add_all_product dflex jcenter al_item_center">
    <a href="{{ $paginator->nextPageUrl() }}" class="btn_all_product js-show-more">показать еще</a>
</div>
@endif
<nav class="navigation">
    <ul class="pagination al_item_center jcenter">
        <li class="page-item">
            @if ($paginator->onFirstPage())
            <a class="page-link disabled">
                <span class="ico pagination_ico">
                    @svg('images/svg/prev.svg')
                </span>
            </a>
            @else
            <a class="page-link" href="{{ $paginator->previousPageUrl() }}">
                <span class="ico pagination_ico">
                    @svg('images/svg/prev.svg')
                </span>
            </a>
            @endif
        </li>
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-item"><span class="page-link border-0">{{ $element }}</span></li>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    <li class="page-item {{ $page == $paginator->currentPage() ? 'active' : ''}}">
                        @if ($page == $paginator->currentPage())
                        <span class="page-link">{{ $page }}</span>
                        @else
                        <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    </li>
                @endforeach
            @endif
        @endforeach
        <li class="page-item">
            @if ($paginator->hasMorePages())
            <a class="page-link" href="{{ $paginator->nextPageUrl() }}">
                <span class="ico pagination_ico">
                    @svg('images/svg/next.svg')
                </span>
            </a>
            @else
            <a class="page-link disabled">
                <span class="ico pagination_ico">
                    @svg('images/svg/next.svg')
                </span>
            </a>
            @endif
        </li>
    </ul>
</nav>
@endif
