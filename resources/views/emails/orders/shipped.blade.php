<h3>Клиент:</h3>
@if($order->name)
Имя: <strong>{{ $order->name }}</strong><br>
@endif
@if($order->phone)
Телефон: <strong>{{ $order->phone }}</strong><br>
@endif
@if($order->email)
Email: <strong>{{ $order->email }}</strong><br>
@endif
@if(count(array_filter($order->address ?? [])))
Адрес доставки: <br><strong>{!! implode('<br>',array_filter($order->address)) !!}</strong><br>
@endif
<br>
@if($order->comment)
Сообщение клиента: <strong>{{ $order->comment }}</strong><br><br>
@endif

<h3>Заказ:</h3>
@if ($order->delivery)
Доставка: <strong>{!! $order->delivery !!}</strong><br>
@endif
<h3>Позиции:</h3>
@include('admin.table.orderItems',['items' => $order->items, 'images' => true])
<hr>
<br>
<h3>Итого: {{ $order->price }} РУБ</h3>

