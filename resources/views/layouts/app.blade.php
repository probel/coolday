<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    {!! \ConfigHelper::get('scripts_header') !!}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ isset($meta['title']) ? strip_tags($meta['title']) : 'CoolDay' }}</title>
    <meta property="og:title" content="{{ isset($meta['title']) ? strip_tags($meta['title']) : 'CoolDay' }}" />
    <meta property="og:type" content="website" />
    @if (isset($meta['url']))
        <meta property="og:url" content="{{ $meta['url'] }}" />
    @endif
    @if (isset($meta['image']))
         <meta property="og:image" content="{{ $meta['image'] }}" />
    @endif
    @if(isset($meta['description']) && $meta['description'])
        <meta property="og:description" content="{{ $meta['description'] }}" />
        <meta content="{{ $meta['description'] }}" name="description"/>
    @endif
    <link rel="icon" type="image/png" href="/favicon.png">
    <link rel="icon" type="image/svg" href="/favicon.svg">
    @if(isset($meta['keywords']) && $meta['keywords'])
        <meta content="{{ $meta['keywords'] }}" name="keywords"/>
    @endif

    <!-- Styles -->

    <style>
        @keyframes loader {
            0% {
                transform: rotate(0deg);
            }
            25% {
                transform: rotate(180deg);
            }
            50% {
                transform: rotate(180deg);
            }
            75% {
                transform: rotate(360deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        @keyframes loader-inner {
            0% {
                height: 0%;
            }
            25% {
                height: 0%;
            }
            50% {
                height: 100%;
            }
            75% {
                height: 100%;
            }
            100% {
                height: 0%;
            }
        }
        body.loading {
            overflow: hidden;
        }

        .loader {
            display: inline-block;
            width: 30px;
            height: 30px;
            position: relative;
            border: 4px solid #BD2937;
            animation: loader 2s infinite ease;
        }

        body.loading >.loader__wrapper {
            display: flex;
        }

        .loader__wrapper {
            display: none;
            width: 100vw;
            height: 100vh;
            justify-content: center;
            align-items: center;
            background-color: rgba(255,255,255,.8);
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1000;
        }

        .loader-inner {
            vertical-align: top;
            display: inline-block;
            width: 100%;
            background-color: #BD2937;
            animation: loader-inner 2s infinite ease-in;
        }


    </style>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="body loading @isset($view){{ $view }} @endisset">
    {!! \ConfigHelper::get('scripts_body_top') !!}
    @include('shared.loader')
    @include('shared.header')
    @include('shared.headerMenu')

    @yield('content')
    @include('shared.footer')
    @include('shared.popups')
    @yield('modals')

    <link href="{{ asset('font-ico/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    @yield('sctipts')
    <script src="{{ mix('js/app.js') }}" ></script>
    {!! \ConfigHelper::get('scripts_body_bottom') !!}
</body>
</html>
