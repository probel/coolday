<{{$component}}
    v-bind:fieldsin = '@json($items)'
    v-bind:prefix = '"{{ $prefix ?? '' }}"'
></{{$component}}>
