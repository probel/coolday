<div class="row">
    <div class="col-md-12">
        <div class="card card-success">
            <div class="card-body">
                <form action="{{ route('import.store') }}" method="post" enctype="multipart/form-data">
                    <div class="row" >
                        <div class="col-md-4 mb-3">
                            <label for="images">Картинки</label>
                            <div class="custom-file1">
                                <input type="file" name="images[]" accept="image/*" class="custom-file-input1" multiple id="images" required>
                                {{-- <label class="custom-file-label1" for="price">Выбрать файл</label> --}}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="article_col">Категория</label>
                                <select name="category_id" id="article_col" required class="form-control">
                                    @foreach (\App\Models\Category::orderBy('order')->get() as $key => $item)
                                    <option {{ $loop->first ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="article_col">Предметы</label>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" checked class="custom-control-input" id="select_all">
                                    <label class="custom-control-label" for="select_all">Выбрать все</label>
                                </div>
                                <hr>
                                @foreach (\App\Models\Thing::orderBy('order')->get() as $key => $item)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="things_ids[]" value="{{ $item->id }}" checked class="custom-control-input select_all-item" id="things-{{ $item->id }}">
                                    <label class="custom-control-label" for="things-{{ $item->id }}">{{ $item->name }}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Импортировать</button>
                </form>
            </div>
        </div>
    </div>
</div>
