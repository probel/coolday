@foreach ($items as $item)
<div style="clear: both; overflow: hidden;">
    @if (($images ?? null))
    <div style="float:left; margin-right: 15px">
        <a href="{{ $item['url'] ?? '' }}"><img src="{{ $item['image'] ?? '' }}" alt=""></a>
    </div>
    @endif
    <div>
        <strong><a href="{{ $item['url'] ?? '' }}">{{ $item['thing_name'] ?? '' }}</a></strong><br>
        <strong>{{ $item['paint_name'] ?? '' }}</strong><br>
        @if ($item['size'] ?? null)
        размер: <strong>{{ $item['size']['eu'] ?? ''}}</strong><br>
        @endif
        @if ($item['color'] ?? null)
        цвет: <strong>{{ $item['color']['name'] ?? '' }}</strong><br>
        @endif
        количество: <strong>{{ $item['qty'] ?? 0 }}</strong>, по цене <strong>{{ $item['priceUnit'] ?? 0 }} РУБ</strong><br>
        на сумму: <strong>{{ $item['price'] ?? 0 }} РУБ</strong>

    </div>
</div>
@if (!$loop->last)
<hr>
@endif
@endforeach
