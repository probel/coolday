@if (Catalog::getSlide())
<section class="first-screen d-flex flex-column justify-content-center position-relative"
    @if (Catalog::getSlide()->image)
        style="background-image:url(/{{ Catalog::getSlide()->image }})"
    @endif
    >
    <div class="container">
        <h1 class="first-screen__title">{!! rv(Catalog::getSlide()->title) !!}</h1>
        @if (Catalog::getSlide()->dedicated)
        <h2 class="first-screen__subtitle w-fit">{!! rv(Catalog::getSlide()->dedicated) !!}</h2>
        @endif
        @if (Catalog::getSlide()->subtitle)
        <h3 class="first-screen__slogan">{!! rv(Catalog::getSlide()->subtitle) !!}</h3>
        @endif
        @if (Catalog::getSlide()->btn_text)
        <a class="btn btn-transparent btn-line" href="{!! rv(Catalog::getSlide()->btn_url) !!}"><span>{!! rv(Catalog::getSlide()->btn_text) !!}</span></a>
        @endif
    </div>
</section>
@endif
