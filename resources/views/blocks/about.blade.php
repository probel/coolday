<section class="about text-block">
    <div class="container">
        {!! rv(site()->front_description ? site()->front_description : ConfigHelper::get('front_description')) !!}
    </div>
</section>
