@extends('layouts.app') @section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="row">
            <div class="col-12">
                <div class="section page_section">
                    <div class="section_wrap">
                        <div class="page_contact">
                            <div class="row">
                                <div class="col-md-3 col-aside-contact">
                                    <div class="section_title">
                                        <h1>{!! $page->title !!}</h1>
                                        <div class="aside_contact">
                                            @if (Contacts::get('address'))
                                            <div class="aside_contact_title">Адрес</div>
                                            <div class="aside_contact_text">
                                                <p>{!! Contacts::get('address') !!}</p>
                                            </div>
                                            @endif
                                            @if ((Contacts::get('phone')) || (Contacts::get('phone2')) )
                                            <div class="aside_contact_title">Телефон</div>
                                            <div class="aside_contact_text">
                                                @if (Contacts::get('phone'))
                                                <a href="tel:{{ str_replace(['(',')',' ','-'],'',Contacts::get('phone')) }}" class="aside_contact_phone">{{ Contacts::get('phone') }}</a>
                                                @endif
                                                @if (Contacts::get('phone2'))
                                                <a href="tel:{{ str_replace(['(',')',' ','-'],'',Contacts::get('phone2')) }}" class="aside_contact_phone">{{ Contacts::get('phone2') }}</a>
                                                @endif
                                            </div>
                                            @endif
                                            @if (Contacts::get('email'))
                                            <div class="aside_contact_title">Email</div>
                                            <div class="aside_contact_text">
                                                <a href="mailto:{{ Contacts::get('email') }}" class="aside_contact_email">{{ Contacts::get('email') }}</a>
                                            </div>
                                            @endif
                                            @if ((Contacts::get('schedule')) || (Contacts::get('weekends')))
                                            <div class="aside_contact_title">Время работы</div>
                                            <div class="aside_contact_info">
                                                <p>
                                                    {!! Contacts::get('schedule') !!} <br>
                                                    <span class="color-red">{!! Contacts::get('weekends') !!}</span>
                                                </p>
                                            </div>
                                            @endif
                                            @if ((Contacts::get('vk')) || (Contacts::get('instagram')))
                                            <div class="aside_contact_title">Мы в соцсетях</div>
                                            <div class="aside_contact_info mt-1">
                                                <span class="social_buttons ml-0">
                                                    <x-social></x-social>
                                                </span>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-content-contact">
                                    <div class="section_map">
                                        <div id="map">
                                            {!! $values->map ?? '' !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contact_list fs-16">
                                <h2 class="head_2">{!! $values->how_title !!}</h2>
                                <div class="contact_items">
                                    @foreach ($values->how ?? [] as $group)
                                    <h2 class="contact_items_title head_3">{!! $group->title ?? '' !!}</h2>
                                    <div class="row">
                                        @foreach ($group->items ?? [] as $item)
                                        <div class="col-3 contact_item_wrap">
                                            <div class="contact_item">
                                                <div class="contact_item_img image">
                                                    <img src="{{ Images::square310($item->image ?? '') }}" alt="{{ $item->title ?? '' }}">
                                                </div>
                                                <div class="contact_item_text">
                                                    {!! $item->title ?? '' !!}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="contact-desc fs-16">
                                {!! $values->legal ?? '' !!}
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
