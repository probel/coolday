@extends('layouts.app')
@section('content')

@if (count($values->slider ?? []))
<div class="home_slider">
    <div class="home_slider_wrap bg-gray2">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($values->slider as $key => $item)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($values->slider as $key => $slide)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="background-image: url({{ \Images::slider($slide->image ?? '') }});">
                    <div class="container">
                        <a @if($slide->link ?? null) href="{{ $slide->link }}" @endif class="slider_link">
                            {{-- <img class="d-block w-100" src="{{ \Images::slider($slide['image'] ?? '') }}" alt="{{ $slide['title'] ?? '' }}"> --}}
                            <div class="carousel-caption carousel-item-wrap tleft">
                                <h3 class="font_size_big carousel-item-title">{{ $slide->title ?? '' }}</h3>
                                <p class="carousel-item-text">{{ $slide->description ?? '' }}</p>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <mask id="path-1-inside-2" fill="white">
                        <path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"/>
                    </mask>
                    <path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z"
                          fill="#BD2937" mask="url(#path-1-inside-2)"/>
                </svg>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <mask id="path-1-inside-1" fill="white">
                        <path d="M0 24C0 37.2548 10.7452 48 24 48C37.2548 48 48 37.2548 48 24C48 10.7452 37.2548 0 24 0C10.7452 0 0 10.7452 0 24Z"/>
                    </mask>
                    <path d="M15.2929 23.2929C14.9024 23.6834 14.9024 24.3166 15.2929 24.7071L21.6569 31.0711C22.0474 31.4616 22.6805 31.4616 23.0711 31.0711C23.4616 30.6805 23.4616 30.0474 23.0711 29.6569L17.4142 24L23.0711 18.3431C23.4616 17.9526 23.4616 17.3195 23.0711 16.9289C22.6805 16.5384 22.0474 16.5384 21.6569 16.9289L15.2929 23.2929ZM32 23H16V25H32V23ZM-2 24C-2 38.3594 9.6406 50 24 50V46C11.8497 46 2 36.1503 2 24H-2ZM24 50C38.3594 50 50 38.3594 50 24H46C46 36.1503 36.1503 46 24 46V50ZM50 24C50 9.6406 38.3594 -2 24 -2V2C36.1503 2 46 11.8497 46 24H50ZM24 -2C9.6406 -2 -2 9.6406 -2 24H2C2 11.8497 11.8497 2 24 2V-2Z"
                          fill="#BD2937" mask="url(#path-1-inside-1)"/>
                </svg>
            </a>
        </div>
    </div>
</div>
@endif

<section class="main_section">
    <div class="container">
        @if (count($values->achievements ?? ''))
        <div class="promo_section">
            <div class="promo dflex">
                @foreach ($values->achievements as $item)
                <div class="promo_item dflex">
                    <div class="promo_ico ico">
                        @if ($item->image ?? '')
                        <img src="{{ asset($item->image) }}" alt="{{ $item->title ?? ''}}">
                        @endif
                    </div>
                    <div class="promo_text_wrap">
                        <span class="promo_title head_3">{!! $item->title ?? ''!!}</span>
                        @if ($item->description ?? null)
                            @foreach (explode("\n", $item->description) as $line)
                            <div class="promo_text">
                                <span>{!! explode('|',$line)[0] ?? '' !!}</span>
                                @if (explode('|',$line)[1] ?? null)
                                <span class="ico question" data-toggle="tooltip" data-placement="top" title="{{ explode('|',$line)[1] ?? '' }}">
                                    @svg('images/svg/question.svg')
                                </span>
                                @endif
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
        @if ($things->count())
        <section class="section section_print">
            <div class="section_wrap">
                <div class="section_title title dflex align-items-center">
                    <h2 class="head_1 m0">{{ $values->things_title ?? '' }}</h2>
                    <span class="line"> | </span>
                    <a href="{{ route('catalog.things') }}" class="section_title_link">Смотреть все >></a>
                </div>
                <div class="section_desc color-gray3">
                    <p>{{ $values->things_description ?? '' }}</p>
                </div>
                <div class="tab_catalog">
                    <x-types-filter></x-types-filter>
                    <div class="tab-content" id="typesTabContent">
                        <div class="catalog_items line">
                            <div class="js-slider-filtered-things-front">
                                @foreach ($things as $thing)
                                <div class="catalog_item" data-num="{{ $loop->index }}" data-tid="{{ $thing->type_id }}">
                                    <div class="catalog_item_img img">
                                        <a href="{{ $thing->url }}">
                                            <img src="{{ $thing->image }}" alt="{{ $thing->name }}">
                                        </a>
                                    </div>
                                    <div class="catalog_item_name"><a href="{{ $thing->url }}">{{ $thing->name }}</a></div>
                                    <div class="catalog_item_params">
                                        @foreach ($thing->params as $param)
                                        <div class="item_param">{{ $param->title ?? '' }}: <span>{{ $param->value ?? '' }}</span></div>
                                        @endforeach
                                        <div class="catalog_item_price head_2">{{ $thing->price }}</div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif

        <section class="section catalog_print">
            <div class="section_wrap">
                @if ($categories->count())
                <div class="section_title title dflex align-items-center">
                    <h2 class="head_1 m0">{{ $values->categories_title ?? '' }}</h2>
                    <span class="line"> | </span>
                    <a href="{{ route('catalog.index') }}" class="section_title_link">Смотреть все >></a>
                </div>
                <div class="section_desc color-gray3">
                    <p>{{ $values->categories_description ?? '' }}</p>
                </div>
                @endif
                <div class="catalog_wrap">
                    @if ($categories->count())
                    <div class="catalog_category_items">
                        <div class="row">
                            @foreach ($categories as $category)
                            <div class="col-sm-6">
                                <div class="catalog_category_item">
                                    <a href="{{ $category->url }}" class="category_item_name">
                                        <div class="category_item_img">
                                            <img src="{{ $category->image }}" class="img-fluid" alt="{{ $category->name }}">
                                        </div>
                                        <span>{{ $category->name }}</span>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    @if ($popular->count())
                    <div class="popular_product">
                        <div class="head_wrap">
                            <h3 class="head_1">{{ $values->popular_title ?? '' }}</h3>
                        </div>
                        <div class="catalog_items line">
                            <div class="carousel_product">
                                @foreach ($popular as $item)
                                    @include('shared.catalog.item')
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @if ($values->description_text ?? null)


                <div class="section desc_cap">
                    <div class="section_wrap">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="desc_cap_content">
                                    <div class="head_wrap">
                                        <h3 class="head_1 desc_cap_title">{{ $values->description_title ?? '' }}</h3>
                                    </div>
                                    <div class="desc_cap_text">{!! $values->description_text ?? '' !!}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if ($values->description_image ?? '')
                                <div class="desc_cap_img img">
                                    <img src="{{ asset($values->description_image) }}" alt="{{ $values->description_title ?? '' }}">
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if (count($values->reviews ?? []))
                <section class="section comment_client">
                    <div class="section_wrap">
                        <div class="section_title title dflex align-items-center">
                            <h3 class="head_1 m0">{{ $values->reviews_title ?? '' }}</h2>
                            @if ($values->reviews_path ?? '')
                            <span class="line"> | </span>
                            <a href="{{ $values->reviews_path }}" target="_blank" class="section_title_link">Смотреть все &gt;&gt;</a>
                            @endif
                        </div>
                        <div class="comments_wrap">
                            <div class="owl-carousel comment_items">
                                @foreach ($values->reviews as $review)
                                <div class="comment_item">
                                    <div class="row_line dflex">
                                        <div class="comment_item_img img">
                                            <img src="{{ Images::review($review->image ?? '') }}" alt="{{ $review->title ?? '' }}">
                                        </div>
                                        <div class="comment_item_content">
                                            <div class="comment_author">{{ $review->title ?? '' }}</div>
                                            <div class="comment_rating">
                                                <div class="rating_stars">
                                                    @for ($i = 0; $i < $review->rate ?? 5; $i++)
                                                    <span class="rating_star">
                                                        <span class="star">
                                                            @svg('images/svg/star.svg')
                                                        </span>
                                                    </span>
                                                    @endfor
                                                </div>
                                            </div>
                                            <div class="comment_text">
                                                {!! $review->description ?? '' !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                @endif
                @if ($values->clients_images ?? '')
                <section class="section section_our_print">
                    <div class="section_wrap">
                        <div class="section_title title dflex align-items-center">
                            <h3 class="head_1 m0">{{ $values->clients_title ?? '' }}</h3>
                        </div>
                        <div class="print_items dflex flex_w">
                            @foreach (explode(',',$values->clients_images) as $img)
                            <div class="print_item">
                                <div class="img">
                                    <img src="{{ asset($img) }}" alt="">
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                @endif
            </div>
        </section>

        <div class="section section_faq">
            <div class="section_wrap">
                @if (count($faq ?? []))
                <div class="section_title title dflex align-items-center">
                    <h3 class="head_1 m0">Часто задаваемые вопросы</h3>
                    @if (Page::find(6))
                    <span class="line"> | </span>
                    <a href="{{ Page::find(6)->url }}" class="section_title_link">Смотреть все &gt;&gt;</a>
                    @endif
                </div>
                <x-faq :items="$faq" :all='false'></x-faq>
                @endif
                @if ($values->instagram_images ?? '')
                <div class="section on_instagram">
                    <div class="section_wrap">
                        <div class="section_title title dflex align-items-center">
                            <h3 class="head_1 m0">{{ $values->instagram_title ?? '' }}</h3>
                            @if ($values->instagram_path ?? null)
                            <span class="line"> | </span>
                            <a href="{{ $values->instagram_path }}" target="_blank" class="section_title_link">Подписаться &gt;&gt;</a>
                            @endif
                        </div>
                        <div class="instagram_items">
                            <div class="owl-carousel instagram_carousel">
                                @foreach (explode(',',$values->instagram_images) as $image)
                                <div class="instagram_item">
                                    <a class="instagram_item_img image">
                                        <img src="{{ Images::instagram($image) }}" alt="">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="section desc_cap">
                    <div class="section_wrap">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="desc_cap_content">
                                    <div class="head_wrap">
                                        <h3 class="head_1 desc_cap_title">{{ $values->description_video_title ?? '' }}</h3>
                                    </div>
                                    <div class="desc_cap_text">{!! $values->description_video_text ?? '' !!}</div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                @if ($values->description_video_path ?? '')
                                <a class="desc_cap_video img d-block" style="background-image: url({{ asset($values->description_video_image ?? '') }})" data-fancybox href="{{ $values->description_video_path }}">
                                    <div class="video_play position-absolute" >
                                        <img src="{{ asset('images/project/play.png') }}">
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>

@endsection
