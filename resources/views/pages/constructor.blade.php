@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="row">
            <div class="col-12">
                <div class="section page_section">
                    <div class="section_wrap">
                        <div class="page_construct">
                            <div class="section_cat_title title dflex align-items-center">
                                <h1 class="head_2">{{ $page->title }}</h1>
                            </div>
                            <div class="section_cat_desc">
                                {!! $values->body ?? '' !!}
                                @if ($values->video ?? null)
                                <p class="fw-600 color-black flex-link">
                                    <span>Смотреть видео</span>
                                    <a href="{{ $values->video }}" data-fancybox target="_blank" class="underline color-blue2 ">Как пользовать конструктором</a>
                                </p>
                                @endif
                            </div>
                            <div class="construct_wrap">
                                @if ($values->constructor ?? null)
                                {!! str_replace('{code}',($thing->code ?? ''),$values->constructor) !!}
                                @else
                                <div class="construct_screen image">
                                    <img src="/images/project/constr.png" alt="i">
                                </div>
                                @endif
                            </div>
                            <p>Если возникли трудности, свяжитесь с нами. Мы обязательно поможем!</p>
                            <div class="header_phones ml-0 fs-18">
                                <div class="header_phone">
                                    <x-phone></x-phone>
                                    <span class="social_buttons">
                                        <x-messagers></x-messagers>
                                    </span>
                                </div>
                                @if (Contacts::get('email'))
                                <a href="mailto:{{ Contacts::get('email') }}" class="header_right_mail ml-0" target="_blank">
                                    <span class="ico_mail ico">
                                        @svg('images/svg/envelope.svg')
                                    </span>
                                    <span class="mail_text">{{ Contacts::get('email') }}</span>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
