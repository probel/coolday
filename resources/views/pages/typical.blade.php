@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <x-title :title="$page->title"></x-title>
        <br>
        <div class="row">
            <div class="col-12">
                {!! $values['body'] ?? '' !!}
            </div>
        </div>
    </div>
</section>
@endsection
