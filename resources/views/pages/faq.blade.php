@extends('layouts.app')
@section('content')

<!-- добавляется класс page -->
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <x-title :title="$page->title"></x-title>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="section section_faq p-0">
                    <div class="section_wrap">
                        <x-faq :items="$page->values->faq ?? []"></x-faq>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
