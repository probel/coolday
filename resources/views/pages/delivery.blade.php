@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <x-title :title="$page->title"></x-title>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="section page_section">
                    <div class="section_wrap">
                        @if (count($values->delivery ?? []))
                        <div class="delivery_wrap">
                            <h2 class="delivery_title head_2 color-red">{{ $values->delivery_title ?? '' }}</h2>
                            <div class="delivery_items">
                                @foreach ($values->delivery as $item)
                                    @if ($item->show ?? null)
                                    <div class="delivery_item dflex">
                                        <div class="delivery_item_ico image">
                                            @if ($item->image ?? null)
                                            <img src="{{ asset($item->image) }}" alt="{{ $item->title ?? '' }}">
                                            @endif
                                        </div>
                                        <div class="delivery_item_content">
                                            <div class="delivery_item_title fw-600 fs-18">{{ $item->title ?? '' }}</div>
                                            <div class="delivery_item_text">
                                                {!! $item->description ?? '' !!}
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif
                        @if (count($values->pay ?? []))
                        <div class="delivery_wrap payments">
                            <h2 class="delivery_title head_2 color-red">{{ $values->pay_title ?? '' }}</h2>
                            <div class="delivery_items">
                                @foreach ($values->pay as $item)
                                    @if ($item->show ?? null)
                                    <div class="delivery_item dflex">
                                        <div class="delivery_item_ico image">
                                            @if ($item->image ?? null)
                                            <img src="{{ asset($item->image) }}" alt="{{ $item->title ?? '' }}">
                                            @endif
                                        </div>
                                        <div class="delivery_item_content">
                                            <div class="delivery_item_title fw-600 fs-18">{{ $item->title ?? '' }}</div>
                                            <div class="delivery_item_text">
                                                {!! $item->description ?? '' !!}
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
