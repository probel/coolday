@extends('layouts.app') @section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <br><br>
        <x-title :title="$page->title"></x-title>
        <br>
        <div class="row">
            <div class="col-12">

                <div class="section section_size_table p-0 fs-16">
                    <div class="section_wrap">
                        {!! $values->body ?? '' !!}
                        <br>
                        <br>
                        <br>
                        <div class="table_size">
                            <div class="table_size_items">
                                @foreach ($things as $thing)
                                    <div class="table_size_item">
                                        <div class="table_size_name">{{ $thing->name }}</div>
                                        <div class="table_size_wrap dflex">
                                            <div class="table_size_img image">
                                                <img src="{{ $thing->image }}" alt="{{ $thing->name }}">
                                            </div>
                                            <div class="table_size_content">
                                                <div class="hidden_title dn">Сдвиньте чтобы увидеть больше &nbsp;
                                                    <span>
                                                        @svg('images/svg/arrow-left.svg')
                                                    </span>
                                                    <span>
                                                        @svg('images/svg/arrow-right.svg')
                                                    </span>
                                                </div>
                                                <div class="table_wrap">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>
                                                                <div class="td-desktop">Российкий р-р</div>
                                                                <div class="td-mobile">RU</div>
                                                            </th>
                                                            @foreach ($thing->sizes as $size)
                                                            <th>{{ $size->rus ?? '' }}</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th>
                                                                <div class="td-desktop">Европейский р-р</div>
                                                                <div class="td-mobile">EU</div>
                                                            </th>
                                                            @foreach ($thing->sizes as $size)
                                                            <th>{{ $size->eu ?? '' }}</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="color-red">
                                                            <td>
                                                                <div class="td-desktop">Ширина изделия, см </div>
                                                                <div class="td-mobile">Ширина, см </div>
                                                            </td>
                                                            </td>
                                                            @foreach ($thing->sizes as $size)
                                                            <th>{{ $size->width ?? '' }}</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr class="color-blue2">
                                                            <td>
                                                                <div class="td-desktop">Длина изделия, см</div>
                                                                <div class="td-mobile">Длина, см</div>
                                                            </td>
                                                            @foreach ($thing->sizes as $size)
                                                            <th>{{ $size->length ?? '' }}</th>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="td-desktop">Ростовка, см</div>
                                                                <div class="td-mobile">Рост, см</div>
                                                            </td>
                                                            @foreach ($thing->sizes as $size)
                                                            <th>{{ $size->height ?? '' }}</th>
                                                            @endforeach
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
