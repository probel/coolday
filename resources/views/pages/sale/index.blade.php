@extends('layouts.app')
@section('content')

<!-- добавляется класс page -->

<!-- добавляется класс page -->
<section class="main_section page pb-0">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <x-title :title="$pg->title"></x-title>
    </div>
    <div class="section category_list sales pt-0">
        <div class="section_wrap">
            <div class="category_list_wrap js-list-wrapper">
                <div class="container">
                    <div class="row js-list">
                        @include('shared.sale.items')
                    </div>
                    <div class="js-list-paginate">
                        @include('shared.sale.paginate')
                    </div>
                </div>
            </div>
        </div>
    </div><!-- category_list -->
</section>

@endsection
