@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <x-title :title="$page->title"></x-title>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="section section_print p-0">
                    <x-types></x-types>
                </div>
            </div>
        </div>
    </div>
    <div class="section category_list opt_price pt-0">
        <div class="section_wrap">
            <div class="category_list_wrap category_list_wrap_opt">
                <div class="container">
                    <div class="tab-content" id="types-tabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row opt_items">
                                <div class="col-12">
                                    @foreach ($things as $item)
                                        @if (count($item->table['ranges'] ?? []))
                                        <div class="opt_item dflex">
                                            <div class="section_title title dflex align-items-center w-100">
                                                <h2 class="head_2 m0">{{ $item->name }}</h2>
                                                <span class="line"> | </span>
                                                <a href="{{ $item->url }}" class="section_title_link">
                                                    <span class="color-red">Заказать &gt;&gt;</span>
                                                </a>
                                            </div>
                                            <div class="opt_item_img image">
                                                <a href="{{ $item->url }}">
                                                    <img src="{{ $item->image }}" alt="{{ $item->name }}">
                                                </a>
                                                <a href="{{ $item->url }}" class="btn_red opt_add_cart"><span>Заказать</span></a>
                                            </div>


                                            <div class="opt_item_content fs-14">
                                                <div class="opt_item_table dflex fw-600">
                                                    <div class="item_table_col">
                                                        Количество
                                                    </div>
                                                    @foreach ($item->table['ranges'] ?? [] as $value)
                                                    <div class="item_table_col">{{ $value }}</div>
                                                    @endforeach
                                                </div>
                                                @if (count($item->table['one'] ?? []))
                                                <div class="opt_item_table dflex">
                                                    <div class="item_table_col">
                                                        <span class="fw-600"><span>Печать</span> 1 сторона</span>
                                                    </div>
                                                    @foreach ($item->table['one'] ?? [] as $value)
                                                    <div class="item_table_col">{{ $value }}</div>
                                                    @endforeach
                                                </div>
                                                @endif
                                                @if (count($item->table['two'] ?? []))
                                                <div class="opt_item_table dflex">
                                                    <div class="item_table_col">
                                                        <span class="fw-600"><span>Печать</span> 2 стороны</span>
                                                        <div class="dn">
                                                            @foreach ($item->table['two'] ?? [] as $value)
                                                            <div class="item_table_col">{{ $value }}</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    @foreach ($item->table['two'] ?? [] as $value)
                                                    <div class="item_table_col">{{ $value }}</div>
                                                    @endforeach
                                                </div>
                                                @endif
                                                @if (count($item->params ?? []))
                                                <div class="opt_item_attrs">
                                                    @foreach ($item->params as $param)
                                                    <div class="opt_item_attr dots dflex">
                                                        <div class="attr_key">{{ $param->title ?? '' }}</div>
                                                        <div class="attr_value fw-600">{{ $param->value ?? '' }}</div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                @endif
                                                <div class="dflex button_wrap">
                                                    <a href="{{ $item->url }}" class="btn_red fs-16 dn"><span>Заказать</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @foreach (\App\Helpers\TypeHelper::all() as $type)
                        <div class="tab-pane fade" id="type-{{ $type->id }}" role="tabpanel" aria-labelledby="type-tab-{{ $type->id }}">
                            <div class="row opt_items">
                                <div class="col-12">
                                    @foreach ($things->where('type_id',$type->id) as $item)
                                        @if (count($item->table['ranges'] ?? []))
                                        <div class="opt_item dflex">
                                            <div class="section_title title dflex align-items-center w-100">
                                                <h2 class="head_2 m0">{{ $item->name }}</h2>
                                                <span class="line"> | </span>
                                                <a href="{{ $item->url }}" class="section_title_link">
                                                    <span class="color-red">Заказать &gt;&gt;</span>
                                                </a>
                                            </div>
                                            <div class="opt_item_img image">
                                                <a href="{{ $item->url }}">
                                                    <img src="{{ $item->image }}" alt="{{ $item->name }}">
                                                </a>
                                                <a href="{{ $item->url }}" class="btn_red opt_add_cart"><span>Заказать</span></a>
                                            </div>
                                            <div class="opt_item_content fs-14">
                                                <div class="opt_item_table dflex fw-600">
                                                    <div class="item_table_col">
                                                        Количество
                                                    </div>
                                                    @foreach ($item->table['ranges'] ?? [] as $value)
                                                    <div class="item_table_col">{{ $value }}</div>
                                                    @endforeach
                                                </div>
                                                @if (count($item->table['one'] ?? []))
                                                <div class="opt_item_table dflex">
                                                    <div class="item_table_col">
                                                        <span class="fw-600"><span>Печать</span> 1 сторона</span>
                                                    </div>
                                                    @foreach ($item->table['one'] ?? [] as $value)
                                                    <div class="item_table_col">{{ $value }}</div>
                                                    @endforeach
                                                </div>
                                                @endif
                                                @if (count($item->table['two'] ?? []))
                                                <div class="opt_item_table dflex">
                                                    <div class="item_table_col">
                                                        <span class="fw-600"><span>Печать</span> 2 стороны</span>
                                                        <div class="dn">
                                                            @foreach ($item->table['two'] ?? [] as $value)
                                                            <div class="item_table_col">{{ $value }}</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    @foreach ($item->table['two'] ?? [] as $value)
                                                    <div class="item_table_col">{{ $value }}</div>
                                                    @endforeach
                                                </div>
                                                @endif
                                                @if (count($item->params ?? []))
                                                <div class="opt_item_attrs">
                                                    @foreach ($item->params as $param)
                                                    <div class="opt_item_attr dots dflex">
                                                        <div class="attr_key">{{ $param->title ?? '' }}</div>
                                                        <div class="attr_value fw-600">{{ $param->value ?? '' }}</div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                @endif
                                                <div class="dflex button_wrap">
                                                    <a href="{{ $item->url }}" class="btn_red fs-16 dn"><span>Заказать</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
