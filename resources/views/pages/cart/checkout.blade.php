@extends('layouts.app')
@section('content')
<section class="main_section page main_section__checkout">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="row js-cart-content">
            @include('shared.cart.checkout')
        </div>
    </div>
</section>
@endsection
