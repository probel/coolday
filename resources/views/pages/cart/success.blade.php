@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="row js-cart-content">
            @include('shared.cart.success')
        </div>
    </div>
</section>
@endsection
