@extends('layouts.app')
@section('content')

<!-- добавляется класс page -->
<section class="main_section page pb-0">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="section_title title dflex align-items-center">
            <h1 class="head_1 m0">{{ $title }}</h1>
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="section section_print p-0">
                    <x-types></x-types>
                </div>
            </div>
        </div>
    </div>
    <div class="section category_list print_cat_page pt-0">
        <div class="tab-content" id="typesTabContent">
            <div class="tab-pane fade show active " id="home" role="tabpanel" aria-labelledby="home-tab">
            @foreach ($things as $group)
                <div class="section_wrap">
                    <x-type-things :title="$group->first()->type_name" :things="$group"></x-type-things>
                </div>
            @endforeach
            </div>
            @foreach ($things as $group)
            <div class="tab-pane fade" id="type-{{ $group->first()->type_id }}" role="tabpanel">
                <div class="section_wrap">
                    <x-type-things :title="$group->first()->type_name" :things="$group"></x-type-things>
                </div>
            </div>
            @endforeach
        </div>

    </div><!-- category_list -->
</section>
@endsection
