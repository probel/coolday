@extends('layouts.app')
@section('content')
<section class="main_section page page_catalog">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="section_cat_title title dflex align-items-center">
            <h1 class="head_2">{{ $title }}</h1>
        </div>
        @if ($description ?? '')
        <div class="section_cat_desc color-gray3">
            {!! $description !!}
        </div>
        @endif
        <div class="open_collection">
            <a href="#" class="btn_all_product">Коллекция принтов</a>
        </div>
        <div class="row">
            <div class="col-sm-3 col-aside">
                <x-aside-menu-component :current="$category->id ?? null"></x-aside-menu-component>
            </div>
            <div class="col-sm-9 col-content">
                <div class="section section_print pt-0">
                    <div class="section_wrap">
                        <div class="tab_catalog">
                            @if ($types ?? null)
                            <x-type-component></x-type-component>
                            @endif
                            <div class="tab-content" id="typesTabContent">
                                <div class="tab-pane fade js-list-wrapper show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="catalog_items mx-0 w-100">
                                        <div class="row js-list">
                                            @include('shared.catalog.items')
                                        </div>
                                    </div>
                                    <div class="js-list-paginate">
                                        @include('shared.catalog.paginate',['items' => $items, 'type' => null])
                                    </div>
                                </div>
                                @foreach ($types ?? [] as $type)
                                <div class="tab-pane fade js-list-wrapper" id="{{ 'type-'.$type->id }}" role="tabpanel" aria-labelledby="">
                                    <div class="catalog_items mx-0 w-100">
                                        <div class="row js-list"></div>
                                    </div>
                                    <div class="js-list-paginate"></div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_cat_desc color-gray3">
            {!! $descriptionFooter ?? '' !!}
        </div>
        <br>
        <br>
        <div class="category_desc">
            <div class="section_wrap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="desc_cap_content p-0">
                            <div class="desc_cap_text fs-16">
                                {!! $footer->footer_text ?? '' !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="desc_cap_img img">
                            @if ($footer->footer_image ?? null)
                            <img src="{{ asset($footer->footer_image) }}" alt="{{ $title ?? '' }}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
    </div>
</section>
@endsection
