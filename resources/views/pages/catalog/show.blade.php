@extends('layouts.app')
@section('content')
<section class="main_section page">
    <div class="container">
        <x-breadcrumb :breadcrumbs="$breadcrumbs ?? []"></x-breadcrumb>
        <div class="section page_section">
            <div class="section_wrap">
                <div class="product_wrap">
                    <div class="row">
                        <div class="col-md-4 col_product_img">
                            <div class="product_cat color-gray2 sm-show">{{ $entity->thing_name }}</div>
                            <h1 class="product_name head_1 sm-show">{{ $entity->paint_name }}</h1>
                            <div class="product-slider show-init">

                                @foreach ($entity->images as $key => $image)
                                <div class="item" data-cid="{{ $image->color ?? '' }}">
                                    <img src="{{ $image->url }}" alt="{{ $image->alt ?? '' }}">
                                </div>
                                @endforeach
                            </div>
                            <div class="product-slider-nav">
                                @foreach ($entity->images as $key => $image)
                                <div class="item {{ $loop->first ? '-is-current' : '' }}" data-cid="{{ $image->color ?? '' }}">
                                    <img src="{{ $image->url }}" alt="{{ $image->alt ?? '' }}">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <form action="{{ route('cart.set') }}" class="col-md-8 d-block col_product_content js-to-cart" method="POST">
                            <div class="product_cat color-gray2 lg-show">{{ $entity->thing_name }}</div>
                            <h2 class="product_name head_1 lg-show">{{ $entity->paint_name }}</h2>
                            <x-related-component :items="$related" :current="$entity->id" :currentType="$entity->type_id"></x-related-component>
                            <div class="tab_catalog">

                                @if ($entity->colors->count())
                                <div class="filter-color">
                                    <span class="color_text">Цвет:</span>
                                    <div class="filter-color__items">
                                        @foreach ($entity->colors as $color)
                                        <div class="filter-color__item" data-color="{{ $color->id }}">
                                            <input class="js-filter-color" title="{{ $color->name }}" value="{{ $color->id }}" data-cid="{{ $color->id }}" data-key="{{ $color->cartKey }}" type="radio" name="color" {!! $loop->first ? 'checked="checked"' : '' !!} name="radio-color">
                                            <span title="{{ $color->name }}" style="background: {{ $color->code }}"></span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                                @if ($entity->sizes->count())
                                <div class="product_size_select">
                                    <div class="product_size_title">Размер:
                                        <a href="#"
                                            class="table_size_popup color-blue2"
                                            data-toggle="modal"
                                            data-target="#sizesModal">
                                            Таблица размеров
                                        </a>
                                    </div>
                                    <div class="product_size_items dflex">
                                        @foreach ($entity->sizes as $key => $size)
                                            @if ($size->status ?? null)
                                            <div data-size={{ $key }}
                                                class="product_size_item js-product_size_item {{ $loop->first ? 'active' : '' }}"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-key="{{ $size->cartKey }}"
                                                title=""
                                                data-original-title="Ширина {{ $size->width ?? '' }} см
                                                Длина {{ $size->length ?? '' }} см
                                                Рост {{ $size->height ?? '' }}см">
                                                <span>{{ $size->eu ?? '' }}({{ $size->rus ?? '' }})</span>
                                            </div>
                                            @endif
                                        @endforeach
                                        <input type="hidden" name="size" class="js-size" value="0">
                                    </div>
                                </div>
                                @endif
                                <div class="product_price dflex align-items-center">
                                    <span class="price_text">Цена:</span>
                                    <span class="price color-red">
                                        <strong class="js-price" data-prices='@json($entity->prices)'>{{ $entity->priceText }}</strong>
                                    </span>
                                    <span class="product-quantity">
                                        <span class="minus">-</span>
                                        <input type="text" value="1" name="qty" class="js-qty" size="5">
                                        <span class="plus">+</span>
                                    </span>
                                    @if ($next = $entity->prices->where('min','>',1)->last())
                                    <a href="{{ Page::find(9)->url }}" target="blank" class="product_sale_link color-blue2">Скидки от {{ $next->min }}шт</a>
                                    @endif
                                </div>
                                <div class="button_wrap dflex align-items-center">
                                    <div class="js-product-btn-add product-btn-add" data-keys='@json(\Cart::get()->items->pluck('key')->toArray())'>
                                        <a href="/cart" class="btn btn_green js-is-added d-none">
                                            <span class="ico">
                                                <svg width="34" height="31" viewBox="0 0 34 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M17.1881 3.5375C17.4816 3.15014 17.7837 2.5919 18.3449 2.60756C19.0707 2.63747 19.5417 3.55174 19.0721 4.13846C17.4344 6.67621 15.7651 9.19686 14.0958 11.7161C13.861 12.0621 13.6048 12.4353 13.2011 12.6004C12.6485 12.6774 12.2605 12.1775 11.9112 11.83C10.7773 10.5398 9.66926 9.22534 8.53398 7.93653C7.99856 7.30138 8.69862 6.30024 9.4631 6.38854C9.89688 6.54946 10.1932 6.92827 10.4982 7.25724C11.2712 8.16296 12.0286 9.08293 12.8174 9.97584C13.0221 9.76222 13.2182 9.54006 13.3915 9.30082C14.647 7.37259 15.9269 5.46145 17.1881 3.5375Z" fill="white"/>
                                                    <path d="M26.6882 0.81064C27.2896 0.446962 28.0139 0.391274 28.6983 0.341268C29.8762 0.334449 31.0552 0.344678 32.2331 0.333313C32.6379 0.352633 33.1506 0.37309 33.386 0.760634C33.6179 1.23341 33.519 2.04487 32.8755 2.10283C31.792 2.18239 30.6994 2.10851 29.6124 2.13465C28.8575 2.16647 27.9889 1.97895 27.3556 2.50401C26.911 2.80632 26.7667 3.35184 26.645 3.84394C25.4899 8.49333 24.3643 13.1507 23.2251 17.8046C23.0545 18.507 22.7782 19.1889 22.3735 19.7912C21.373 21.23 19.6596 22.1176 17.9087 22.1369C13.8111 22.1358 9.71469 22.1335 5.61712 22.1415C5.26921 22.1255 4.88038 22.146 4.5825 21.9392C4.32327 21.6789 4.33009 21.2618 4.40172 20.9265C4.59387 20.4458 5.15324 20.3333 5.61598 20.339C9.74993 20.3367 13.8839 20.3413 18.0178 20.339C19.6573 20.3901 21.124 19.0661 21.4537 17.4978C15.985 17.4693 10.5162 17.4932 5.04751 17.4807C4.39149 17.4853 3.71273 17.2693 3.24431 16.7965C2.8532 16.3931 2.71449 15.8305 2.58147 15.3032C1.93227 12.6643 1.24328 10.0355 0.594078 7.39661C0.519039 7.0318 0.448548 6.64312 0.55201 6.27717C0.755524 5.88621 1.27852 5.69415 1.69237 5.83393C2.03687 5.94645 2.16762 6.32604 2.26312 6.63971C2.95097 9.39456 3.67066 12.1415 4.33692 14.9009C4.41195 15.1554 4.45402 15.4396 4.63707 15.6441C4.84513 15.7112 5.06456 15.7237 5.28172 15.7339C10.8039 15.7396 16.3272 15.718 21.8505 15.7532C22.9465 11.513 23.9197 7.24205 25.0044 2.99952C25.209 2.06192 25.8958 1.3107 26.6882 0.81064Z" fill="white"/>
                                                    <path d="M6.34298 23.8927C7.13884 23.7859 7.99383 23.7915 8.72603 24.1632C9.9437 24.7542 10.7794 26.1259 10.627 27.4886C10.5042 28.8978 9.41388 30.1536 8.03021 30.4616C6.34298 30.9435 4.359 29.9491 3.83146 28.2568C3.57451 27.3317 3.60066 26.2623 4.14298 25.4395C4.61595 24.6439 5.43228 24.0643 6.34298 23.8927ZM6.69202 25.6088C5.95755 25.8065 5.35724 26.5453 5.4482 27.3238C5.49709 27.9557 5.94618 28.5171 6.54195 28.7239C7.56065 29.1626 8.93636 28.2932 8.82039 27.1431C8.86473 26.0759 7.66525 25.3281 6.69202 25.6088Z" fill="white"/>
                                                    <path d="M18.1702 23.9007C19.0763 23.7587 20.0655 23.795 20.8613 24.3008C22.2655 25.1122 22.9806 27.0329 22.2416 28.5183C21.6788 29.7866 20.311 30.6503 18.9205 30.5742C17.4425 30.5333 15.9906 29.5127 15.6348 28.0444C15.114 26.2726 16.3624 24.2496 18.1702 23.9007ZM18.6227 25.5975C17.9041 25.7452 17.3459 26.4158 17.322 27.1477C17.2799 27.8875 17.8416 28.5683 18.5385 28.7683C19.7107 29.1956 21.1285 27.8739 20.6453 26.6794C20.3895 25.8543 19.4401 25.4009 18.6227 25.5975Z" fill="white"/>
                                                </svg>
                                            </span> Перейти в корзину
                                        </a>
                                        <button type="submit" class="btn_red js-is-not-added">
                                            <span class="ico">
                                                @svg('images/svg/basket.svg')
                                            </span> Добавить в корзину
                                        </button>
                                    </div>
                                    <button class="product_one_click color-blue2 btn btn_blue js-quick-modal" data-eid="{{ $entity->id }}">
                                        Купить в 1 клик
                                    </button>
                                </div>
                                <div class="product_single_info color-gray3">
                                    @foreach (Delivery::all() as $delivery)
                                    <div class="single_info_line dflex">
                                        <div class="ico">
                                            @if ($delivery->icon)
                                            <img src="{{ asset($delivery->icon) }}" alt="{{ strip_tags($delivery->name) }}">
                                            @endif
                                        </div>
                                        <div class="info_line_text">
                                            {!! $delivery->name !!}
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @csrf
                                <input type="hidden" name="eid" value="{{ $entity->id }}">
                                <input type="hidden" name="key" value="{{ $entity->cartKey }}">
                            </div>
                        </form>
                    </div>

                    <div class="row row_product_desc">
                        <div class="col-md-4 col_product_desc fs-16">
                            @if ($entity->description)
                            <div class="product_desc_title fw-600 fs-18">Описание</div>
                            {!! $entity->description !!}
                            @endif
                            @if ($entity->rule)
                            <div class="product_desc_title fw-600 fs-18">Правило ухода</div>
                            {!! $entity->rule !!}
                            @endif
                        </div>
                        <div class="col-md-8 col_product_params">
                            @if (count($entity->params ?? []))
                            <div class="product_desc_title fw-600 fs-18">Параметры</div>
                            <div class="opt_item_attrs">
                                @foreach ($entity->params as $param)
                                <div class="opt_item_attr dots dflex">
                                    <div class="attr_key">{{ $param->title ?? '' }}</div>
                                    <div class="attr_value fw-600">{{ $param->value ?? '' }}</div>
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                    <x-products :items="$similar ?? null" :title="'Вам могут понравится'"></x-products>
                    <x-products :items="$latests ?? null" :title="'Вы недавно просматривали'"></x-products>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
</section>
@endsection

@section('modals')
    @if ($entity->sizes->count()))
    <div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="table_size_name">{{ $entity->thing_name }}</div>
                    <div class="hidden_title dn">Сдвиньте чтобы увидеть больше &nbsp;
                        <span class="scroll_left">
                            @svg('images/svg/arrow-left.svg')
                        </span>
                        <span class="scroll_right">
                            @svg('images/svg/arrow-right.svg')
                        </span>
                    </div>
                    <div class="table_wrap table_size">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>RU</th>
                                    @foreach ($entity->sizes as $size)
                                    <th>{{ $size->rus ?? '' }}</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>EU</th>
                                    @foreach ($entity->sizes as $size)
                                    <th>{{ $size->eu ?? '' }}</th>
                                    @endforeach
                                </tr>
                                <tr class="color-red">
                                    <td>
                                        <div class="td-desktop">Ширина изделия, см </div>
                                        <div class="td-mobile">Ширина, см </div>
                                    </td>
                                    @foreach ($entity->sizes as $size)
                                    <th>{{ $size->width ?? '' }}</th>
                                    @endforeach
                                </tr>
                                <tr class="color-blue2">
                                    <td>
                                        <div class="td-desktop">Длина изделия, см</div>
                                        <div class="td-mobile">Длина, см</div>
                                    </td>
                                    @foreach ($entity->sizes as $size)
                                    <th>{{ $size->length ?? '' }}</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>
                                        <div class="td-desktop">Ростовка, см</div>
                                        <div class="td-mobile">Рост, см</div>
                                    </td>
                                    @foreach ($entity->sizes as $size)
                                    <th>{{ $size->height ?? '' }}</th>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    {!! Page::find(10)->values->body ?? '' !!}
                    {!! Page::find(10)->values->body ?? '' !!}
                </div>
                <button type="button" class="btn modal_close" data-dismiss="modal">
                    <span class="ico">
                        @svg('images/svg/close.svg')
                    </span>
                </button>
            </div>
        </div>
    </div>
@endif
@endsection
