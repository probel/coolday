<?php

/* FOR ADMIN */
Auth::routes();
/* disable default register route */
Route::any('register', function () {
    abort(404);
});
Route::any('/home', function () {
    return redirect('/admin');
})->name('home');
/* END FOR ADMIN */

Route::get('/', 'PageController@showFront')->name('front');

Route::get('/image/create', 'ImageController@create')->name('image');
Route::get('/images/products/{id}/{key}/{size}/{cid}.test', 'ImageController@show')->name('image.color');
Route::group(['prefix' => '/sale'], function () {
    Route::any('/', 'SaleController@index')->name('sale.index');
    Route::get('/{slug}', 'SaleController@show')->name('sale.show');
});
Route::group(['prefix' => '/catalog'], function () {
    Route::get('/', 'CatalogController@index')->name('catalog.index');
    Route::any('/things', 'CatalogController@things')->name('catalog.things');
    Route::get('/search', 'CatalogController@search')->name('catalog.search');
    Route::post('/search', 'CatalogController@searchHint')->name('catalog.searchHint');
    Route::any('/{slugs?}', 'CatalogController@resolver')->where('slugs','.+')->name('catalog.resolver');
});

Route::group(['prefix' => '/cart'], function () {
    Route::get('/', 'CartController@show')->name('cart.show');
    Route::get('/checkout', 'CartController@checkout')->name('cart.checkout');
    Route::get('/success', 'CartController@success')->name('cart.success');
    Route::any('/set', 'CartController@set')->name('cart.set');
    Route::any('/change', 'CartController@change')->name('cart.change');
    Route::any('/clear', 'CartController@clear')->name('cart.clear');

    Route::post('/quick', 'OrderController@quick')->name('cart.quick');
    Route::post('/order', 'OrderController@store')->name('order.store');
});
Route::get('/constructor', 'PageController@constructor')->name('constructor');

Route::get('/sitemap.xml', 'SitemapController@index')->name('sitemap');
Route::group(['prefix' => '/sitemaps'], function () {
    Route::get('general.xml', 'SitemapController@general')->name('sitemap.general');
    Route::get('products{part}.xml', 'SitemapController@products')->name('sitemap.products');
});

Route::any('/{slug}', 'PageController@show')->where('slug', '(?!admin).*')->name('page');

