<?php

namespace App\Helpers;

use App\Models\Category;
use App\Helpers\PageHelper;
use Illuminate\Support\Collection;

class CategoryHelper
{
    protected $_categories;

    public function categories()
    {
        if (!is_null($this->_categories)) {
            return $this->_categories;
        }
        $categories = \Cache::get('all_categories');
        if (!$categories) {
            $categories = Category::active()
                            ->orderBy('order')
                            ->toBase()
                            ->get();
            $categories = $categories->map(function ($item, $key) use($categories) {
                $item->path = $this->makePath($item, $categories);
                $item->url = route('catalog.resolver',[$item->path]);
                $item->image = asset($item->image);
                $item->categoryIds = $categories->where('parent_id', $item->id)->pluck('id')->toArray();
                $item->categoryIds[] = $item->id;
                $item->meta = [
                    'title' =>  $item->meta_title,
                    'description' => $item->meta_description,
                    'keywords' => $item->meta_keywords,
                ];
                return $item;
            });
            $categories = $categories->map(function ($item, $key) use($categories) {
                $item->breadcrumbs = $this->makeBreadcrumbs($item, $categories);
                return $item;
            });
            \Cache::put('all_categories', $categories);
        }
        $this->_categories = $categories;
        return $categories;
    }
    /**
     * Возвращает коллекцию категорий
     *
     * @return collection
     */
    public static function all()
    {
        return app(CategoryHelper::class)->categories();
    }

    /**
     * Возвращает категорию по ID
     *
     * @param int $categoryId
     *
     * @return float
     */
    public static function find(int $categoryId)
    {
        $category = app(CategoryHelper::class)
            ->categories()
            ->where('id',$categoryId)
            ->first();
        return $category;
    }

    public static function getForFront($ids)
    {
        if (!count($ids)) {
            return collect([]);
        }
        $categories = app(CategoryHelper::class)
            ->categories()
            ->whereIn('id',$ids);
        return $categories;
    }
    /**
     * Возвращает категорию по URL
     *
     * @param string $slug
     *
     * @return object
     */
    public static function getToSlug(string $slug)
    {
        $category = app(CategoryHelper::class)
            ->categories()
            ->where('path',$slug)
            ->first();
        return $category;
    }

    /**
     * Генерация пути с учетом родителей
     * @param mixed $category
     * @param mixed $categories
     *
     * @return string
     */
    private function makePath($category, $categories)
    {
        $path = '';
        if ($category->parent_id) {
            $parent = $categories->where('id', $category->parent_id)->first();
            $path .= $this->makePath($parent, $categories).'/';
        }
        $path .= $category->slug;
        return $path;
    }

    private function makeBreadcrumbs($category, $categories)
    {
        $page = PageHelper::find(8);
        $breadcrumbs = $page->breadcrumbs;

        if ($category->parent_id) {
            $parent = $categories->where('id',$category->parent_id)->first();
            $breadcrumbs[] = ['href'=> $parent->url,'name' => $parent->name];
        }
        $breadcrumbs[] = ['href'=> $category->url, 'name' => $category->name];
        return $breadcrumbs;
    }
}
