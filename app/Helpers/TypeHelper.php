<?php

namespace App\Helpers;

use App\Models\Type;
use Illuminate\Support\Collection;

class TypeHelper
{
    protected $_types;

    public function types()
    {
        if (!is_null($this->_types)) {
            return $this->_types;
        }
        $types = \Cache::get('all_types');
        if (!$types) {
            $columns = ['id','name','icon'];
            $types = Type::active()
                            ->select($columns)
                            ->orderBy('order')
                            ->toBase()
                            ->get();
            \Cache::put('all_types', $types);
        }
        $this->_types = $types;
        return $types;
    }

    /**
     * Возвращает коллекцию страниц
     *
     * @return collection
     */
    public static function all()
    {
        return app(TypeHelper::class)->types();
    }

}
