<?php

namespace App\Helpers;

use App\Models\ThingPrice;
use Illuminate\Support\Collection;

class PriceHelper
{
    protected $_prices;
    protected $unitPrices;

    public function prices()
    {
        if (!is_null($this->_prices)) {
            return $this->_prices;
        }
        $prices = \Cache::get('all_prices');
        if (!$prices) {
            $columns = ['min','value', 'type', 'thing_id'];
            $prices = ThingPrice::select($columns)
                ->toBase()
                ->get()
                ->values();
            \Cache::put('all_prices', $prices);
        }
        $this->_prices = $prices;
        return $prices;
    }
    public function unitPrices()
    {
        if (!is_null($this->unitPrices)) {
            return $this->unitPrices;
        }

        $prices = \Cache::get('prices_for_unit_all');
        if (!$prices) {
            $columns = ['id', 'min', 'type', 'value', 'thing_id'];
            $prices = ThingPrice::select($columns)
                ->where('min',1)
                ->toBase()
                ->get();
            \Cache::put('prices_for_unit_all', $prices);
        }
        $this->unitPrices = $prices;
        return $prices;

    }
    /**
     * Возвращает коллекцию цен за единицу товара
     *
     * @return collection
     */
    public static function allUnit()
    {
        return app(PriceHelper::class)->unitPrices();
    }

    /**
     * Возвращает цену предмета по его ID, с учетом типа нанесения принта
     *
     * @param int $thingId
     * @param string $type
     *
     * @return float
     */
    public static function findUnit(int $thingId, string $type = 'one')
    {
        $price = app(PriceHelper::class)->unitPrices()
            ->where('thing_id',$thingId)
            ->where('type',$type)
            ->first();
        return $price->value ?? 0;
    }

    /**
     * Возвращает форматированную строку цены предмета
     *
     * @param int $thingId
     * @param string $type
     *
     * @return string
     */
    public static function findUnitText(int $thingId, string $type = 'one')
    {
        $price = self::findUnit($thingId, $type = 'one');
        if ($price) {
            $price .= ' руб.';
        } else {
            $price = 'Договорная';
        }
        return $price;
    }
    /**
     * Возвращает цены по ID предмета
     *
     * @param int $thingId
     * @param string $type
     *
     * @return array
     */
    public static function thingPrices(int $thingId, string $type = 'one')
    {
        $columns = ['min','value'];
        $prices = ThingPrice::select($columns)
            ->where('thing_id',$thingId)
            ->orderBy('min','desc')
            ->where('type',$type)
            ->toBase()
            ->get()
            ->values();
        return $prices;
    }
    public static function thingPricesAll(int $thingId)
    {

        $prices = app(PriceHelper::class)
                    ->prices()
                    ->where('thing_id',$thingId);
        return $prices;
    }
}
