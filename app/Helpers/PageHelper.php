<?php

namespace App\Helpers;

use App\Models\Page;
use Illuminate\Support\Collection;

class PageHelper
{
    protected $_pages;

    public function pages()
    {
        if (!is_null($this->_pages)) {
            return $this->_pages;
        }
        $pages = \Cache::get('all_pages');
        if (!$pages) {
            $pages = Page::active()
                            ->orderBy('order')
                            ->toBase()
                            ->get();
            $pages = $pages->map(function ($item, $key) use($pages) {
                $item->path = $item->slug;
                $item->url = route('page',[$item->path]);
                $item->values = json_decode($item->values);
                $item->meta = [
                    'title' =>  $item->meta_title,
                    'description' => $item->meta_description,
                    'keywords' => $item->meta_keywords,
                ];
                return $item;
            });
            $pages = $pages->map(function ($item, $key) use($pages) {
                $item->breadcrumbs = $this->makeBreadcrumbs($item);
                return $item;
            });
            \Cache::put('all_pages', $pages);
        }
        $this->_pages = $pages;
        return $pages;
    }
    /**
     * Возвращает коллекцию страниц
     *
     * @return collection
     */
    public static function all()
    {
        return app(PageHelper::class)->pages();
    }

    /**
     * Возвращает страницу по ID
     *
     * @param int $pageId
     *
     * @return float
     */
    public static function find(int $pageId)
    {
        $page = app(PageHelper::class)
            ->pages()
            ->where('id',$pageId)
            ->first();
        return $page;
    }

    /**
     * Возвращает страницу по URL
     *
     * @param string $slug
     *
     * @return object
     */
    public static function getToSlug(string $slug)
    {
        $page = app(PageHelper::class)
            ->pages()
            ->where('path',$slug)
            ->first();
        return $page;
    }
    private function makeBreadcrumbs($page)
    {
        $breadcrumbs = [
            ['href'=>'/','name' => 'Главная'],
        ];
        $breadcrumbs[] = ['href'=> $page->url, 'name' => $page->title];
        return $breadcrumbs;
    }
}
