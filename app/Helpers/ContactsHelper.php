<?php

namespace App\Helpers;

use App\Models\Contact;
use Illuminate\Support\Collection;

class ContactsHelper
{
    protected $_contacts;
    public function __construct()
    {
        if (is_null($this->_contacts)) {

            $page = Page::find(3);
            $this->_contacts = $page ? ($page->values ?? []) : [];
        }
    }
    public static function get($name)
    {
        return self::getValue($name);
    }
    private static function getValue($name)
    {
        $page = PageHelper::find(3);
        return $page->values->$name ?? null;
    }

}
