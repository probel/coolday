<?php

namespace App\Helpers;

use App\Models\Config;
use Illuminate\Support\Collection;

class ConfigHelper
{
    protected $_configs;

    public function configs()
    {
        if (!is_null($this->_configs)) {
            return $this->_configs;
        }
        $configs = \Cache::get('all_configs');
        if (!$configs) {
            $columns = ['key','value'];
            $configs = Config::select($columns)
                            ->toBase()
                            ->get();
            \Cache::put('all_configs', $configs);
        }
        $this->_configs = $configs;
        return $configs;
    }

    /**
     * Возвращает коллекцию настроек
     *
     * @return collection
     */
    public static function all()
    {
        return app(ConfigHelper::class)->configs();
    }
    /**
     * Возвращает значение по ключу
     *
     * @param string $key
     *
     * @return object
     */
    public static function get(string $key)
    {
        $item = app(ConfigHelper::class)
            ->configs()
            ->where('key',$key)
            ->first();
        return $item->value ?? null;
    }
    public static function isHoliday()
    {
        $start = static::get('holiday_start');
        $end = static::get('holiday_end');

        if (!$start || !$end) return false;

        $now = date('Y-m-d');
        if ($now >= $start && $now <= $end) return true;

        return false;
    }
}
