<?php

namespace App\Helpers;

use App\Models\Delivery;
use Illuminate\Support\Collection;

class DeliveryHelper
{
    protected $_deliveries;

    public function deliveries()
    {
        if (!is_null($this->_deliveries)) {
            return $this->_deliveries;
        }
        $deliveries = \Cache::get('all_deliveries');
        if (!$deliveries) {
            $columns = ['id','name','icon','price'];
            $deliveries = Delivery::active()
                            ->select($columns)
                            ->orderBy('order')
                            ->toBase()
                            ->get();
            \Cache::put('all_deliveries', $deliveries);
        }
        $this->_deliveries = $deliveries;
        return $deliveries;
    }

    /**
     * Возвращает коллекцию способов доставки
     *
     * @return collection
     */
    public static function all()
    {
        return app(DeliveryHelper::class)->deliveries();
    }
    /**
     * Возвращает доставку по ID
     *
     * @param int $deliveryId
     *
     * @return float
     */
    public static function find(int $deliveryId)
    {
        $delivery = app(DeliveryHelper::class)
            ->deliveries()
            ->where('id',$deliveryId)
            ->first();
        return $delivery;
    }
}
