<?php

namespace App\Helpers;

use App\Models\Color;
use Illuminate\Support\Collection;

class ColorHelper
{
    protected $_colors;

    public function colors()
    {
        if (!is_null($this->_colors)) {
            return $this->_colors;
        }
        $colors = \Cache::get('all_colors');
        if (!$colors) {
            $columns = ['id','name','code'];
            $colors = Color::select($columns)
                ->orderBy('order')
                ->toBase()
                ->get();
            \Cache::put('all_colors', $colors);
        }
        $this->_colors = $colors;
        return $colors;
    }

    /**
     * Возвращает коллекцию цветов
     *
     * @return collection
     */
    public static function all()
    {
        return app(ColorHelper::class)->colors();
    }

    /**
     * Возвращает коллекцию цветов по списку ID
     *
     * @param array $colorIds
     *
     * @return collection
     */
    public static function filtered(array $colorIds)
    {
        $colors = app(ColorHelper::class)->colors()
            ->whereIn('id',$colorIds)
            ->values();
        return $colors;
    }

}
