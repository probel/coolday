<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The order instance.
     *
     * @var Order
     */
    protected $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $title = 'Новый заказ от пользователя '.$order->name;
        //dd($order->items);
        $files = [];
        foreach ($order->items as $key => $item) {
            $paintId = $item['paint_id'] ?? null;
            if (!$paintId ) {
                continue;
            }
            $paint = \App\Models\Paint::find($paintId);
            if ($path = $paint->image_big ?? null) {
                if (strpos($path,'/storage/') !== false) {
                    $files[] = storage_path(str_replace('../storage/','', $path));
                } else {
                    $files[] = public_path($path);
                }
            }
        }

        $message = $this->from(config('mail.from.address'),config('app.name'))->subject($title)->view('emails.orders.shipped')->with([
            'order' => $this->order,
        ]);
        foreach ($files as $key => $file) {
            $message->attach($file);
        }
        return $message;
    }
}
