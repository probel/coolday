<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use \App\Services\AdminService;
use App\Jobs\MakeEntityImagesJob;
/**
 * Class Paint
 *
 * @property \App\Models\Paint $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Paint extends Section implements Initializable
{
    public function initialize()
    {
        $this->title = 'Принты';
        $this->icon = 'fab fa-product-hunt';
        $this->created(function ($config, \Illuminate\Database\Eloquent\Model $model) {
            AdminService::setEntitiesParams($model);
        });
        $this->updated(function ($config, \Illuminate\Database\Eloquent\Model $model) {
            AdminService::setEntitiesParams($model);
        });

    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        /* $paints = \App\Models\Paint::where('id','>',896)->get();
        foreach ($paints as $key => $paint) {
            foreach ($paint->entities as $key => $entity) {
                MakeEntityImagesJob::dispatch($entity->id);
            }
        } */

        $display = AdminDisplay::datatables()

            ->setApply(function ($query)  {
                //dd($request->all());
                //$query->orderBy('order', 'asc');
            })
            ->setActions([
                AdminColumn::action('delete', 'Удалить выбранные')
                    ->setAction(route('admin.delete.selected',['type' => 'Paint'])),

            ])
            ->setOrder([[6, 'asc']])
            ->setColumns([
                AdminColumn::checkbox()->setHtmlAttribute('class', 'text-center'),
                AdminColumn::image('image1','Картинка')->setWidth('90px'),
                AdminColumn::text('name','Название'),
                \AdminColumnEditable::checkbox('status','Доступен', 'Не доступен')
                    ->setLabel('Статус')
                    ->setWidth('120px'),
                AdminColumn::text('category.name','Категория')
                    ->setSearchable(false),
                //AdminColumn::lists('things.name','Предметы')->setWidth('200px'),

                AdminColumn::datetime('updated_at')->setLabel('Дата Изменения')->setFormat('d.m.Y H:i'),
                AdminColumn::custom('Положение', function(\Illuminate\Database\Eloquent\Model $model) {
                    return \App\Services\AdminService::getOrderColumnContent($model,'/admin/paints/');
                })->setWidth('150px')->setOrderable(function($query, $direction) {
                    $query->orderBy('order', $direction);
                })->setSearchable(false),
            ])
            ->setDisplaySearch(true)
            ->paginate(20);
            $display->setView('table_paints');
        /* dd($display->getView());
        dd(get_class_methods($display)); */
       //$display->getActions()->setView('product_actions')->setPlacement('card.heading.actions');
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $paint = $id ? \App\Models\Paint::find($id) : false;

        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function ($id) use($paint)  {
            $tabs = [];

            $elements = [
                AdminFormElement::columns()
                    ->addColumn([AdminFormElement::text('name', 'Название')->required()],5)
                    ->addColumn([
                        AdminFormElement::select('category_id', 'Категория', $this->categoryTree())
                            ->setSortable(false)
                            ->required()
                    ],3)
                    ->addColumn([AdminFormElement::checkbox('duplex', 'Двухсторонний принт')->setDefaultValue(false)->setHelpText('при установке цена на печать будет рассчитываться за две стороны')],3)
                    ->addColumn([AdminFormElement::checkbox('status', 'Доступен')->setDefaultValue(true)],1),
            ];
            /* $elements[] = AdminFormElement::multiselect('things', 'Предметы')
                ->setModelForOptions(\App\Models\Thing::class, 'name')
                ->setDefaultValue($thids)
                ->setSortable(false); */

            /* THINGS */
            $things = \App\Models\Thing::orderBy('name')->get();
            if ($paint) {
                $selected = $paint->things->pluck('id')->toArray();
            } else {
                $selected = $things->pluck('id')->toArray();
            }
            $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'things','title'=>__('Предметы')]);
            $elements[] = AdminFormElement::html('<div class="custom-control custom-checkbox">
                    <input type="checkbox"
                            name="select_all"
                            class="custom-control-input"
                            '.(count($selected) == count($things) ? 'checked="checked"' : '').'
                            id="select_all">
                    <label class="custom-control-label cursor-pointer" for="select_all">Выделить все</label>
                </div>');
            $elements[] = AdminFormElement::html('<hr><div class="row">');
            foreach ($things->split(3) as $key => $group) {
                $elements[] = AdminFormElement::html('<div class="col-md-4">');
                foreach ($group as $key => $thing) {
                    $elements[] = AdminFormElement::html('<div class="custom-control custom-checkbox">
                        <input type="checkbox"
                                name="things['.$thing->id.']"
                                class="custom-control-input select_all-item"
                                '.(in_array($thing->id,$selected) ? 'checked="checked"' : '').'
                                id="things__'.$thing->id.'">
                        <label class="custom-control-label cursor-pointer" for="things__'.$thing->id.'">'.$thing->name.'</label>
                    </div>');
                }
                $elements[] = AdminFormElement::html('</div>');
            }
            $elements[] = AdminFormElement::html('</div>');
            $elements[] = AdminFormElement::view('admin.form.panelClose');
            /* END THINGS */

            $elements[] = AdminFormElement::columns()
                ->addColumn([AdminFormElement::image('image1', 'Картинка 1')],3)
                ->addColumn([AdminFormElement::image('image2', 'Картинка 2')],3)
                ->addColumn([AdminFormElement::image('image3', 'Картинка 3')],3)
                ->addColumn([
                    AdminFormElement::file('image_big', 'Картинка для печати')->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
                        return '../storage/app/paint_big'; // public/files
                    })
                ],3);

            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Основное');
            $elements = [


            ];
            //$tabs[] = AdminService::seoTab();
            return $tabs;
        });
        return AdminForm::form()->addElement($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
    public static function categoryTree(&$tree = [], $prefix='' ,$parent = 0)
    {
        if ($parent) {
            $categories = \App\Models\Category::where('parent_id',$parent)->orderBy('order')->get();
        } else {
            $categories = \App\Models\Category::whereNull('parent_id')->orWhere('parent_id',$parent)->orderBy('order')->get();
        }
        foreach ($categories as $key => $category) {
            $tree[$category->id] = $prefix.$category->name;
            if (\App\Models\Category::where('parent_id',$category->id)->count()) {
                self::categoryTree($tree,'- ',$category->id);
            }
        }

        return $tree;
    }
}
