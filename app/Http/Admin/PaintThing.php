<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use \App\Services\AdminService;
/**
 * Class PaintThing
 *
 * @property \App\Models\PaintThing $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class PaintThing extends Section implements Initializable
{
    public function initialize()
    {
        $this->title = 'Товары';
        $this->icon = 'fas fa-compress-alt';
        $this->created(function ($config, \Illuminate\Database\Eloquent\Model $model) {

        });
        $this->updated(function ($config, \Illuminate\Database\Eloquent\Model $model) {

        });

    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = AdminDisplay::datatables()

            ->setApply(function ($query)  {
                //dd($request->all());
                //$query->orderBy('order', 'asc');
            })
            ->setActions(
                AdminColumn::action('delete', 'Удалить выбранные')
                    ->setAction(route('admin.delete.selected',['type' => 'PaintThing']))
            )
            ->setOrder([[6, 'desc']])
            ->setColumns([
                AdminColumn::checkbox()->setHtmlAttribute('class', 'text-center'),
                AdminService::nameColumn(),
                AdminColumn::relatedLink('thing.name','Предмет'),
                AdminColumn::relatedLink('paint.name','Принт'),
                AdminColumn::custom('Цена', function ($model) {
                    return $model->getPrice();
                }),
                AdminService::seoColumn(),
                AdminColumn::datetime('updated_at')->setLabel('Дата Изменения')->setFormat('d.m.Y H:i'),
            ])
            ->setDisplaySearch(true)
            ->paginate(20);

        //$display->getActions()->setView('paintthing_actions')->setPlacement('panel.heading.actions');
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $paintthing = $id ? \App\Models\PaintThing::find($id) : false;

        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function ($id) use($paintthing)  {
            $tabs = [];
            $elements = [
                AdminFormElement::columns()
                    ->addColumn([AdminFormElement::text('name', 'Название')->required()],6),
            ];

            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Основное');

            $tabs[] = AdminService::seoTab();
            return $tabs;
        });
        return AdminForm::form()->addElement($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
