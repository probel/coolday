<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Services\AdminService;

/**
 * Class Delivery
 *
 * @property \App\Models\Delivery $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Delivery extends Section implements Initializable
{
    public function initialize()
    {
        $this->title = 'Доставка';
        $this->icon = 'fa fa-truck';

        $this->created(function ($config, \Illuminate\Database\Eloquent\Model $model) {

        });
        $this->updated(function ($config, \Illuminate\Database\Eloquent\Model $model) {

        });
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[6, 'asc']])
            ->setColumns([
                AdminColumn::image('icon','Иконка')->setWidth('100px'),
                AdminColumn::text('name','Название'),
                AdminColumn::text('price','Цена'),
                \AdminColumnEditable::checkbox('status', 'Доступнен', 'Не доступнен')->setWidth('140px'),
                AdminColumn::datetime('created_at')->setLabel('Дата Создания')->setFormat('d.m.Y H:i'),
                AdminColumn::datetime('updated_at')->setLabel('Дата Изменения')->setFormat('d.m.Y H:i'),
                AdminColumn::custom('Положение', function(\Illuminate\Database\Eloquent\Model $model) {
                    return \App\Services\AdminService::getOrderColumnContent($model,'/admin/deliveries/');
                })->setWidth('150px')->setOrderable(function($query, $direction) {
                    $query->orderBy('order', $direction);
                })->setSearchable(false),
            ])
            ->setDisplaySearch(true)
            ->paginate(30);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $model = $id ? \App\Models\Delivery::find($id) : null;
        //dd($model->values);
        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function () use($model) {

            $tabs = [];
            $fields =  [
                AdminFormElement::text('name', 'Название')->required(),
                AdminFormElement::text('hint', 'Подсказка'),
                AdminFormElement::number('price', 'Цена'),
                AdminFormElement::checkbox('status','Доступен')->setDefaultValue(true),
                AdminFormElement::image('icon','Иконка'),
            ];

            $tabs[] = AdminDisplay::tab(AdminForm::elements($fields))->setLabel('Содержимое');

            return $tabs;
        });
        $form = AdminForm::panel()->addHeader($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }
    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }
}
