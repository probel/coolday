<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;
use App\Services\AdminService;

/**
 * Class Pages
 *
 * @property \App\Models\Page $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Page extends Section implements Initializable
{

    public function initialize()
    {
        $this->title = 'Страницы';
        $this->icon = 'fa fa-file-alt';
        $this->created(function ($config, \Illuminate\Database\Eloquent\Model $model) {
            AdminService::setPageValues($model);
        });
        $this->updated(function ($config, \Illuminate\Database\Eloquent\Model $model) {
            AdminService::setPageValues($model);
        });

    }
    public function isCreatable()
    {
        return true;
    }

    public function isDeletable(Model $model)
    {
        return $model->view == 'typical' && $model->id > 10;
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setApply(function ($query) {})
            ->setOrder([[0, 'asc']])
            ->setColumns([
                AdminColumn::text('id', '#'),
                AdminColumn::custom('Заголовок' ,function ($model)
                {
                    return '<a href="'.$model->getUrl().'" target="_blank">'.
                        ($model->admin_title ?? $model->name).
                    '</a>';
                }),
                \AdminColumnEditable::checkbox('status','Доступна', 'Не доступна')
                    ->setLabel('Статус')
                    ->setWidth('120px'),
                AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('d.m.Y H:i'),
            ])
            ->setDisplaySearch(true)
            ->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $page = $id ? \App\Models\Page::find($id) : null;
        $tabs = AdminDisplay::tabbed();

        $tabs->setTabs(function () use($page) {

            $tabs = [];
            $adminTitle = AdminFormElement::text('admin_title', 'Административный заголовок');
            $simpleTitle = AdminFormElement::text('title', 'Заголовок')->required(true);
            $elements = [];
            \array_unshift($elements,$simpleTitle);
            if ($page) {
                switch ($page->type) {
                    case 'front':
                        $elements = [
                            AdminFormElement::view('admin.form.panelOpen',['key'=>'slider','title'=>__('Слайдер')]),
                            AdminFormElement::view('admin.slider',['items' => $page->values['slider'] ?? [],'prefix'=>'values[slider]']),
                            AdminFormElement::view('admin.form.panelClose'),
                        ];
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'achievements','title'=>__('Преимущества')]);
                        $elements[] = AdminFormElement::view('admin.form.component',[
                            'component' => 'achievements-component',
                            'items' => $page->values['achievements'] ?? [],
                            'prefix'=>'values[achievements]'
                        ]);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'things','title'=>__('Предметы для печати')]);
                        $elements[] = AdminFormElement::text('values[things_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['things_title'] ?? '');
                        $elements[] = AdminFormElement::text('values[things_description]', 'Описание')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['things_description'] ?? '');
                        $elements[] = AdminFormElement::multiselect('values[things]', 'Предметы')
                                        ->setModelForOptions(\App\Models\Thing::class, 'name')
                                        ->setSortable(false)
                                        ->setValueSkipped(true)
                                        ->setDefaultValue(implode(',',$page->values['things'] ?? []));
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'categories','title'=>__('Принты')]);
                        $elements[] = AdminFormElement::text('values[categories_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['categories_title'] ?? '');
                        $elements[] = AdminFormElement::text('values[categories_description]', 'Описание')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['categories_description'] ?? '');
                        $elements[] = AdminFormElement::multiselect('values[categories]', 'Категории')
                                        ->setModelForOptions(\App\Models\Category::class, 'name')
                                        ->setSortable(false)
                                        ->setValueSkipped(true)
                                        ->setDefaultValue(implode(',',$page->values['categories'] ?? []));
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'popular','title'=>__('Популярные товары')]);
                        $elements[] = AdminFormElement::text('values[popular_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['popular_title'] ?? '');
                        $elements[] = AdminFormElement::multiselect('values[popular]', 'Популярные')
                                        ->setModelForOptions(\App\Models\PaintThing::class, 'name')
                                        ->setSortable(false)
                                        ->setValueSkipped(true)
                                        ->setDefaultValue(implode(',',$page->values['popular'] ?? []));
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'description','title'=>__('Описание')]);
                        $elements[] = AdminFormElement::text('values[description_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_title'] ?? '');
                        $elements[] = AdminFormElement::ckeditor('values[description_text]', 'Текст')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_text'] ?? '');
                        $elements[] = AdminFormElement::file('values[description_image]', 'Картинка')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_image'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'reviews','title'=>__('Отзывы')]);
                        $elements[] = AdminFormElement::text('values[reviews_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['reviews_title'] ?? '');
                        $elements[] = AdminFormElement::text('values[reviews_path]', 'Адрес ссылки')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['reviews_path'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.component',[
                            'component' => 'reviews-component',
                            'items' => $page->values['reviews'] ?? [],
                            'prefix'=>'values[reviews]'
                        ]);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'clients','title'=>__('Клиенты')]);
                        $elements[] = AdminFormElement::text('values[clients_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['clients_title'] ?? '');
                        $elements[] = AdminFormElement::images('values[clients_images]', 'Картинки')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['clients_images'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'instagram','title'=>__('Инстаграм')]);
                        $elements[] = AdminFormElement::text('values[instagram_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['instagram_title'] ?? '');
                        $elements[] = AdminFormElement::text('values[instagram_path]', 'Адрес ссылки')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['instagram_path'] ?? '');
                        $elements[] = AdminFormElement::images('values[instagram_images]', 'Картинки')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['instagram_images'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.panelClose');

                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'description_video','title'=>__('Описание с видео')]);
                        $elements[] = AdminFormElement::text('values[description_video_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_video_title'] ?? '');
                        $elements[] = AdminFormElement::ckeditor('values[description_video_text]', 'Текст')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_video_text'] ?? '');
                        $elements[] = AdminFormElement::text('values[description_video_path]', 'Ссылка на видео')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_video_path'] ?? '');
                        $elements[] = AdminFormElement::image('values[description_video_image]', 'Картинка')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['description_video_image'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        break;
                    case 'constructor':
                        $elements[] = AdminFormElement::ckeditor('values[body]', 'Описание')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['body'] ?? '');
                        $elements[] = AdminFormElement::text('values[video]', 'Адрес ссылки видео')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['video'] ?? '');
                        $elements[] = AdminFormElement::textarea('values[constructor]', 'Код конструктора')
                                        ->setValueSkipped(true)
                                        ->setRows(3)
                                        ->setDefaultValue($page->values['constructor'] ?? '');
                        break;
                    case 'delivery':
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'delivery','title'=>__('Доставка')]);
                        $elements[] = AdminFormElement::text('values[delivery_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['delivery_title'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.component',[
                            'component' => 'delivery-component',
                            'items' => $page->values['delivery'] ?? [],
                            'prefix'=>'values[delivery]'
                        ]);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'pay','title'=>__('Оплата')]);
                        $elements[] = AdminFormElement::text('values[pay_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['pay_title'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.component',[
                            'component' => 'delivery-component',
                            'items' => $page->values['pay'] ?? [],
                            'prefix'=>'values[pay]'
                        ]);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        break;
                    case 'faq':
                        /* FAQ */
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'faq','title'=>__('FAQ')]);
                        $elements[] = AdminFormElement::view('admin.form.component',[
                            'component' => 'faq-component',
                            'items' => $page->values['faq'] ?? [],
                            'prefix'=>'values[faq]'
                        ]);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');

                        break;
                    case 'contacts':
                        /* Contacts */

                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'contacts','title'=>__('Контакты')]);
                        $elements[] = AdminFormElement::text('values[address]', 'Адрес')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['address'] ?? '');
                        $elements[] = AdminFormElement::text('values[phone]', 'Телефон')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['phone'] ?? '');
                        $elements[] = AdminFormElement::text('values[phone2]', 'Телефон 2')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['phone2'] ?? '');
                        $elements[] = AdminFormElement::text('values[email]', 'E-mail')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['email'] ?? '');
                        $elements[] = AdminFormElement::text('values[schedule]', 'Рабочие дни')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['schedule'] ?? '');
                        $elements[] = AdminFormElement::text('values[weekends]', 'Выходные дни')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['weekends'] ?? '');
                        $elements[] = AdminFormElement::text('values[vk]', 'Ссылка в контакте')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['vk'] ?? '');
                        $elements[] = AdminFormElement::text('values[instagram]', 'Ссылка инстаграм')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['instagram'] ?? '');
                        $elements[] = AdminFormElement::text('values[viber]', 'Ссылка вайбер')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['viber'] ?? '');
                        $elements[] = AdminFormElement::text('values[telegram]', 'Ссылка телеграм')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['telegram'] ?? '');
                        $elements[] = AdminFormElement::text('values[whatsapp]', 'Ссылка WhatsApp')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['whatsapp'] ?? '');
                        $elements[] = AdminFormElement::textarea('values[map]', 'Код карты')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['map'] ?? '')
                                        ->setRows(3);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'how','title'=>__('Как найти')]);
                        $elements[] = AdminFormElement::text('values[how_title]', 'Заголовок')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['how_title'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.component',[
                            'component' => 'how-component',
                            'items' => $page->values['how'] ?? [],
                            'prefix'=>'values[how]'
                        ]);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'legal','title'=>__('Юридическая информация')]);
                        $elements[] = AdminFormElement::textarea('values[legal]', 'Юридическая информация')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['legal'] ?? '')
                                        ->setRows(7);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');

                        break;
                    case 'sale':
                        $elements[] = AdminFormElement::number('values[paginate]', 'Количество элементов на страницу')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['paginate'] ?? 25);
                        break;
                    case 'catalog':
                        $elements[] = AdminFormElement::number('values[paginate]', 'Количество элементов на страницу')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['paginate'] ?? 24);
                        $elements[] = AdminFormElement::ckeditor('values[body]', 'Описание')
                                        ->setValueSkipped(true)
                                        ->setDefaultValue($page->values['body'] ?? '');
                        $elements[] = AdminFormElement::view('admin.form.panelOpen',['key'=>'footer','title'=>__('Подвал')]);
                        $elements[] = AdminFormElement::columns()
                                        ->addColumn([
                                            AdminFormElement::ckeditor('values[footer_text]', 'Описание в подвале')
                                                                ->setValueSkipped(true)
                                                                ->setDefaultValue($page->values['footer_text'] ?? '')
                                        ],9)
                                        ->addColumn([
                                            AdminFormElement::image('values[footer_image]', 'Картинка')
                                                ->setValueSkipped(true)
                                                ->setDefaultValue($page->values['footer_image'] ?? '')
                                        ],3);
                        $elements[] = AdminFormElement::view('admin.form.panelClose');
                        break;
                    case 'wholesale':
                    case 'seo':

                        break;

                    default:
                        $elements = [
                            AdminFormElement::ckeditor('values[body]', 'Содержимое')
                                ->setValueSkipped(true)
                                ->setDefaultValue($page->values['body'] ?? ''),
                        ];
                        \array_unshift($elements,$simpleTitle);
                        \array_unshift($elements,$adminTitle);
                        break;
                }
            }

            if (count($elements)) {
                $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Содержимое');
            }

            $tabs[] = AdminService::seoTab((!$page || $page->type == 'typical'));

            return $tabs;
        });
        $form = AdminForm::panel()->addHeader($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
