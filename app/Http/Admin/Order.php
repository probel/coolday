<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;

/**
 * Class Order
 *
 * @property \App\Models\Order $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Order extends Section implements Initializable
{
    public function initialize()
    {
        $this->title = 'Заказы';
        $this->icon = 'fa fa-shopping-cart';

    }
    public function isCreatable()
    {
        return false;
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[0, 'desc']])
            ->setColumns([

                AdminColumn::text('id', '#'),

                AdminColumn::custom('Клиент', function ($order) {
                    $res = '';
                    $res.= 'ФИО: ' .$order->name.'<br>';
                    $res.= 'Телефон: ' .$order->phone.'<br>';
                    if($order->email)
                        $res.= 'Email: ' .$order->email.'<br>';

                    $res.= 'комментарий: ' .$order->comment;
                    return $res;
                }),
                AdminColumn::custom('Адрес', function ($order) {
                    $res = '';
                    if(count(array_filter($order->address ?? []))) {
                        $res.= 'Адрес: ' .implode('<br>',array_filter($order->address)).'<br>';
                    }
                    return $res;
                }),

                AdminColumn::custom('Заказ', function ($order) {
                    return view('admin.table.orderItems',['items'=>$order->items]);
                }),
                AdminColumn::text('price', 'Сумма'),
                AdminColumn::custom('Доставка', function ($order) {
                    return $order->delivery;
                })->setWidth('200px'),
                AdminColumn::datetime('created_at')->setLabel('Дата Создания')->setFormat('d.m.Y H:i'),
            ])
            ->setDisplaySearch(true)
            ->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */



    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
