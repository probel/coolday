<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use \App\Services\AdminService;
/**
 * Class Thing
 *
 * @property \App\Models\Thing $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Thing extends Section implements Initializable
{
    public function initialize()
    {
        $this->title = 'Предметы';
        $this->icon = 'fab fa-product-hunt';
        $this->created(function ($config, \Illuminate\Database\Eloquent\Model $model) {
            $this->setValues($model);
            AdminService::setEntitiesParams($model);
        });
        $this->updated(function ($config, \Illuminate\Database\Eloquent\Model $model) {
            $this->setValues($model);
            //AdminService::setEntitiesParams($model);
        });

    }
    private function setValues(&$model)
    {
        $allow = [
            'save_and_continue',
            'save_and_create',
            'save_and_close'
        ];
        if (in_array(request()->next_action, $allow)) {
            $params = request()->params ?? [];
            $model->params = $params;
            $sizes = request()->sizes ?? [];
            $model->sizes = $sizes;
            $images = request()->images ?? [];
            $model->images = $images;
            $model->save();
        }

    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = AdminDisplay::datatables()



            ->setOrder([[7, 'asc']])
            ->setActions(
                AdminColumn::action('delete', 'Удалить выбранные')
                    ->setAction(route('admin.delete.selected',['type' => 'Thing']))
            )
            ->setColumns([
                AdminColumn::checkbox()->setHtmlAttribute('class', 'text-center'),
                AdminColumn::image('teaser_image','Картинка')->setWidth('90px'),
                AdminService::nameColumn(),
                \AdminColumnEditable::checkbox('status','Доступен', 'Не доступен')
                    ->setLabel('Статус')
                    ->setWidth('120px'),
                AdminService::seoColumn(),
                //AdminColumn::text('price','Цена')->setWidth('90px'),
                AdminColumn::custom('Тип',function(\Illuminate\Database\Eloquent\Model $model) {
                    if (!$model->type) return '';
                    $res = '<a href="/admin/types/'.$model->type->id.'/edit">'.$model->type->name.'</a>'/* .
                    '<a target="_blank" href="'.$model->category->getUrl().'"><i class="ml-2 fas fa-external-link-alt"></i></a>' */;

                    return $res;
                }),
                AdminColumn::datetime('updated_at')->setLabel('Дата Изменения')->setFormat('d.m.Y H:i'),
                AdminColumn::custom('Положение', function(\Illuminate\Database\Eloquent\Model $model) {
                    return \App\Services\AdminService::getOrderColumnContent($model,'/admin/things/');
                })->setWidth('150px')->setOrderable(function($query, $direction) {
                    $query->orderBy('order', $direction);
                })->setSearchable(false),
            ])
            ->setDisplaySearch(true)
            ->paginate(20);

        //$display->getActions()->setView('thing_actions')->setPlacement('panel.heading.actions');
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $thing = $id ? \App\Models\Thing::find($id) : false;

        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function ($id) use($thing)  {
            $tabs = [];
            $paids = \App\Models\Paint::all()->pluck('id')->toArray();
            $elements = [
                AdminFormElement::columns()
                    ->addColumn([AdminFormElement::text('name', 'Название')->required()],4)
                    ->addColumn([
                        AdminFormElement::select('type_id', 'Тип')
                            ->setModelForOptions(\App\Models\Type::class, 'name')
                            ->setSortable(false)
                            ->required()
                    ],2)
                    ->addColumn([AdminFormElement::checkbox('status', 'Доступен')->setDefaultValue(true)],2)
                    ->addColumn([AdminFormElement::checkbox('wholesale', 'Отображать на странице оптовых цен')->setDefaultValue(true)],2)
                    ->addColumn([AdminFormElement::checkbox('customprint', 'Отображать на странице печать на предметах')->setDefaultValue(true)],2),
                AdminFormElement::text('code', 'Код в ссылке'),
                /* AdminFormElement::multiselect('paints', 'Принты')->setModelForOptions(\App\Models\Paint::class, 'name')
                    ->setDefaultValue($paids)
                    ->setSortable(false), */
                AdminFormElement::ckeditor('description', 'Описание'),
                AdminFormElement::ckeditor('rule', 'Правило ухода'),
                AdminFormElement::columns()
                    ->addColumn([AdminFormElement::image('teaser_image', 'Картинка в тизере')],3),
            ];

            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Основное');
            $elements = [
                AdminFormElement::hasMany('prices', [
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('type', 'Тип печати',['one' => 'Одна сторона','two' => 'Две стороны'])
                            ->required()
                            ->setSortable(false)
                            ->setDefaultValue('one'),
                    ],3)
                    ->addColumn([
                        AdminFormElement::number('min', 'Минимальное количество')->setDefaultValue(1)
                    ],3)
                    ->addColumn([
                        AdminFormElement::number('value', 'Цена')->setStep(0.01)->setHelpText('Указать 0 или оставить пустым для отображения "Договорная"'),
                    ],3)
                ]),
            ];
            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Цены');
            $nums = [0 => '1'];
            for ($i=0; $i < (count($thing->images ?? [])); $i++) {
                if ($i) {
                    $nums[$i] = $i+1;
                }
            }
            $elements = [
                AdminFormElement::select('slider_image', 'Номер картинки в слайдере',$nums),
                AdminFormElement::view('admin.form.component',[
                    'component' => 'things-images',
                    'items' => $thing->images ?? [],
                    'prefix'=>'images'
                ]),
            ];
            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Картинки');

            $elements = [
                AdminFormElement::view('admin.form.component',[
                    'component' => 'params-component',
                    'items' => $thing->params ?? [],
                    'prefix'=>'params'
                ]),
            ];
            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Параметры');
            $elements = [
                AdminFormElement::view('admin.form.component',[
                    'component' => 'things-sizes',
                    'items' => $thing->sizes ?? [],
                    'prefix'=>'sizes'
                ]),
            ];
            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Размеры');
            $tabs[] = AdminService::seoTab();
            return $tabs;
        });
        return AdminForm::form()->addElement($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
