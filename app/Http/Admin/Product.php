<?php

namespace App\Http\Admin;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumn;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use \App\Services\AdminService;
/**
 * Class Product
 *
 * @property \App\Models\Product $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Product extends Section implements Initializable
{
    public function initialize()
    {
        $this->title = 'Распродажа';
        $this->icon = 'fas fa-piggy-bank';
        $this->created(function ($config, \Illuminate\Database\Eloquent\Model $model) {

        });
        $this->updated(function ($config, \Illuminate\Database\Eloquent\Model $model) {

        });

    }
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    public function can($action, Model $model)
    {
        return \Auth::user()->isManager();
    }

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

        $display = AdminDisplay::datatables()

            ->setApply(function ($query)  {
                //dd($request->all());
                //$query->orderBy('order', 'asc');
            })
            ->setOrder([[6, 'asc']])
            ->setColumns([
                AdminColumn::image('image','Картинка')->setWidth('90px'),
                AdminColumn::text('name','Название'),
                \AdminColumnEditable::checkbox('status','Доступен', 'Не доступен')
                    ->setLabel('Статус')
                    ->setWidth('120px'),
                AdminColumn::text('old_price','Старая цена')->setWidth('90px'),
                AdminColumn::text('price','Цена')->setWidth('90px'),
                AdminColumn::datetime('updated_at')->setLabel('Дата Изменения')->setFormat('d.m.Y H:i'),
                AdminColumn::custom('Положение', function(\Illuminate\Database\Eloquent\Model $model) {
                    return \App\Services\AdminService::getOrderColumnContent($model,'/admin/products/');
                })->setWidth('150px')->setOrderable(function($query, $direction) {
                    $query->orderBy('order', $direction);
                })->setSearchable(false),
            ])
            ->setDisplaySearch(true)
            ->paginate(20);

        //$display->getActions()->setView('product_actions')->setPlacement('panel.heading.actions');
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

        $product = $id ? \App\Models\Product::find($id) : false;

        $tabs = AdminDisplay::tabbed();
        $tabs->setTabs(function ($id) use($product)  {
            $tabs = [];
            $elements = [
                AdminFormElement::columns()
                    ->addColumn([AdminFormElement::text('name', 'Название')->required()],6)
                    ->addColumn([],3)
                    ->addColumn([AdminFormElement::checkbox('status', 'Доступен')->setDefaultValue(true)],3),

                AdminFormElement::columns()
                    ->addColumn([AdminFormElement::number('price', 'Цена')],3)
                    ->addColumn([AdminFormElement::number('old_price', 'Старая цена')],3),
                //AdminFormElement::textarea('short_description', 'Описание в тизере')->setRows(1),
                //AdminFormElement::ckeditor('description', 'Описание'),
                AdminFormElement::image('image', 'Картинка'),
            ];

            $tabs[] = AdminDisplay::tab(AdminForm::elements($elements))->setLabel('Основное');

            return $tabs;
        });
        return AdminForm::form()->addElement($tabs);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
