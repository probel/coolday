<?php

namespace App\Http\Controllers;

use App\Models\Callback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
class CallbackController extends Controller
{



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /* public function store(Request $request)
    {
        $res = ['status' => 'success'];
        $res['fields']['.js-client-phone'] = $request->phone;
        $res['fields']['.js-client-name'] = $request->name.',';
        //$res['src'] = captcha_src();
        //$res['popup'] = '#thankfulness-pop';
        $callback = new Callback();
        $callback->country_id = country()->id;
        $callback->phone = $request->phone;
        $callback->name = $request->name;
        $callback->message = $request->message;
        $callback->themes = $request->theme ?? '';
        $callback->email = $request->email;
        $callback->ip = $_SERVER['REMOTE_ADDR'];
        $callback->type = $request->type ?? 'Перезвоните мне';

        //$callback->message = $request->message;
        $callback->status = 0;
        if (!$request->agree) {
            $callback->save();
            $files = $request->file('upload_files');
            if ($request->hasFile('upload_files')) {
                $fls = [];
                foreach ($files as $file) {

                    $fls[] = $file->storeAs(
                        "feedbacks/".$callback->id.'/',
                        $file->getClientOriginalName());
                }
                $callback->files = $fls;
                $callback->save();
            }
            session(['callback_id' => $callback->id]);
            $res['location'] = route('callback.success').'/';
            Mail::to(getConfigValue('message_mail'))->send(new \App\Mail\Callback($callback));
        }
        if ($request->ajax()) {
            return $res;
        }
        return redirect(route('callback.success').'/');
    } */
    /* public function success()
    {
        $callback_id = request()->session()->get('callback_id', null);
        if ($callback_id && $callback = Callback::find($callback_id)) {

            //session(['callback_id' => null]);
            $meta = [
                'title' => rv('Спасибо за заявку'),
                'description' => rv(getConfigValue('success_description')),
                'keywords' => rv(getConfigValue('success_keywords')),
            ];
            $breadcrumbs = [
                ['href'=>'/','name'=>'Главная'],
                ['href'=>'','name'=>'Спасибо за заявку']
            ];
            return view('pages.thankyou',[
                    'callback'=>$callback,
                    'title' => 'Спасибо за заявку',
                    'view' =>   'thankyou',
                    'meta' => $meta,
                    'breadcrumbs'   => $breadcrumbs,
                ]);
        } else {
            return redirect('/');
        }
    } */
}
