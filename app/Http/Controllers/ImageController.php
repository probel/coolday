<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Repositories\EntityRepository;

class ImageController extends Controller
{
    /**
     * @var EntityRepository;
     */
    protected $entityRepository;

    public function __construct()
    {
        $this->entityRepository = app(EntityRepository::class);
    }
    public function create()
    {
        $id = request()->eid;
        $key = request()->key;
        $size = request()->size;
        $entity = $this->entityRepository->getForImage($id);
        if (!$entity) {
            return $this->abort();
        }
        $entity->images = collect(\json_decode($entity->images) ?? []);
        return $this->makeImage($entity,$key,$size);
    }
    public function show($id,$key,$size,$cid = null)
    {
        $entity = $this->entityRepository->getForImage($id);
        if (!$entity) {
            return $this->abort();
        }

        $entity->images = collect(\json_decode($entity->images) ?? []);
        return $this->makeImage($entity,$key,$size,$cid);
    }
    private function makeImage($entity, $num = 0, $size = 505, $cid = null)
    {
        $imagePath = \Images::makeImage($entity, $num, $size, $cid);
        $imageContent = \file_get_contents($imagePath);
        return response($imageContent, 200)->header('Content-Type', 'image/jpeg');
    }
    private function abort()
    {
        return response(\Image::make('images/no-image.png')->stream('jpg', 70), 200)->header('Content-Type', 'image/jpeg');
    }
}
