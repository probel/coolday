<?php

namespace App\Http\Controllers;


use \App\Helpers\{ PageHelper, CategoryHelper };
use \App\Repositories\{ ThingRepository, EntityRepository };

class PageController extends Controller
{


    /**
     * Display page.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = PageHelper::getToSlug($slug);
        if (!$page) {
            abort(404);
        }

        if ($page->type == 'wholesale') {
            return $this->wholesale($page);
        }
        if ($page->type == 'sizes') {
            return $this->sizes($page);
        }
        $meta = $page->meta;
        $title = $page->title;
        $values = $page->values;
        $breadcrumbs = $page->breadcrumbs;

        return view('pages.'.$page->type,compact('meta','title','page','values','breadcrumbs'));
    }

    /**
     * Show index page
     *
     * @return \illuminate\http\response
     */
    public function showFront()
    {
        $page = PageHelper::find(1);
        if (!$page) {
            abort(404);
        }
        $meta = $page->meta;
        $values = $page->values;
        $things = collect([]);

        $thingRepository = app(ThingRepository::class);
        $things = $thingRepository->getForFront($values->things ?? []);

        $categories = CategoryHelper::getForFront($values->categories ?? []);

        $entityRepository = app(EntityRepository::class);
        $popular = $entityRepository->getPopular($values->popular ?? []);

        $faqPage = PageHelper::find(6);
        $faq = $faqPage->values->faq ?? [];
        return view('pages.front', compact('meta','page','values','things','categories','popular', 'faq'));

    }

    public function constructor()
    {
        $page = PageHelper::find(11);
        $meta = $page->meta;
        $title = $page->title;
        $values = $page->values;
        $breadcrumbs = $page->breadcrumbs;
        $thing = null;

        $thingRepository = app(ThingRepository::class);
        $thing = $thingRepository->getForConstructor(request()->id);

        return view('pages.'.$page->type,compact('meta','title','page','values','breadcrumbs', 'thing'));
    }
    /**
     * Show wholesale page
     *
     * @param object $page
     *
     * @return \illuminate\http\response
     */
    public function wholesale($page)
    {
        $meta = $page->meta;
        $title = $page->title;
        $values = $page->values;
        $breadcrumbs = $page->breadcrumbs;

        $thingRepository = app(ThingRepository::class);
        $things = $thingRepository->getForWholesale();
        return view('pages.'.$page->type,compact('meta','title','page','values','breadcrumbs', 'things'));
    }
    /**
     * Show sizes page
     *
     * @param object $page
     *
     * @return \illuminate\http\response
     */
    public function sizes($page)
    {
        $meta = $page->meta;
        $title = $page->title;
        $values = $page->values;
        $breadcrumbs = $page->breadcrumbs;

        $thingRepository = app(ThingRepository::class);
        $things = $thingRepository->getForSizes();
        return view('pages.'.$page->type,compact('meta','title','page','values','breadcrumbs', 'things'));
    }


}
