<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Page;
use App\Repositories\{ EntityRepository, SaleRepository};
use App\Helpers\PageHelper;
class CartController extends Controller
{
    /**
     * Репозиторий товаров
     *
     * @var EntityRepository;
     */
    protected $entityRepository;

    /**
    * Репозиторий распродажи
    *
    * @var SaleRepository;
    */
    protected $saleRepository;

    public function __construct()
    {
        $this->entityRepository = app(EntityRepository::class);
        $this->saleRepository = app(SaleRepository::class);
    }

    /**
     * Show cart page
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $page = PageHelper::find(4);
        if (!$page) {
            abort(404);
        }
        $meta = $page->meta;
        $breadcrumbs = $page->breadcrumbs;
        $title = $page->title;
        $values = $page->values ?? [];
        $cart = \Cart::get();

        $emptyMessage = 'Ваша корзина пуста';
        $emptyText = '';
        $quickOrdered =  session('ordered',false);
        if ($quickOrdered) {
            $emptyMessage = 'Заказ успешно оформлен';
            $emptyText = 'Наш менеджер свяжется с Вами в ближайшее время.';
            session(['ordered'=> false]);
        }
        if (request()->ajax()) {
            $res = view('shared.cart.content', compact('cart'))->render();
            return response()->json(['status'=>'success','content'=>$res]);
        }
        return view('pages.cart.show',compact('page','meta','breadcrumbs','emptyMessage', 'emptyText', 'title','values','cart','quickOrdered'));
    }

    /**
     * Show checkout page
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        $page = PageHelper::find(4);
        if (!$page) {
            abort(404);
        }
        $meta = $page->meta;
        $breadcrumbs = $page->breadcrumbs;
        $title = $page->title;
        $values = $page->values ?? [];
        $cart = \Cart::get();
        if (!$cart->totalQty) {
            return redirect(route('cart.show'));
        }
        $emptyMessage = 'Ваша корзина пуста';
        $emptyText = '';
        $quickOrdered =  session('ordered',false);
        if ($quickOrdered) {
            $emptyMessage = 'Заказ успешно оформлен';
            $emptyText = 'Наш менеджер свяжется с Вами в ближайшее время.';
            session(['ordered'=> true]);
        }
        if (request()->ajax()) {
            $res = view('shared.cart.checkout')->render();
            return response()->json(['status'=>'success','content'=>$res]);
        }
        return view('pages.cart.checkout',compact('page','meta','breadcrumbs', 'title','values','cart','quickOrdered'));
    }

    /**
     * Show success order page
     *
     * @return \Illuminate\Http\Response
     */
    public function success()
    {
        if (!session('ordered',false)) {
            return redirect(route('cart.show'));
        }

        $page = PageHelper::find(4);
        $meta = $page->meta;
        $breadcrumbs = $page->breadcrumbs;
        $title = $page->title;
        $values = $page->values ?? [];

        $entities = null;
        $eids = session('last_order',[]);

        $entityRepository = app(EntityRepository::class);
        $entities = $entityRepository->getPopular($eids);

        if (count($eids)) {
            $orderItems = $entityRepository->getPopular($eids);
            $entities = collect([]);

            foreach ($orderItems as $key => $item) {
                $entities = $entities->merge($entityRepository->getRelated($item->paint_id));
            }
            $entities = $entities->shuffle()->take(10);
        }
        session(['ordered' => false]);
        if (request()->ajax()) {
            $res = view('shared.cart.success', compact('entities'))->render();
            return response()->json(['status'=>'success','content'=>$res]);
        }
        return view('pages.cart.success',compact('page','meta','breadcrumbs','entities'));
    }

    /**
     * Add to cart
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request)
    {
        $eid = (int) $request->eid;
        $pid = (int) $request->pid;
        $cid = (int) $request->color;
        $sid = (int) $request->size;
        $qty = (int) ($request->qty ?? 1);
        $item = null;
        $key = '';
        if ($pid) {
            $item = $this->saleRepository->getForCart($pid);
        }
        if ($eid) {
            $item = $this->entityRepository->getForCart($eid, $cid, $sid);
        }

        if ($item) {
            $key = $item->cartKey;
            \Cart::set($item,$qty);
        }

        $cart = \Cart::get();
        if (request()->ajax()) {
            $res = $this->ajaxResponse();
            $res['key'] = $key;
            $res['fields']['[data-key="'.$key.'"] .item_add_cart_ico'] = @file_get_contents(public_path('images/svg/purchase.svg'));
            return response()->json($res);
        }
        return redirect(route('cart.show'));
    }

    /**
     * Change or delete item in the cart
     *
     * @return \Illuminate\Http\Response
     */
    public function change()
    {
        $qty = (int) request()->qty;
        $cart = \Cart::changeQty(request()->key, $qty);
        if (request()->ajax()) {
            $res = $this->ajaxResponse();
            $res['fields']['.js-cart-content'] = view('shared.cart.content', compact('cart'))->render();
            return response()->json($res);
        }
        return redirect(route('cart.show'));
    }

    /**
     * Clear cart
     *
     * @return \Illuminate\Http\Response
     */
    public function clear()
    {

        $cart = \Cart::clear();
        if (request()->ajax()) {
            return response()->json($this->ajaxResponse());
        }
        return redirect(route('cart.show'));
    }

    /**
     * Make ajax response
     * @return \Illuminate\Http\Response
     */
    private function ajaxResponse()
    {
        $cart = \Cart::get();
        $res = [
            'status'    => 'success',
            'fields'    => [
                '.js-cart-informer' => view('shared.cart.informer', compact('cart'))->render(),
                '.js-mobile-cart-informer' => view('shared.cart.informer-mobile', compact('cart'))->render()
            ],
            'total'     => number_format($cart->totalPrice,0,'.',' '),
        ];
        if (!$cart->totalQty) {
            $res['location'] = route('cart.show');
        }
        if (request()->confirm) {
            $res['popup'] = view('shared.cart.popup')->render();
        }
        if (request()->iscart) {
            $res['fields']['.js-cart-content'] = view('shared.cart.content', compact('cart'))->render();
        }
        return $res;
    }
}
