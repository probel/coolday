<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AdminSection;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Support\Str;
use App\Jobs\UpdatePrices;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\{ ProductImport };
use \App\Models\{
    Category,
    Page,
    Product,
};

class AdminController extends Controller
{

    public function deleteItems()
    {
        $model = "\App\Models\\".request()->type;

        $model::whereIn('id',request()->_id)->delete();

    }
    public function index()
    {
        return redirect('admin/orders');
    }
    public function getColors()
    {
        $colors = \App\Models\Color::orderBy('order')->get(['id','name','code'])->toArray();
        return response()->json($colors);
    }
    public function importForm()
    {
        $content = view('admin.import')->render();
        return AdminSection::view($content, 'Импорт');
    }
    public function importStore()
    {

        $images = request()->file('images');
        $cid = request()->category_id;
        $things = \App\Models\Thing::whereIn('id',request()->things_ids)->get();
        $paintsCount = 0;
        $thingsCount = 0;
        foreach ($images as $key => $image) {
            $extension = $image->getClientOriginalExtension();
            $name = $image->getClientOriginalName();
            $name = str_replace('.'.$extension,'',$name);
            $file = $image->store('paints', ['disk' => 'images']);
            $path = 'images/uploads/'.$file;
            $paint = \App\Models\Paint::firstOrCreate(['name' =>$name,'category_id' => $cid],['image1'=>$path,'image_big' => $path]);
            $paintsCount++;
            foreach ($things as $key => $thing) {
                $entity = \App\Models\PaintThing::firstOrCreate(['paint_id' => $paint->id,'thing_id' => $thing->id]);
                $thingsCount++;
            }
            \App\Services\AdminService::setEntitiesParams($paint);
        }

        $message = "<div>Принты загружены<br>
                    Всего принтов: <strong>$paintsCount</strong><br>
                    Товаров добавлено: <strong>$thingsCount</strong></div>";
        \MessagesStack::addSuccess($message);

        return back();
    }

    public function saveImage(Request $request)
    {
        $path = 'images/uploads';
        $file = request()->upload;

        $settings = [];
        $filename = $file->getClientOriginalName();
        //dd(request()->all());
        if (class_exists('Intervention\Image\Facades\Image') && (bool) getimagesize($file->getRealPath())) {

            $image = \Intervention\Image\Facades\Image::make($file);

            foreach ($settings as $method => $args) {
                call_user_func_array([$image, $method], $args);
            }

            $value = $path.'/'.$filename;

            $image->save(public_path().'/'.$value);
            $res = "<!DOCTYPE html>
            <html lang=\"en\">
            <head>
                <title>Document</title>
            </head>
            <body>
            <script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(".request()->CKEditorFuncNum.", '/".$value."');</script>
            </body>
            </html>";
            return $res;
            return response()->json(['test'=>'test'])->withCallback($request->input('callback'));
        }

        $file->move(public_path().'/'.$path, $filename);

        //S3 Implement
        $value = $path.'/'.$filename;
        $res = "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(1, '/".$value."');</script>";
        return $res;
        return response()->json(['uploaded'=>1,'url' => '/'.$value, 'fileName' => $filename]);
    }
    public function saveFieldFile(Request $request)
    {
        $path = 'files/documents';
        $file = $request->file;
        $settings = [];
        $fl= $file->getClientOriginalName();
        $filename = pathinfo($fl, PATHINFO_FILENAME);
        $filename  = Str::slug($filename , '_');
        $extension = pathinfo($fl, PATHINFO_EXTENSION);
        $fn = $filename;
        $i = 1;
        while (true) {
            if (\file_exists(public_path($path.'/'.$fn.'.'.$extension))) {
                $fn = $filename.'_'.$i;
                $i++;
            } else {
                $filename = $fn.'.'.$extension;
                break;
            }
        }

        $file->move($path, $filename);
        $value = $path.'/'.$filename;
        return response()->json(['path' => asset($value), 'value' => $value]);
    }
    public function saveFieldImage(Request $request)
    {
        $path = 'images/uploads';
        $file = $request->file;
        $settings = [];
        $extension = $file->getClientOriginalExtension();
        $filename = md5($file->getClientOriginalName().time()).'.'.$extension;
        if (class_exists('Intervention\Image\Facades\Image') && (bool) getimagesize($file->getRealPath())) {

            $image = \Intervention\Image\Facades\Image::make($file);

            foreach ($settings as $method => $args) {
                call_user_func_array([$image, $method], $args);
            }

            $value = $path.'/'.$filename;

            $image->save($value);

            return response()->json(['path' => asset($value), 'value' => $value]);
        }

        $file->move($path, $filename);

        $value = $path.'/'.$filename;

        return response()->json(['path' => asset($value), 'value' => $value]);
    }
    public function saveFile(Request $request)
    {
        $path = 'files/uploads';
        $file = $request->file;
        $settings = [];
        $filename = $file->getClientOriginalName();

        $file->move($path, $filename);

        //S3 Implement
        $value = $path.'/'.$filename;

        return response()->json(['path' => asset($value), 'value' => $value]);
    }
    public function changeOrder(Request $request)
    {
        $new = (int) $request->new;
        $old = (int) $request->old;
        $model = $request->model;
        //dd($model);
        if ($new > $model::count()-1) {
            $new = $model::count()-1;
        }
        if ($new != $old) {
            if ($new < $old) {
                $model::where('order','>=',$new)->where('order','<',$old)->increment('order');
            } else {
                $model::where('order','<=',$new)->where('order','>',$old)->decrement('order');
            }
        }
        $model::where('id',$request->id)->update(['order' => $new]);
        return back();
    }

    public function downloadFile($id,$file)
    {

        $item = \App\Models\Callback::find($id);
        if ($item && ($item->files[$file] ?? null)) {
            return \Storage::download($item->files[$file]);
        }
        return back();
    }
}
