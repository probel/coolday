<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{ Page, Product };
use App\Helpers\PageHelper;
use \App\Repositories\{ SaleRepository };
class SaleController extends Controller
{
    /**
     * Репозиторий товаров
     *
     * @var SaleRepository;
     */
    protected $saleRepository;

    public function __construct()
    {
        $this->saleRepository = app(SaleRepository::class);
    }

    /**
     * Display sale page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pg = PageHelper::find(7);
        $paginate = $page->values->paginate ?? 25;
        $page = (int) (request()->page ?? 1);
        $products = $this->saleRepository->getCatalogItems($page,$paginate);
          if (request()->ajax()) {
            $products->withPath(route('sale.index'));
            $list = view('shared.sale.items',compact('products'))->render();
            $paginate = view('shared.sale.paginate',compact('products'))->render();
            return response()->json([
                    'status' => 'success',
                    'list'  => $list,
                    'paginate' => $paginate,
                ]);
        }
        $meta = $pg->meta;
        $breadcrumbs = $pg->breadcrumbs;
        return view('pages.sale.index',compact('pg','products','meta','breadcrumbs'));
    }
}
