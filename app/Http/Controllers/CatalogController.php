<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\{ Category, Product, Thing, Paint, PaintThing, Page };
use \App\Repositories\{ EntityRepository, ThingRepository };
use \App\Helpers\{ CategoryHelper, PageHelper, TypeHelper };
class CatalogController extends Controller
{
    /**
     * Репозиторий товаров
     *
     * @var EntityRepository;
     */
    protected $entityRepository;

    public function __construct()
    {
        $this->entityRepository = app(EntityRepository::class);
    }

    public function index() {
        return $this->catalog();
    }

    public function resolver($slugString = null)
    {
        /* If Slug Belongs To Category */
        $category = CategoryHelper::getToSlug($slugString);
        if ($category) {
            return $this->catalog($category);
        }

        /* If Slug Belongs To Entity */
        $entity = $this->entityRepository->getToSlug($slugString);
        if ($entity) {
            return $this->show($entity);
        }

        abort(404);
    }

    /**
     * Предметы
     *
     * @return \Illuminate\Http\Response
     */
    public function things()
    {
        $page = PageHelper::find(13);
        if (!$page) {
            abort(404);
        }
        $meta = $page->meta;
        $breadcrumbs = [
            ['href' => '/', 'name' => 'Главная'],
            ['href' => route('catalog.index'), 'name' => 'Каталог'],
            ['href' => '', 'name' => $page->title],
        ];
        $title = $page->title;

        $thingRepository = app(ThingRepository::class);
        $things = $thingRepository->getForCatalog();

        return view('pages.catalog.things',compact('page','meta','title','breadcrumbs','things'));
    }


    /**
     * Страница поиска
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $text = request()->text;

        $page = (int) (request()->page ?? 1);
        $paginate = 24;

        $title = 'Поиск "'.$text.'"';
        $meta = [
            'title' => $title,
        ];
        $breadcrumbs = [
            ['href' => '/', 'name' => 'Главная'],
            ['href' => '', 'name' => 'Результаты поиска'],
        ];

        $items = $this->entityRepository->getCatalogItems($page, $paginate, null, null, $text);
        return view('pages.catalog.index',compact('meta','title','breadcrumbs','items'));
    }

    /**
     * Подсказки для поиска
     *
     * @return \Illuminate\Http\Response
     */
    public function searchHint()
    {
        $text = trim(request()->text);
        if (!$text) {
            return response()->json(['status'=>'empty']);
        }
        $items = $this->entityRepository->getSearchItems($text);
        if (!$items->count()) {
            return response()->json(['status'=>'empty']);
        }
        $list = view('shared.search.popup-results',['searchResults' => $items,'text'  => $text])->render();
        return response()->json([
            'status'    => 'success',
            'list'   => $list,
        ]);
    }

    /**
     * Страница каталога
     *
     * @param Object $category
     *
     * @return [type]
     */
    private function catalog($category = null)
    {

        $pg = PageHelper::find(8);
        if (!$pg) {
            abort(404);
        }
        $request = request();
        $paginate = (int) ($pg->values->paginate ?? 24);
        $page = (int) (request()->page ?? 1);
        $categoryIds = $category->categoryIds ?? null;

        if (request()->ajax()) {
            $type = $request->type;
            $items = $this->entityRepository->getCatalogItems($page, $paginate, $type, $categoryIds);
            $list = view('shared.catalog.items', compact('items'))->render();
            $paginate = view('shared.catalog.paginate', compact('items', 'type'))->render();

            return response()->json([
                'status'    => 'success',
                'list'   => $list,
                'paginate'  => $paginate,
            ]);
        }
        $descriptionFooter = '';
        if ($category) {
            $meta = $category->meta;
            $title = $category->name;
            $description = $category->description;
            $descriptionFooter = $category->description_footer;
            $footer = $category->footer ?? [];
            $breadcrumbs = $category->breadcrumbs;
        } else {
            $meta = $pg->meta;
            $title = $pg->title;
            $description = $pg->values->body ?? '';
            $footer = $pg->values;
            $breadcrumbs = $pg->breadcrumbs;
        }
        $items = $this->entityRepository->getCatalogItems($page, $paginate, null, $categoryIds);
        $types = TypeHelper::all();

        return view('pages.catalog.index',compact('meta','title','description', 'descriptionFooter','footer','breadcrumbs','category','items','types'));
    }

    /**
     * Страница товара
     *
     * @param Object $entity
     *
     * @return [type]
     */
    private function show($entity)
    {

        $meta = $entity->meta;
        $breadcrumbs = $entity->breadcrumbs;

        $latests = session('latests',[]);
        array_unshift($latests,$entity->id);
        $latests = array_unique($latests);
        session(['latests'=>$latests]);

        $similar = $this->entityRepository->getSimilar($entity);
        $latests = $this->entityRepository->getLatests($entity);
        $related = $this->entityRepository->getRelated($entity->paint_id);

        return view('pages.catalog.show',compact('meta','breadcrumbs','entity', 'similar', 'latests', 'related'));
    }

}
