<?php

namespace App\Http\Controllers;

use App\Models\{
    Order,
};
use App\Repositories\EntityRepository;
use Illuminate\Http\Request;
use App\Jobs\SendOrderEmailsJob;
use App\Helpers\DeliveryHelper;
class OrderController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = \Cart::get();
        $res = ['status' => 'success'];
        if ($cart->totalQty) {
            $eids = $cart->items->pluck('id')->toArray();
            session(['last_order' => $eids]);
            $order = $this->makeOrder($request);
            SendOrderEmailsJob::dispatch($order->id)->onQueue('high');

        } else {
            $res['location'] = route('cart.show');
        }

        if (request()->ajax()) {
            if (count($eids)) {
                $entities = \App\Models\PaintThing::whereIn('id',$eids)->get();
            }
            $res['content'] = view('shared.cart.success',compact('entities'))->render();
            return response()->json($res);
        }

        return redirect(route('cart.success'));
    }
    public function quick(Request $request)
    {

        $res = [
            'status' => 'success',
        ];
        if ($request->eid == 'cart') {
            $order = $this->makeOrder($request);
            $res['location'] = '/cart/success';
        } else {

            $eid = $request->eid;
            $cid = $request->color;
            $sid = $request->size;
            $entityRepository = app(EntityRepository::class);
            $item = $entityRepository->getForCart($eid, $cid, $sid);


            $order = new Order;
            $order->name = strip_tags($request->name ?? '');
            $order->phone = strip_tags($request->phone ?? '');

            $comment = 'Быстрый заказ. ';
            if ($request->comment) {
                $comment .= $request->comment;
            }
            $order->comment = $comment;
            $order->date = \Carbon\Carbon::now();

            $qty = $request->qty ?? 1;

            $price = $item->prices->where('min',1)->first()->value ?? 0;

            $item->priceUnit = $price;
            $item->qty = $qty;
            $item->price = $qty * $price;

            $order->price = $price * $qty;

            $order->items = [ $item ];
            $order->save();
            $res['popup'] = '#quick-success';
        }
        SendOrderEmailsJob::dispatch($order->id)->onQueue('high');

        return response()->json($res);
    }

    private function makeOrder(Request $request)
    {
        $cart = \Cart::get();
        $order = new Order;
        $order->name = strip_tags($request->name ?? '');
        $order->phone = strip_tags($request->phone ?? '');
        $order->email = strip_tags($request->email ?? '');
        $order->address = $request->address ?? [];
        $order->comment = strip_tags($request->comment ?? '');
        $order->date = \Carbon\Carbon::now();
        $total = $cart->totalPrice;
        if ($request->delivery) {
            $delivery = DeliveryHelper::find($request->delivery);
            $total += $delivery->price ?? 0;
            $order->delivery = ($delivery->name ?? '').' '.($delivery->price ?? 0).'руб';
        }
        $order->price = $total;
        $order->items = $cart->items;
        $order->save();

        \Cart::clear();

        session(['ordered' => true]);
        return $order;
    }
}
