<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Helpers\{ PageHelper, CategoryHelper };
use \App\Repositories\{ ThingRepository, EntityRepository };
use Sitemap;

class SitemapController extends Controller
{
    public function index()
    {
        Sitemap::addSitemap(route('sitemap.general'));
        Sitemap::addSitemap(route('sitemap.products',[0]));
        Sitemap::addSitemap(route('sitemap.products',[1]));
        Sitemap::addSitemap(route('sitemap.products',[2]));
        Sitemap::addSitemap(route('sitemap.products',[3]));
        return Sitemap::index();
    }
    public function general()
    {
        $paths = collect([]);

        /* PAGES */
        $pages = PageHelper::all();
        $paths = $paths->merge($pages->pluck('url'));

        /* CATEGORIES */
        $categories = \App\Helpers\CategoryHelper::all();
        $paths = $paths->merge($categories ->pluck('url'));

        /* THINGS */
        $thingRepository = app(ThingRepository::class);
        $products = $thingRepository->getForSitemap();
        $paths = $paths->merge($products);

        $paths = $paths->merge($products);
        foreach ($paths as $url) {
            Sitemap::addTag($url, false, 'daily', '1');
        }

        return Sitemap::render();
    }
    public function products($part)
    {
        $paths = collect([]);

        /* ENTITIES */
        $entityRepository = app(EntityRepository::class);
        $products = $entityRepository->getForSitemap($part);

        $paths = $paths->merge($products);
        foreach ($paths as $url) {
            Sitemap::addTag($url, false, 'daily', '1');
        }
        return Sitemap::render();
    }
}
