<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Traits\OrderableModel;
use App\Traits\{ SeoTrait, Status };

class Category extends Model
{
    use OrderableModel;
    use SeoTrait;
    use Status;

    protected $casts = [
        'values' => 'array'
    ];
    protected $with = ['parent'];
    /**
     * @param $query
     * @param int $position
     *
     * @return mixed
     */
    public function scopeFindByPosition($query, $position)
    {
        return $query->where($this->getOrderField(), $position);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }
    public function children()
    {
        return $this->hasMany(Category::class,'parent_id');
    }

    public function getPath()
    {
        $path = '';
        if ($this->parent_id) {
            $path .= $this->parent->getPath().'/';
        }
        $path .= $this->slug;
        return $path;
    }
    public function getUrl()
    {
        return route('catalog.resolver',[$this->getPath()]);
    }
}
