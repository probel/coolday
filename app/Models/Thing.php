<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Traits\OrderableModel;
use App\Traits\{ SeoTrait, Status };

class Thing extends Model
{
    use OrderableModel;
    use SeoTrait;
    use Status;

    //protected $with = ['prices'];
    protected $casts = [
        'params'    => 'array',
        'sizes'     => 'array',
        'images'    => 'array',
    ];


    public function scopeFindByPosition($query, $position)
    {
        return $query->where($this->getOrderField(), $position);
    }
    public function entities()
    {
        return $this->hasMany(PaintThing::class);
    }
    public function prices()
    {
        return $this->hasMany(ThingPrice::class);
    }
    public function paints()
    {
        return $this->belongsToMany(Paint::class,'paint_things');
    }
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function getUrl()
    {
        return route('constructor', ['id' => $this->id]);
    }
    public function getPrice($qty = 1,$type = 'one')
    {

        $price = $this
            ->prices
            ->sortByDesc('min')
            ->where('type',$type)
            ->where('min','<=',$qty)
            ->toBase()
            ->first();
        return $price->value ?? 0;
    }
    public function getPrices($type = 'one')
    {
        $prices = $this->prices()->orderBy('min','desc')->where('type',$type)->get(['min','value']);
        return $prices;
    }
    public function getPriceTable()
    {
        $table = [
            'ranges' => [],
            'one'   => [],
            'two'   => []
        ];
        $keys = $this->prices->sortBy('min')->groupBy('min')->keys();
        $prices = $this->prices->sortByDesc('min');
        $isOne = $prices->where('type','one')->count();
        $isTwo = $prices->where('type','two')->count();
        for ($i=0; $i < count($keys); $i++) {
            if ($keys[$i + 1] ?? false) {
                $table['ranges'][] = $keys[$i].'-'.($keys[$i+1] - 1).' шт';
            } else {
                $table['ranges'][] = $keys[$i].' и >';
            }
            if ($isOne) {
                if ($price = $prices->where('type','one')->where('min','<=',$keys[$i])->first()) {
                    if ($price->value) {
                        $table['one'][] = $price->value.' руб';
                    } else {
                        $table['one'][] = 'Договорная';
                    }
                } else {
                    $table['one'][] = '';
                }
            }
            if ($isTwo) {
                if ($price = $prices->where('type','two')->where('min','<=',$keys[$i])->first()) {
                    if ($price->value) {
                        $table['two'][] = $price->value.' руб';
                    } else {
                        $table['two'][] = 'Договорная';
                    }
                } else {
                    $table['two'][] = '';
                }
            }
        }

        return $table;
        //dd($this->prices);
    }
}
