<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CategoryHelper;
class PaintThing extends Model
{
    protected $with = ['thing','paint'];
    protected $fillable = [
        'paint_id', 'thing_id'
    ];
    public function thing()
    {
        return $this->belongsTo(Thing::class);
    }
    public function paint()
    {
        return $this->belongsTo(Paint::class);
    }
    public function getPrice($qty = 1)
    {
        $type = $this->paint->duplex ? 'two' : 'one';
        return $this->thing->getPrice($qty,$type);
    }
    public function getPriceText($qty = 1)
    {
        $price = $this->getPrice($qty);
        if ($price) {
            return $price.' руб.';
        }
        return 'Договорная';
    }
    public function getPrices()
    {
        $type = $this->paint->duplex ? 'two' : 'one';
        return $this->thing->getPrices($type);
    }
    public function getColors()
    {
        $ids = array_filter(array_unique(array_column($this->thing->images,'color')));
        if (!count($ids)) {
            return null;
        }
        $colors = \App\Models\Color::whereIn('id',$ids)->orderBy('order')->get();
        return $colors;
    }

    public function getUrl()
    {
        $category = CategoryHelper::find($this->paint->category_id);
        $path = $category->path;
        $path .= '/'.$this->slug;
        $url = route('catalog.resolver',[$path]);
        return $url;

    }

    public function getCartKey($cid = null, $sid = null)
    {
        return md5($this->id.'-'.$cid.'-'.$sid);
    }
    public function isInCart()
    {
        $key = $this->getCartKey();
        return \Cart::get()->items->where('key',$key)->count();

    }
}
