<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Traits\OrderableModel;
use App\Traits\{ Status };

class Paint extends Model
{
    use OrderableModel;
    //use SeoTrait;
    use Status;
    /**
     * @param $query
     * @param int $position
     *
     * @return mixed
     */
    protected $with = ['category'];

    public function scopeFindByPosition($query, $position)
    {
        return $query->where($this->getOrderField(), $position);
    }
    protected $casts = [
        'images' => 'array',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $fillable = [
        'name', 'slug', 'category_id', 'image1', 'image2', 'image3', 'image_big'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function things()
    {
        return $this->belongsToMany(Thing::class,'paint_things');
    }
    public function entities()
    {
        return $this->hasMany(PaintThing::class);
    }
    public function getRelated()
    {
        $items = $this->entities()
            ->select(['paint_things.*','things.id as thing_id','things.order'])
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('things.status',1)
            ->orderBy('things.order')
            ->get();
        return $items;
    }
    public function getUrl()
    {
        if (!$this->category) {
            return '/';
        }
        $url = route('catalog.image',['category'=>$this->category->slug,'slug'=>$this->slug]);
        return $url;
    }

}
