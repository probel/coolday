<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];
    protected $casts = [
        'items' => 'collection',
        'address' => 'array',
    ];
    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
