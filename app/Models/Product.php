<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Traits\OrderableModel;
use App\Traits\{ SeoTrait, Status };
class Product extends Model
{
    use OrderableModel;

    use Status;
    /**
     * @param $query
     * @param int $position
     *
     * @return mixed
     */
    protected $with = ['category'];

    public function scopeFindByPosition($query, $position)
    {
        return $query->where($this->getOrderField(), $position);
    }
    protected $casts = [
        'images' => 'array',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $fillable = [
        'name', 'slug', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function prices()
    {
        return $this->hasMany(Price::class);
    }
    public function getUrl()
    {
        return route('sale.show',['slug'=>$this->slug]);
    }
    public function getPrice()
    {
        return  $this->price;
    }
    public function isDescription()
    {
        return (boolean) trim(\strip_tags($this->description));
    }
    public function getCartKey()
    {
        return md5($this->id);
    }
    public function isInCart()
    {
        $key = $this->getCartKey();
        return \Cart::get()->items->where('key',$key)->count();

    }
}
