<?php

namespace App\Observers;

use App\Models\Paint;
use App\Jobs\MakeEntityImagesJob;

class PaintObserver
{
    /**
     * Handle the paint "created" event.
     *
     * @param  \App\Models\Paint  $paint
     * @return void
     */
    public function created(Paint $paint)
    {
        $this->setThings($paint);
        $this->cacheClear($paint);
        $this->makeImages($paint);
    }

    /**
     * Handle the paint "updating" event.
     *
     * @param  \App\Models\Paint  $paint
     * @return void
     */
    public function updating(Paint $paint)
    {
        if (request()->category_id) {
            $paint->category_id = request()->category_id;
        }
        $this->setThings($paint);
    }
    /**
     * Handle the paint "updated" event.
     *
     * @param  \App\Models\Paint  $paint
     * @return void
     */
    public function updated(Paint $paint)
    {
        $this->cacheClear($paint);
        $this->makeImages($paint);
    }

    /**
     * Handle the paint "deleting" event.
     *
     * @param  \App\Models\Paint  $paint
     * @return void
     */
    public function deleting(Paint $paint)
    {
        $this->cacheClear($paint);
    }
    /**
     * @param Paint $paint
     *
     * @return void
     */
    public function setThings(Paint $paint)
    {
        if (request()->things) {
            $ids = array_keys(request()->things);
            $paint->things()->sync($ids);
        }
    }
    /**
     * Add jobs for make preview images
     *
     * @param Paint $paint
     *
     * @return void
     */
    private function makeImages(Paint $paint)
    {

        /* foreach ($paint->entities as $key => $entity) {
            MakeEntityImagesJob::dispatch($entity->id);
        } */
    }

    /**
     * Clear depended cache
     *
     * @param Paint $paint
     *
     * @return void
     */
    private function cacheClear(Paint $paint)
    {

    }
}
