<?php

namespace App\Observers;

use App\Models\Thing;
use App\Jobs\MakeEntityImagesJob;

class ThingObserver
{
    /**
     * Handle the thing "created" event.
     *
     * @param  \App\Models\Thing  $thing
     * @return void
     */
    public function created(Thing $thing)
    {
        $this->cacheClear($thing);
        $this->makeImages($thing);
    }


    /**
     * Handle the thing "updated" event.
     *
     * @param  \App\Models\Thing  $thing
     * @return void
     */
    public function updated(Thing $thing)
    {
        $this->cacheClear($thing);
        $this->makeImages($thing);
    }

    /**
     * Handle the thing "deleting" event.
     *
     * @param  \App\Models\Thing  $thing
     * @return void
     */
    public function deleting(Thing $thing)
    {
        $this->cacheClear($thing);
    }

    /**
     * Add jobs for make preview images
     *
     * @param Thing $thing
     *
     * @return null
     */
    private function makeImages(Thing $thing)
    {

        /* foreach ($thing->entities as $key => $entity) {
            MakeEntityImagesJob::dispatch($entity->id);
        } */
    }
    /**
     * Clear depended cache
     *
     * @param Thing $thing
     *
     * @return null
     */
    private function cacheClear(Thing $thing)
    {
        \Cache::forget('prices_for_unit_all');
        \Cache::forget('all_prices');

    }
}
