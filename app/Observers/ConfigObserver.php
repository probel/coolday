<?php

namespace App\Observers;

use App\Models\Config;

class ConfigObserver
{
    /**
     * Handle the config "created" event.
     *
     * @param  \App\Models\Config  $config
     * @return void
     */
    public function created(Config $config)
    {
        $this->cacheClear();
    }

    /**
     * Handle the config "updated" event.
     *
     * @param  \App\Models\Config  $config
     * @return void
     */
    public function updated(Config $config)
    {
        $this->cacheClear();
    }

    /**
     * Handle the config "deleted" event.
     *
     * @param  \App\Models\Config  $config
     * @return void
     */
    public function deleted(Config $config)
    {
        $this->cacheClear();
    }

     /**
     * Clear depended cache
     *
     * @return null
     */
    private function cacheClear()
    {
        \Cache::forget('all_configs');
    }
}
