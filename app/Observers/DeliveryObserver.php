<?php

namespace App\Observers;

use App\Models\Delivery;

class DeliveryObserver
{
    /**
     * Handle the delivery "created" event.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return void
     */
    public function created(Delivery $delivery)
    {
        $this->cacheClear();
    }

    /**
     * Handle the delivery "updated" event.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return void
     */
    public function updated(Delivery $delivery)
    {
        $this->cacheClear();
    }

    /**
     * Handle the delivery "deleted" event.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return void
     */
    public function deleted(Delivery $delivery)
    {
        $this->cacheClear();
    }

     /**
     * Clear depended cache
     *
     * @return null
     */
    private function cacheClear()
    {
        \Cache::forget('all_deliveries');
    }
}
