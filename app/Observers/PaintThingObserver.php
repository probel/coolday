<?php

namespace App\Observers;

use App\Models\PaintThing;

class PaintThingObserver
{
    /**
     * Handle the paint thing "created" event.
     *
     * @param  \App\Models\PaintThing  $paintThing
     * @return void
     */
    public function created(PaintThing $paintThing)
    {
        //
    }

    /**
     * Handle the paint thing "updated" event.
     *
     * @param  \App\Models\PaintThing  $paintThing
     * @return void
     */
    public function updated(PaintThing $paintThing)
    {
        //
    }

    /**
     * Handle the paint thing "deleted" event.
     *
     * @param  \App\Models\PaintThing  $paintThing
     * @return void
     */
    public function deleted(PaintThing $paintThing)
    {
        //
    }

    /**
     * Handle the paint thing "restored" event.
     *
     * @param  \App\Models\PaintThing  $paintThing
     * @return void
     */
    public function restored(PaintThing $paintThing)
    {
        //
    }

    /**
     * Handle the paint thing "force deleted" event.
     *
     * @param  \App\Models\PaintThing  $paintThing
     * @return void
     */
    public function forceDeleted(PaintThing $paintThing)
    {
        //
    }
}
