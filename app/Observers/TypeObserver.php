<?php

namespace App\Observers;

use App\Models\Type;

class TypeObserver
{
    /**
     * Handle the type "created" event.
     *
     * @param  \App\Models\Type  $type
     * @return void
     */
    public function created(Type $type)
    {
        $this->cacheClear();
    }

    /**
     * Handle the type "updated" event.
     *
     * @param  \App\Models\Type  $type
     * @return void
     */
    public function updated(Type $type)
    {
        $this->cacheClear();
    }

    /**
     * Handle the type "deleted" event.
     *
     * @param  \App\Models\Type  $type
     * @return void
     */
    public function deleted(Type $type)
    {
        $this->cacheClear();
    }

     /**
     * Clear depended cache
     *
     * @return null
     */
    private function cacheClear()
    {
        \Cache::forget('all_types');
    }
}
