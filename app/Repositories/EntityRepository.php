<?php
namespace App\Repositories;

use App\Models\PaintThing as Model;
use Illuminate\Database\Eloquent\Collection;
use App\Models\{Thing, Paint};
use App\Helpers\{ PriceHelper, CategoryHelper, ColorHelper };

/**
 * Class EntityRepository
 *
 * @package App\Repositories
 */
class EntityRepository extends CoreRepository
{

    protected $teaserColumns = [
        'paint_things.id',
        'paint_things.paint_id',
        'paint_things.thing_id',
        'paint_things.slug',
        'paint_things.hash',
        'things.type_id',
        'things.name as thing_name',
        'things.images as images',
        'things.sizes',
        'things.order as thing_order',
        'paints.category_id',
        'paints.name as paint_name',
        'paints.duplex',
        'paints.order as paint_order',
    ];
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }
    /**
     * @param int $page
     * @param int $paginate
     * @param null $tid
     * @param null $cids
     * @param null $text
     * @param null $thid
     *
     * @return [type]
     */
    public function getCatalogItems($page = 1, $paginate = 24, $typeId = null, $cids = null, $text = null)
    {
        $columns = $this->teaserColumns;
        $items = $this
            ->startConditions()
            ->select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->when($typeId, function ($query, $typeId) {
                return $query->where('type_id', $typeId);
            })
            ->when($text, function ($query, $text) {
                return $query->where('things.name', 'like', "%$text%")->orWhere('paints.name', 'like', "%$text%");
            })
            ->when($cids, function ($query, $cids) {

                // Специфические категории, в которых выводим только рубашки и сортируем по принтам
                if (count($cids) == 1 && (\in_array(60,$cids) || \in_array(74,$cids))) {

                    return $query
                        ->whereIn('category_id', $cids)
                        //->where('thing_id',20)
                        ->orderByRaw("FIELD(things.id , '20') DESC")
                        ->orderBy("thing_order")
                        ->orderBy("paint_order");
                }
                return $query->whereIn('category_id', $cids)->orderBy('hash');

            }, function ($query) {
                return $query->orderBy('hash');
            })
            ->toBase()
            ->paginate($paginate, $columns, 'page', $page);

        $items->getCollection()->transform(function ($item) {
            return $this->prepare($item);
        });
        return $items;
    }

    public function getSearchItems($text)
    {

        $columns = [
            'paint_things.id',
            'paint_things.paint_id',
            'paint_things.thing_id',
            'paint_things.slug',
            'paint_things.hash',
            'paints.category_id',
            'things.name as thing_name',
            'paints.name as paint_name',
        ];
        $items = $this
            ->startConditions()
            ->select($columns )
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->when($text, function ($query, $text) {
                return $query->where('things.name', 'like', "%$text%")->orWhere('paints.name', 'like', "%$text%");
            })
            ->orderBy('hash')
            ->take(5)
            ->toBase()
            ->get();

        $items->map(function ($item) use($text) {
            $item->url = $this->makeUrl($item);
            $name = $item->thing_name.' '.$item->paint_name;
            $item->name = preg_replace("/($text)/iu", "<strong>\\1</strong>", $name);
            return $item;
        });
        return $items;
    }

    /**
     * Get Entity for show in catalog
     *
     * @param sting $slugString
     * @return Model
     */
    public function getToSlug($slugString)
    {

        $slugs = explode('/',$slugString);
        $slug = array_pop($slugs);
        $leftSlug = implode('/',$slugs);
        $columns = $this->teaserColumns;
        $columns[] = 'paint_things.meta_title';
        $columns[] = 'paint_things.meta_description';
        $columns[] = 'paint_things.meta_keywords';
        $columns[] = 'things.description';
        $columns[] = 'things.rule';
        $columns[] = 'things.params';
        $items = $this
            ->startConditions()
            ->select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->where('paint_things.slug',$slug)
            ->toBase()
            ->get();

        $item = $items->first(function($item) use ($leftSlug) {
            $category = CategoryHelper::find($item->category_id);
            return $category->path == $leftSlug;
        });

        if ($item) {
            $item = $this->prepare($item);
            $item->name = $item->thing_name .' '.$item->paint_name;

            /* Meta */
            $metaTitle = $item->meta_title ? $item->meta_title : \ConfigHelper::get('default_title');
            $metaTitle = $this->replaceVariables($item,$metaTitle);

            $metaDescription = $item->meta_description ? $item->meta_description : \ConfigHelper::get('default_description');
            $metaDescription = $this->replaceVariables($item,$metaDescription);

            $metaKeywords = $item->meta_keywords ? $item->meta_keywords : \ConfigHelper::get('default_keywords');
            $metaKeywords = $this->replaceVariables($item,$metaKeywords);

            $item->meta = [
                'title' =>  $metaTitle,
                'description' => $metaDescription,
                'keywords' => $metaKeywords,
            ];
            $category = CategoryHelper::find($item->category_id);
            $item->breadcrumbs = $category->breadcrumbs;
            $item->breadcrumbs[] = ['href'=> $item->url,'name' => $item->name];

            /* Params */
            $item->params = json_decode($item->params);

            /* Images */

            $item->images = $item->images->map(function ($image, $key) use($item) {
                $name = \md5(\json_encode($image)).'.jpg';
                $image->url = $this->makeImageUrl($item,$key,$name,505);
                //$image->cartKey = md5($item->id.'-'.($image->color ?? null).'-');
                $image->alt = $this->makeAlt($item, $key);
                return $image;
            });
            /* Colors */
            $colorIds = $item->images->unique('color')->pluck('color')->toArray();
            $item->colors = null;
            if (count($colorIds)) {
                $colors = ColorHelper::filtered($colorIds);
                $item->colors = $colors->map(function ($color, $key) use($item) {
                    $color->cartKey = md5($item->id.'-'.$color->id.'-');
                    return $color;
                });
            }
            /*  Prices */
            $type = $item->duplex ? 'two' : 'one';
            $item->prices = PriceHelper::thingPrices($item->thing_id);

            /*  Sizes */
            $sizes = collect(json_decode($item->sizes));
            $item->sizes = $sizes->map(function ($size, $sizeKey) use($item) {
                $size->cartKey = md5($item->id.'--'.$sizeKey);
                return $size;
            });

        }
        return $item;
    }

    /**
     * Get Entity for show image
     *
     * @param sting $slugString
     * @return Model
     */
    public function getForImage($id)
    {
        $columns = [
            'paint_things.id',
            'paint_things.paint_id',
            'paint_things.thing_id',
            'things.images',
            'paints.image1',
            'paints.image2',
            'paints.image3',
        ];
        $entity = $this
            ->startConditions()
            ->select($columns)
            ->join('things','paint_things.thing_id', '=', 'things.id')
            ->join('paints','paint_things.paint_id', '=', 'paints.id')
            ->where('paint_things.id',$id)
            ->toBase()
            ->first();
        return $entity;
    }

    /**
     * Список товаров по массиву ID
     *
     * @param Array $ids
     *
     * @return collection
     */
    public function getPopular(Array $ids)
    {
        if (!count($ids)) {
            return collect([]);
        }
        $columns = $this->teaserColumns;
        $items = $this
            ->startConditions()
            ->select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->whereIn('paint_things.id',$ids)
            ->orderBy('hash')
            ->toBase()
            ->get();

        $items = $this->prepareItems($items);
        return $items;
    }
    /**
     * @param Int $paintId
     *
     * @return collection
     */
    public function getRelated(Int $paintId)
    {

        $columns = $this->teaserColumns;
        $items = $this
            ->startConditions()
            ->select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->where('paint_things.paint_id',$paintId)
            ->where('things.status',1)
            ->orderBy('things.order')
            ->toBase()
            ->get();

        $items = $this->prepareItems($items, true);
        if ($items->count()) {
            \Cache::put('paint_related_'.$paintId, $items);
        }

        return $items;
    }

    /**
     * @param Object $entity
     *
     * @return collection
     */
    public function getSimilar($entity)
    {

        $items = $this
            ->startConditions()
            ->select($this->teaserColumns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->where('paint_things.id','<>',$entity->id)
            ->where('category_id',$entity->category_id)
            ->inRandomOrder()
            ->take(10)
            ->toBase()
            ->get();
        $items = $this->prepareItems($items);
        return $items;
    }

    /**
     * @param object $entity
     *
     * @return collection
     */
    public function getLatests($entity)
    {

        $ids = session('latests',[]);

        if (count($ids)) {
            $str = implode(',', $ids);
            $columns = $this->teaserColumns;
            $items = $this
                ->startConditions()
                ->select($columns)
                ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
                ->join('things', 'paint_things.thing_id', '=', 'things.id')
                ->where('paints.status',1)
                ->where('things.status',1)
                ->where('paint_things.id','<>',$entity->id)
                ->whereIn('paint_things.id',$ids)
                ->orderByRaw("FIELD('paint_things.id', $str)")
                ->toBase()
                ->get();
            $items = $this->prepareItems($items);
        } else {
            $items = collect([]);
        }

        return $items;
    }

    /**
     *  Все элементы для карты сайта
     *
     * @return collection
     */
    public function getForSitemap($part)
    {
        $count = $this
                    ->startConditions()
                    ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
                    ->join('things', 'paint_things.thing_id', '=', 'things.id')
                    ->where('paints.status',1)
                    ->where('things.status',1)
                    ->count();
        $count = (int) ($count / 4);
        $skip = $part * $count;
        $columns = $this->teaserColumns;
        $items = $this
            ->startConditions()
            ->select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->skip($skip)
            ->take($count)
            ->toBase()
            ->get();

        $items = $items->map(function ($item, $key) {
            return $this->makeUrl($item);
        });
        return $items;
    }
    /**
     * @param int $entityId
     * @param int $colorId
     * @param int $sizeId
     *
     * @return object
     */
    public function getForCart(int $entityId, $colorId = null, $sizeId = null)
    {

        $columns = $this->teaserColumns;
        $columns[] = 'things.sizes';
        $columns[] = 'things.images';
        $item = $this
            ->startConditions()
            ->select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->where('paint_things.id',$entityId)
            ->toBase()
            ->first();
        if ($item) {
            $item->cartKey = md5($item->id.'--');
            $item->url = $this->makeUrl($item);
            $item->qty = 0;
            /*  Prices */
            $type = $item->duplex ? 'two' : 'one';
            $item->prices = PriceHelper::thingPrices($item->thing_id);
            $item->price = 0;
            $item->priceUnit = 0;
            /*  Sizes */
            $item->size = null;
            $sizes = collect(json_decode($item->sizes));
            $item->sizes = $sizes->map(function ($size, $sizeKey) use($item) {
                $size->cartKey = md5($item->id.'--'.$sizeKey);
                return $size;
            });

            if ($item->sizes->count()) {
                $sizeKey = (int) $sizeId;
                $item->size = $item->sizes[$sizeKey] ?? $item->sizes->first();
                $item->cartKey = $item->size->cartKey;
            }

            /* Images */
            $images = collect(json_decode($item->images));
            $item->images = $images->map(function ($image, $key) use($item) {
                $image->key = $key;
                return $image;
            });
            $image = null;
            /* Colors */
            $item->color = null;
            $colorIds = $images->unique('color')->pluck('color')->filter()->toArray();
            $item->colors = null;
            if (count($colorIds)) {
                $colors = ColorHelper::filtered($colorIds);
                $item->colors = $colors->map(function ($color, $key) use($item) {
                    $color->cartKey = md5($item->id.'-'.$color->id.'-');
                    return $color;
                });
                $color = null;
                if ($colorId) {
                    $color = $item->colors->where('id',$colorId)->first();
                }
                if (!$color) {
                    $color = $item->colors->first();
                }
                $item->color = $color;
                $item->cartKey = $color->cartKey;
                $image = $item->images->where('color',$color->id)->first();

            }
            if (!$image) {
                $image = $item->images->first();
            }
            $name = md5(\json_encode($image));
            $item->image = $this->makeImageUrl($item,$image->key,$name,205);
        }
        return $item;
    }
    private function prepareItems($items, $isChangeImageForTShirt = false)
    {
        $items = $items->map(function ($item, $key) use($isChangeImageForTShirt) {
            $item = $this->prepare($item, $isChangeImageForTShirt);
            return $item;
        });
        return $items;
    }

    private function prepare($item, $isChangeImageForTShirt = false)
    {

        $type = $item->duplex ? 'two' : 'one';
        $item->priceText = PriceHelper::findUnitText($item->thing_id, $type);

        $item->url = $this->makeUrl($item);

        $item->alt = $this->makeAlt($item);
        $item->images = collect(json_decode($item->images));
        $image = $item->images->first();
        if ($image) {
            $imageName = md5(\json_encode($image)).'.jpg';
            $num = 0;
            if ($isChangeImageForTShirt && $item->type_id == 2 && $item->images->count() > 2) {
                $num = 2;
            }
            $item->image = $this->makeImageUrl($item,$num,$imageName,400);

        } else {
            $item->image = asset('images/no-image.png');
        }

        $item->cartKey = md5($item->id.'--');

        /*  Sizes */
        $sizes = json_decode($item->sizes);
        if ($sizes && count($sizes)){
            $item->cartKey = md5($item->id.'--'. 0);
        }
        /* Colors */
        $colorIds = $item->images->unique('color')->pluck('color')->toArray();
        if (count($colorIds)) {
            $colors = ColorHelper::filtered($colorIds);
            $color = $colors->first();
            if ($color) {
                $item->cartKey = md5($item->id.'-'.$color->id.'-');
            }
        }
        return $item;
    }
    private function replaceVariables($item,$text)
    {
        $text = str_replace(
            ['%name_thing%','%name_paint%'],
            [$item->thing_name, $item->paint_name],
            $text,
        );
        return $text;
    }
    private function makeAlt($item, $num = 0)
    {
        $alt = str_replace(
            ['%name_thing%','%name_paint%','%num%'],
            [$item->thing_name, $item->paint_name, ($num ? $num : '')],
            \ConfigHelper::get('default_alt'),
        );
        return $alt;
    }
    /**
     * Make url for object
     *
     * @param object $item
     *
     * @return string
     */
    private function makeUrl($item)
    {
        $category = CategoryHelper::find($item->category_id);
        $path = $category->path;
        $path .= '/'.$item->slug;
        $url = route('catalog.resolver',[$path]);
        return $url;
    }
    /**
     * Make url for specific image
     *
     * @param object $item
     * @param object $image
     * @param int $size
     *
     * @return string
     */
    private function makeImageUrl($item, $key, $name, $size = 505)
    {
        $host = config('image_storage.host');
        $path = "/files/styles/$item->id/$key/$size/$name";
        $url = $host.$path;
        return $url;
    }

}
