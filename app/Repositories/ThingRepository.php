<?php
namespace App\Repositories;

use App\Models\Thing as Model;
use App\Helpers\PriceHelper;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ThingRepository
 *
 * @package App\Repositories
 */
class ThingRepository extends CoreRepository
{

    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Collection for catalog
     *
     * @param int $page
     * @param int $paginate
     *
     * @return [type]
     */
    public function getForFront($ids)
    {
        if (!count($ids)) {
            return collect([]);
        }
        $columns = ['id','name','type_id','teaser_image as image','params','code'];
        $items = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->whereIn('id',$ids)
            ->orderBy('order')
            ->toBase()
            ->get();


        $items->map(function ($item) {
            return $this->prepare($item);
        });
        return $items;
    }
    public function getForConstructor($id = null)
    {
        $columns = ['code'];
        $item = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->when($id, function ($query, $id) {
                return $query->where('id', $id);
            })
            ->orderBy('order')
            ->toBase()
            ->first();

        return $item;
    }
    public function getForCatalog()
    {

        $columns = [
            'things.id',
            'things.name',
            'things.type_id',
            'teaser_image as image',
            'params',
            'code',
            'types.name as type_name'
        ];
        $items = $this
            ->startConditions()
            ->select($columns)
            ->join('types', 'things.type_id', '=', 'types.id')
            ->where('things.status',1)
            ->where('things.customprint',1)
            ->orderBy('things.order')
            ->orderBy('types.order')
            ->toBase()
            ->get();


        $items->map(function ($item) {
            return $this->prepare($item);
        });
        $items = $items->groupBy('type_id');
        return $items;
    }
    public function getForWholesale()
    {

        $columns = ['id','name','type_id','teaser_image as image','params','code'];
        $items = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->where('wholesale',1)
            ->orderBy('order')
            ->toBase()
            ->get();


        $items->map(function ($item) {
            $item->params = collect(json_decode($item->params));
            $item->image = \Images::square310($item->image);
            $item->url = $this->makeUrl($item);
            $prices = PriceHelper::thingPricesAll($item->id);
            $item->table = $this->getPriceTable($prices);
            return $item;
        });
        return $items;
    }
    public function getForSizes()
    {

        $columns = ['id','name','teaser_image as image','sizes','code'];
        $items = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->where('sizes','>','')
            ->where('sizes','<>','[]')
            ->orderBy('order')
            ->toBase()
            ->get();


        $items->map(function ($item) {
            $item->sizes = collect(json_decode($item->sizes));
            $item->image = \Images::square310($item->image);
            $item->url = $this->makeUrl($item);
            return $item;
        });
        return $items;
    }

    public function getForSitemap()
    {

        $columns = [
            'id',
        ];
        $items = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->toBase()
            ->get();

        $items = $items->map(function ($item) {
            return $this->makeUrl($item);
        });
        return $items;
    }

    private function prepare($item)
    {
        $item->params = collect(json_decode($item->params))->take(2);
        $item->image = \Images::thingTeaser($item->image);
        $item->price = PriceHelper::findUnitText($item->id);
        $item->url = $this->makeUrl($item);
        return $item;
    }

    /**
     * Make url for object
     *
     * @param object $item
     *
     * @return string
     */
    private function makeUrl($item)
    {
        $url = route('constructor', ['id' => $item->id]);
        return $url;
    }
    /**
     * Make url for specific image
     *
     * @param object $item
     * @param object $image
     * @param int $size
     *
     * @return string
     */
    private function makeImageUrl($item, $size = 505)
    {
        $url = \Images::productSmall($item->image);
        return $url;
    }
    private function getPriceTable($prices)
    {
        $table = [
            'ranges' => [],
            'one'   => [],
            'two'   => []
        ];
        $keys = $prices->sortBy('min')->groupBy('min')->keys();
        $prices = $prices->sortByDesc('min');
        $isOne = $prices->where('type','one')->count();
        $isTwo = $prices->where('type','two')->count();
        for ($i=0; $i < count($keys); $i++) {
            if ($keys[$i + 1] ?? false) {
                $table['ranges'][] = $keys[$i].'-'.($keys[$i+1] - 1).' шт';
            } else {
                $table['ranges'][] = $keys[$i].' и >';
            }
            if ($isOne) {
                if ($price = $prices->where('type','one')->where('min','<=',$keys[$i])->first()) {
                    if ($price->value) {
                        $table['one'][] = $price->value.' руб';
                    } else {
                        $table['one'][] = 'Договорная';
                    }
                } else {
                    $table['one'][] = '';
                }
            }
            if ($isTwo) {
                if ($price = $prices->where('type','two')->where('min','<=',$keys[$i])->first()) {
                    if ($price->value) {
                        $table['two'][] = $price->value.' руб';
                    } else {
                        $table['two'][] = 'Договорная';
                    }
                } else {
                    $table['two'][] = '';
                }
            }
        }

        return $table;
        //dd($this->prices);
    }

}
