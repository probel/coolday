<?php
namespace App\Repositories;

use App\Models\Product as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class SaleRepository
 *
 * @package App\Repositories
 */
class SaleRepository extends CoreRepository
{

    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Collection for catalog
     *
     * @param int $page
     * @param int $paginate
     *
     * @return [type]
     */
    public function getCatalogItems($page = 1, $paginate = 24)
    {
        $columns = ['id','name','price','old_price','slug','image'];
        $items = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->orderBy('order')
            ->toBase()
            ->paginate($paginate, $columns, 'page', $page);

        $items->getCollection()->transform(function ($item) {
            return $this->prepare($item);
        });
        return $items;
    }


    /**
     * @param int $saleId
     * @param int $colorId
     * @param int $sizeId
     *
     * @return object
     */
    public function getForCart(int $saleId)
    {

        $columns = ['id','name','price','old_price','slug','image'];
        $item = $this
            ->startConditions()
            ->select($columns)
            ->where('status',1)
            ->where('id',$saleId)
            ->toBase()
            ->first();
        if ($item) {
            $item = $this->prepare($item);
            $item->cartKey = md5($item->id);
            $item->qty = 0;
            $item->paint_name = $item->name;


            /*  Prices */
            $price = (object) ['min' => 1, 'value'=>$item->price];
            if ($item->price) {
                $item->priceText = $item->price . ' руб.';
            } else {
                $item->priceText = 'Договорная';
            }
            $item->prices = collect([$price]);
            $item->price = 0;
            $item->priceUnit = 0;


            $item->size = null;
            $item->colors = null;

        }
        return $item;
    }

    private function prepare($item)
    {
        //$prices = collect(['min' => 1, 'value' => $item->price]);
        if ($item->price) {
            $item->priceText = $item->price . ' руб.';
        } else {
            $item->priceText = 'Договорная';
        }
        $item->url = $this->makeUrl($item);
        $item->image = $this->makeImageUrl($item);
        $item->cartKey = md5($item->id);
        return $item;
    }

    /**
     * Make url for object
     *
     * @param object $item
     *
     * @return string
     */
    private function makeUrl($item)
    {
        $url = route('sale.index');
        return $url;
    }
    /**
     * Make url for specific image
     *
     * @param object $item
     * @param object $image
     * @param int $size
     *
     * @return string
     */
    private function makeImageUrl($item, $size = 505)
    {
        $url = \Images::productSmall($item->image);
        return $url;
    }

}
