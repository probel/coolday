<?php


Route::get('', ['as' => 'admin.dashboard', 'uses' => 'App\Http\Controllers\AdminController@index']);

Route::get('/colors/get', ['as' => 'colors', 'uses' => 'App\Http\Controllers\AdminController@getColors']);

Route::post('/save/image', ['as' => 'admin.save.image', 'uses' => 'App\Http\Controllers\AdminController@saveImage']);
Route::post('/save/fieldfile', ['as' => 'admin.save.fieldfile', 'uses' => 'App\Http\Controllers\AdminController@saveFieldFile']);
Route::post('/save/fieldimage', ['as' => 'admin.save.fieldimage', 'uses' => 'App\Http\Controllers\AdminController@saveFieldImage']);
Route::post('order', ['as' => 'admin.order', 'uses' => 'App\Http\Controllers\AdminController@changeOrder']);
Route::get('/download/callback/{id}/{file}', ['as' => 'admin.download', 'uses' => 'App\Http\Controllers\AdminController@downloadFile']);
Route::get('paints/import', ['as' => 'import', 'uses' => 'App\Http\Controllers\AdminController@importForm']);
Route::post('paints/import', ['as' => 'import.store', 'uses' => 'App\Http\Controllers\AdminController@importStore']);
Route::post('/del/selected', ['as' => 'admin.delete.selected', 'uses' => 'App\Http\Controllers\AdminController@deleteItems']);
