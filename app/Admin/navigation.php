<?php

use SleepingOwl\Admin\Navigation\Page;

$items = [

    AdminSection::addMenuPage(\App\Models\Order::class)
        ->addBadge(function (){return \App\Models\Order::where('status','new')->count();})
        ->setPriority(1),
    [
        'title' => 'Принты',
        'icon' => 'fa fa-print',
        'priority' => 2,
        'pages' => [
            new Page(\App\Models\Category::class),
            new Page(\App\Models\Paint::class),
        ]
    ],
    [
        'title' => 'Предметы',
        'icon' => 'fab fa-product-hunt',
        'priority' => 2,
        'pages' => [
            new Page(\App\Models\Type::class),
            new Page(\App\Models\Thing::class),
        ]
    ],
    AdminSection::addMenuPage(\App\Models\PaintThing::class)->setPriority(3),
    AdminSection::addMenuPage(\App\Models\Product::class)->setPriority(3),
    [
        'title' => 'Справочники',
        'icon' => 'fa fa-th-list',
        'priority' => 4,
        'pages' => [
            new Page(\App\Models\Color::class),
            new Page(\App\Models\Delivery::class),
        ]
    ],
    AdminSection::addMenuPage(\App\Models\Page::class)->setPriority(5),
    AdminSection::addMenuPage(\App\Models\Config::class)->setPriority(5),
    AdminSection::addMenuPage(\App\Models\User::class)->setPriority(5),
];
return $items;
