<?php

namespace App\Services;

use \App\Models\{PaintThing};

class ImageService
{

    public static function slider($image = '')
    {
        if (!$image || !\file_exists(\public_path($image))) {
            $image = 'images/uploads/13620193861b8c1b623cd9ab9a6e365f.png';
        }
        $folder = 'images/styles/slider';
        $fileName = pathinfo($image,PATHINFO_FILENAME);
        $width = 1920;
        $height = 980;
        //dd($image);
        $path = $folder.'/'.$fileName.'.jpg';
        if(!file_exists($path)) {
            $imageObject = \Image::make(public_path($image));
            $imageWidth = $imageObject->width();
            $imageHeight = $imageObject->height();
            if ($imageWidth / $imageHeight > $width / $height) {
                $imageObject->widen($width);
            } else {
                $imageObject->heighten($height);
            }
            @mkdir(public_path($folder),0777,true);
            $imageObject->save(public_path($path));
        }
        return asset($path);
    }
    public static function review($image = '')
    {
        if (!$image || !\file_exists(\public_path($image))) {
            $image = 'images/uploads/13620193861b8c1b623cd9ab9a6e365f.png';
        }
        $folder = 'images/styles/reviews';
        $fileName = pathinfo($image,PATHINFO_FILENAME);
        $width = 310;
        $height = 310;
        $path = $folder.'/'.$fileName.'.jpg';
        if(!file_exists($path)) {
            $imageObject = \Image::make(public_path($image));
            $imageWidth = $imageObject->width();
            $imageHeight = $imageObject->height();
            if ($imageWidth / $imageHeight > $width / $height) {
                $imageObject->widen($width);
            } else {
                $imageObject->heighten($height);
            }
            @mkdir(public_path($folder),0777,true);
            $imageObject->save(public_path($path));
        }
        return asset($path);
    }
    public static function instagram($image = '')
    {
        if (!$image || !\file_exists(\public_path($image))) {
            $image = 'images/uploads/13620193861b8c1b623cd9ab9a6e365f.png';
        }
        $folder = 'images/styles/instagram';
        $fileName = pathinfo($image,PATHINFO_FILENAME);
        $width = 310;
        $height = 310;
        $path = $folder.'/'.$fileName.'.jpg';
        if(!file_exists($path)) {
            $imageObject = \Image::make(public_path($image));
            $imageWidth = $imageObject->width();
            $imageHeight = $imageObject->height();
            if ($imageWidth / $imageHeight > $width / $height) {
                $imageObject->widen($width);
            } else {
                $imageObject->heighten($height);
            }
            @mkdir(public_path($folder),0777,true);
            $imageObject->save(public_path($path));
        }
        return asset($path);
    }
    public static function thingTeaser($image = '')
    {
        if (!$image || !\file_exists(\public_path($image))) {
            $image = 'images/no-image.png';
        }
        $folder = 'images/styles/thing';
        $fileName = pathinfo($image,PATHINFO_FILENAME);
        $width = 205;
        $height = 205;
        //dd($image);
        $path = $folder.'/'.$fileName.'.jpg';
        if(!file_exists($path)) {
            $imageObject = \Image::make(public_path($image));
            $imageWidth = $imageObject->width();
            $imageHeight = $imageObject->height();
            if ($imageWidth / $imageHeight > $width / $height) {
                $imageObject->widen($width);
            } else {
                $imageObject->heighten($height);
            }
            @mkdir(public_path($folder),0777,true);
            $imageObject->save(public_path($path));
        }
        return asset($path);
    }
    public static function productSmall($image = '')
    {
        if (!$image || !\file_exists(\public_path($image))) {
            $image = 'images/no-image.png';
        }
        $folder = 'images/styles/product/small';
        $fileName = pathinfo($image,PATHINFO_FILENAME);
        $width = 205;
        $height = 205;
        //dd($image);
        $path = $folder.'/'.$fileName.'.jpg';
        if(!file_exists($path)) {
            $imageObject = \Image::make(public_path($image));
            $imageWidth = $imageObject->width();
            $imageHeight = $imageObject->height();
            if ($imageWidth / $imageHeight > $width / $height) {
                $imageObject->widen($width);
            } else {
                $imageObject->heighten($height);
            }
            @mkdir(public_path($folder),0777,true);
            $imageObject->save(public_path($path));

            /* $watermark = \Image::make(storage_path('masks/water.png'));
            if ($width > 2443 || $height > 1680) {
                $watermark->fit($width,$height);
            }
            $imageObject->insert($watermark,'center');
            $imageObject->mask(public_path($image), true);
            $bg = \Image::canvas($width,$height,'#262424');
            $bg->insert($imageObject,'center');
             */
            //$bg->save(public_path($path));
        }
        return asset($path);
    }

    public static function square310($image = '')
    {
        if (!$image || !\file_exists(\public_path($image))) {
            $image = 'images/no-image.png';
        }
        $folder = 'images/styles/square310';
        $fileName = pathinfo($image,PATHINFO_FILENAME);
        $width = 310;
        $height = 310;
        $path = $folder.'/'.$fileName.'.jpg';
        if(!file_exists($path)) {
            $imageObject = \Image::make(public_path($image));
            $imageWidth = $imageObject->width();
            $imageHeight = $imageObject->height();
            if ($imageWidth / $imageHeight > $width / $height) {
                $imageObject->widen($width);
            } else {
                $imageObject->heighten($height);
            }
            @mkdir(public_path($folder),0777,true);
            $imageObject->save(public_path($path));
        }
        return asset($path);
    }
    public static function makeImage($entity, $num = 0, $size = 505, $cid = null)
    {
        if ($cid) {
            $image = $entity->images->where('color',$cid)->first();

        } else {
            $image = $entity->images[$num] ?? null;
        }
        if (!$image || !\file_exists(\public_path($image->path ?? 'bad'))) {
            $image = [
                'path' => 'images/no-image.png',
                'num'  => 0,
            ];
        }

        $paintNum = 'image'.(($image->num ?? 0)+1);
        $paintImage = $entity->$paintNum ?? null;

        if ($paintImage && !\file_exists(\public_path($paintImage))) {
            $paintImage = 'images/no-image.png';
        }

        $imageObject = \Image::make(public_path($image->path));
        $imageWidth = $imageObject->width();
        $imageHeight = $imageObject->height();

        $leftX = ((float) ($image->left_x ?? 0)) / 100;
        $rightX = ((float) ($image->right_x ?? 0)) / 100;
        $leftY = ((float) ($image->left_y ?? 0)) / 100;
        $rightY = ((float) ($image->right_y ?? 0)) / 100;

        /* Вычисляем размеры облости для принта */
        $paintWidth = (int) ($imageWidth * ($rightX - $leftX));
        $paintHeight = (int) ($imageHeight * ($rightY - $leftY));

        if ($paintImage && $paintWidth > 0 && $paintHeight > 0) {
            $paintObject = \Image::make(public_path($paintImage));

            /* Вписываем принт в размеры отведенные для его */
            $paintObject->resize($paintWidth, $paintHeight, function ($constraint) {
                $constraint->aspectRatio();
            });

            /* Верхний правый угол */
            $left = (int) ($imageWidth * $leftX);
            $top = (int) ($imageHeight * $leftY);

            /* Размеры картинки принта */
            $paintObjectWidth = $paintObject->width();
            $paintObjectHeight = $paintObject->height();

            /* Центрирование не пропорциональной картинки */
            $left += (int) (($paintWidth - $paintObjectWidth) / 2);
            $top += (int) (($paintHeight - $paintObjectHeight) / 2);

            /* Применяем эффекты к принту */
            if ($image->brightness ?? null) {
                $paintObject->brightness($image->brightness);
            }
            if (($image->colorize_r ?? null) ||
                ($image->colorize_g ?? null) ||
                ($image->colorize_b ?? null)) {
                $paintObject->colorize(
                    ((int)($image->colorize_r ?? 0)),
                    ((int)($image->colorize_g ?? 0)),
                    ((int)($image->colorize_b ?? 0))
                );
            }
            $imageObject->insert($paintObject,'top-left',$left,$top);
        }

        if ($imageWidth / $imageHeight > 1) {
            $imageObject->widen($size);
        } else {
            $imageObject->heighten($size);
        }
        $path = storage_path('tmp.jpg');
        $imageObject->save($path);
        return $path;
    }
}
