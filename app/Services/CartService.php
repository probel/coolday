<?php

namespace App\Services;

use Illuminate\Support\Str;
use \App\Models\{Product, PaintThing};

class CartService
{

    protected $default;
    private $_cart;

    public function __construct()
    {
        $this->default = (object) [
            'items' => collect(),
            'totalQty' => 0,
            'totalPrice' => 0,
            'totalCount' => 0,
        ];
        $saved = session('shop_cart');
        if ($saved) {
            $this->_cart = $saved;
        } else {
            $this->_cart = $this->default;
        }
    }
    public function load()
    {
        return $this->_cart;
    }
    public function setItem($item,$qty)
    {
        $cart = $this->load();
        $item->priceUnit = $this->calcUnitPrice($item,$qty);
        $item->qty = $qty;
        $item->price = $item->priceUnit * $qty;
        $cart->items[$item->cartKey] = $item;
        $this->_cart = $cart;
        $this->calcTotal();
        $this->store();
        return $this->load();
    }
    public function clearCart()
    {

        $this->_cart = $this->default;
        $this->store();
        return $this->load();
    }
    public function changeItemQty($key,$qty)
    {
        $cart = $this->load();
        if ($qty) {
            $item = $cart->items->where('cartKey',$key)->first();
            if ($item) {
                $item->qty = $qty;
                $item->priceUnit = $this->calcUnitPrice($item,$qty);
                $item->qty = $qty;
                $item->price = $item->priceUnit * $qty;
                $cart->items[$item->cartKey] = $item;
            }
        } else {
            $cart->items->forget($key);
        }

        $this->_cart = $cart;
        $this->calcTotal();
        $this->store();
        return $this->load();
    }
    public function store()
    {
        session(['shop_cart' => $this->_cart]);
        return true;
    }

    public static function get()
    {
        return app(CartService::class)->load();
    }

    /**
     * Добавить элемент в корзину
     * @param object $item
     * @param int $qty
     *
     * @return object
     */
    public static function set($item, $qty = 1)
    {
        app(CartService::class)->setItem($item,$qty);
        return app(CartService::class)->load();
    }

    public static function changeQty($key, $qty = 1)
    {
        app(CartService::class)->changeItemQty($key,$qty);
        return app(CartService::class)->load();
    }
    public static function clear()
    {
        app(CartService::class)->clearCart();
        return app(CartService::class)->load();
    }

    /**
     * Поиск цены по количеству
     * @param object $item
     * @param int $qty
     *
     * @return float
     */
    private function calcUnitPrice($item,$qty)
    {

        $price = $item->prices->where('min','<=',$qty)->first()->value ?? 0;

        return $price;
    }

    private function calcTotal()
    {
        $cart = $this->_cart;
        $cart->totalQty = $cart->items->count();
        $cart->totalPrice = $cart->items->sum('price');
        $cart->totalCount = $cart->items->sum('qty');
        $this->_cart = $cart;
    }



    /**
     * @param string $key
     *
     * @return boolean
     */
    public static function checkEntity($key)
    {
        $status = (boolean) self::get()->items->where('key',$key)->count();
        return $status;
    }

}
