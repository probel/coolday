<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\{ PaintThing };
use \App\Repositories\EntityRepository;
use Illuminate\Support\Facades\Http;
class MakeEntityImagesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $entityRepository;
    protected $eid;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($eid)
    {
        $this->entityRepository = app(EntityRepository::class);
        $this->eid = $eid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $entity = $this->entityRepository->getForImage($this->eid);
        $entity->images = collect(\json_decode($entity->images) ?? []);
        foreach ($entity->images ?? [] as $key => $image) {
            //$imagePath = \Images::makeImage($entity, $key, 505);
            $fileName = md5(json_encode($image)).'.jpg';
            $this->post($entity->id, $key, 505,$fileName);
            if (!$key) {
                $this->post($entity->id, $key, 400,$fileName);
                $this->post($entity->id, $key, 205,$fileName);
            }
        }
    }
    private function post($eid, $key, $size, $name)
    {
        $host = config('image_storage.host');
        $path = "/files/styles/$eid/$key/$size/$name";
        //$params = ['eid' => $eid,'key'=>$key,'size'=>$size,'name'=>$name];
        //$q = http_build_query($params);
        $url = $host.$path;//.'?'.$q;
        @file_get_contents($url);
        //$response = Http::get($url);
        //\dump($response->body());
    }
}
