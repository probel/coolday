<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use App\Admin\Widgets\NavigationUserBlock;
use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface;

class AdminSectionsServiceProvider extends ServiceProvider
{
    protected $widgets = [
        NavigationUserBlock::class
    ];
    /**
     * @var array
     */
    protected $sections = [

        \App\Models\Order::class => 'App\Http\Admin\Order',
        \App\Models\Category::class => 'App\Http\Admin\Category',
        \App\Models\Page::class => 'App\Http\Admin\Page',
        \App\Models\Product::class => 'App\Http\Admin\Product',
        \App\Models\Config::class => 'App\Http\Admin\Config',
        \App\Models\User::class => 'App\Http\Admin\Users',
        \App\Models\Type::class => 'App\Http\Admin\Type',
        \App\Models\Thing::class => 'App\Http\Admin\Thing',
        \App\Models\Paint::class => 'App\Http\Admin\Paint',
        \App\Models\Color::class => 'App\Http\Admin\Color',
        \App\Models\Delivery::class => 'App\Http\Admin\Delivery',
        \App\Models\PaintThing::class => 'App\Http\Admin\PaintThing',
    ];


    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
        $widgetsRegistry = $this->app[\SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface::class];

        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }
    }
}
