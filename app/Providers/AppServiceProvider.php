<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Contracts\Routing\UrlGenerator;
use App\Services\CartService;
use App\Models\{ Paint, Thing, PaintThing, Category, Type, Delivery, Page, Color, Config };
use App\Observers\{
    PaintObserver,
    ThingObserver,
    PaintThingObserver,
    CategoryObserver,
    TypeObserver,
    ColorObserver,
    DeliveryObserver,
    PageObserver,
    ConfigObserver
};
use App\Helpers\{
    PriceHelper,
    CategoryHelper,
    PageHelper,
    TypeHelper,
    DeliveryHelper,
    ColorHelper,
    ConfigHelper
};
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        Schema::defaultStringLength(191);
        $this->app->singleton(PriceHelper::class, function ()
        {
            return new PriceHelper();
        });
        $this->app->singleton(CategoryHelper::class, function ()
        {
            return new CategoryHelper();
        });
        $this->app->singleton(PageHelper::class, function ()
        {
            return new PageHelper();
        });
        $this->app->singleton(TypeHelper::class, function ()
        {
            return new TypeHelper();
        });
        $this->app->singleton(ColorHelper::class, function ()
        {
            return new ColorHelper();
        });
        $this->app->singleton(DeliveryHelper::class, function ()
        {
            return new DeliveryHelper();
        });
        $this->app->singleton(ConfigHelper::class, function ()
        {
            return new ConfigHelper();
        });
        $this->app->singleton(CartService::class, function ()
        {
            return new CartService();
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paint::observe(PaintObserver::class);
        Thing::observe(ThingObserver::class);
        Category::observe(CategoryObserver::class);
        Page::observe(PageObserver::class);
        Type::observe(TypeObserver::class);
        Color::observe(ColorObserver::class);
        Delivery::observe(DeliveryObserver::class);
        PaintThing::observe(PaintThingObserver::class);
        Config::observe(ConfigObserver::class);
        //\URL::forceScheme('https');
        \Blade::directive('svg', function($arguments) {

            list($path, $class) = array_pad(explode(',', trim($arguments, "() ")), 2, '');
            $path = trim($path, "' ");
            if (!file_exists(public_path($path))) {
                return '';
            }

            $class = trim($class, "' ");

            // Create the dom document as per the other answers
            $svg = new \DOMDocument();
            $svg->load(public_path($path));
            $svg->documentElement->setAttribute("class", $class);
            $output = $svg->saveXML($svg->documentElement);

            return $output;
        });
    }
}
