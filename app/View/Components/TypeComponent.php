<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\TypeHelper;

class TypeComponent extends Component
{
    /**
     * Collection of Types
     * @var collection
     */
    protected $types;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->types = TypeHelper::all();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $types = $this->types;
        return view('components.type-component',compact('types'));
    }
}
