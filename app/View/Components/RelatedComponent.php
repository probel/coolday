<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\TypeHelper;

class RelatedComponent extends Component
{
    /**
     * Collection of Types
     * @var collection
     */
    protected $types;

    /**
     * Collection of Items
     * @var collection
     */
    protected $items;

    /**
     *  Current Item ID
     * @var int
     */
    protected $current;

    /**
     *  Current Type ID
     * @var int
     */
    protected $currentType;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($items, $current = null, $currentType = null)
    {
        $this->types = TypeHelper::all();
        $this->items = $items;
        $this->current = $current;
        $this->currentType = $currentType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $types = $this->types;
        $items = $this->items;
        $current = $this->current;
        $currentType = $this->currentType;
        return view('components.related-component', compact('types', 'items', 'current', 'currentType'));
    }
}
