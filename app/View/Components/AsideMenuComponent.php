<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\CategoryHelper;

class AsideMenuComponent extends Component
{
    /**
     *  Collection of Categories
     * @var Collection
     */
    protected $categories;

    /**
     *  Current Category ID
     * @var int
     */
    protected $current;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($current = null)
    {
        $this->categories = CategoryHelper::all();
        $this->current = $current;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $categories = $this->categories;
        $current = $this->current;
        return view('components.aside-menu-component',compact('categories','current'));
    }
}
