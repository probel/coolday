<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\MakeEntityImagesJob;

class RenewImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'renew:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $columns = [
            'paint_things.id',
            'paint_things.paint_id',
            'paint_things.thing_id',
            'paint_things.slug',
            'paint_things.hash',
            'things.type_id',
            'things.name as thing_name',
            'things.images as images',
            'things.sizes',
            'paints.category_id',
            'paints.name as paint_name',
            'paints.duplex',
            'paints.order as paint_order',
        ];;
        $items = \App\Models\PaintThing
            ::select($columns)
            ->join('paints', 'paint_things.paint_id', '=', 'paints.id')
            ->join('things', 'paint_things.thing_id', '=', 'things.id')
            ->where('paints.status',1)
            ->where('things.status',1)
            ->orderBy('hash')
            ->toBase()
            ->skip(60000)
            ->take(20000)
            ->get();
        $images = [];
        $host = config('image_storage.host');

        foreach ($items as $key => $item) {
            $item->images = collect(json_decode($item->images));
            foreach ($item->images as $imageKey => $image) {
                if ($image) {
                    $imageName = md5(\json_encode($image)).'.jpg';
                    $path = "/files/styles/$item->id/$imageKey/505/$imageName";
                    $url = $host.$path;
                    $images[] = $url;
                }
            }

        }
        \file_put_contents(storage_path('images.txt'),\implode("\n",$images));
        dd('end');
        $items->getCollection()->transform(function ($item) {
            return $this->prepare($item);
        });
        return $items;
        /* $items = \App\Models\PaintThing::get();
        foreach ($items as $key => $item) {
            MakeEntityImagesJob::dispatch($item->id);
            if (!($key % 100)) {
                \dump($item->id);
            }
        } */
        \dump('end');
    }
}
